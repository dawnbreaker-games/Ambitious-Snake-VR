Shader "Custom/NewSurfaceShader"
{
	Properties
	{
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input
		{
		    float2 uv_MainTex;
			float2 
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
		    // put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutputStandard o)
		{
		    // // Albedo comes from a texture tinted by color
		    // fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
		    // o.Albedo = c.rgb;
		    // // Metallic and smoothness come from slider variables
		    // o.Metallic = _Metallic;
		    // o.Smoothness = _Glossiness;
		    // o.Alpha = c.a;

// The MIT License
// Copyright © 2017 Inigo Quilez
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


// One way to avoid texture tile repetition one using one small
// texture to cover a huge area. Basically, it creates 8 different
// offsets for the texture and picks two to interpolate between.
//
// Unlike previous methods that tile space (https://www.shadertoy.com/view/lt2GDd
// or https://www.shadertoy.com/view/4tsGzf), this one uses a random
// low frequency texture (cache friendly) to pick the actual
// texture's offset.
//
// Also, this one mipmaps to something (ugly, but that's better than
// not having mipmaps at all like in previous methods)
//
// More info here: http://www.iquilezles.org/www/articles/texturerepetition/texturerepetition.htm

		float sum( float3 v ) { return v.x+v.y+v.z; }

		float3 textureNoTile( in float2 x, float v )
		{
			float k = texture( In.uv_MainTex, 0.005*x ).x; // cheap (cache friendly) lookup
			
			float2 duvdx = dFdx( x );
			float2 duvdy = dFdx( x );
			
			float l = k*8.0;
			float f = fract(l);
			
		#if 1
			float ia = floor(l); // my method
			float ib = ia + 1.0;
		#else
			float ia = floor(l+0.5); // suslik's method (see comments)
			float ib = floor(l);
			f = min(f, 1.0-f)*2.0;
		#endif    
			
			float2 offa = sin(float2(3.0,7.0)*ia); // can replace with any other hash
			float2 offb = sin(float2(3.0,7.0)*ib); // can replace with any other hash

			float3 cola = textureGrad( iChannel0, x + v*offa, duvdx, duvdy ).xyz;
			float3 colb = textureGrad( iChannel0, x + v*offb, duvdx, duvdy ).xyz;
			
			return mix( cola, colb, smoothstep(0.2,0.8,f-0.1*sum(cola-colb)) );
		}

		void mainImage( out float4 fragColor, in float2 fragCoord )
		{
			float2 uv = fragCoord.xy / iResolution.xx;
			
			float f = smoothstep( 0.4, 0.6, sin(iTime    ) );
			float s = smoothstep( 0.4, 0.6, sin(iTime*0.5) );
				
			float3 col = textureNoTile( (4.0 + 6.0*s)*uv, f );
			
			fragColor = float4( col, 1.0 );
		}
		}
		ENDCG
	}
	FallBack "Diffuse"
}
