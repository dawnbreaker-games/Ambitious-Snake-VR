﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.InputSystem;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace AmbitiousSnake
{
	public class GameManager : SingletonMonoBehaviour<GameManager>
	{
		public GameObject[] registeredGos = new GameObject[0];
		public TileParent tileParentPrefab;
		public GameModifier[] gameModifiers = new GameModifier[0];
		public static Dictionary<string, GameModifier> gameModifierDict = new Dictionary<string, GameModifier>();
		public static bool paused;
		public static IUpdatable[] updatables = new IUpdatable[0];
		public static int framesSinceLevelLoaded;
		public static bool isQuittingGame;
		public static float pausedTimeSinceLevelLoad;
		public static float totalTimeSinceLevelLoad;
		public static float UnpausedTimeSinceLevelLoad
		{
			get
			{
				return totalTimeSinceLevelLoad - pausedTimeSinceLevelLoad;
			}
		}
		public const int LAGGY_FRAMES_AFTER_SCENE_LOAD = 2;
		bool leftGameplayMenuInput;
		bool previousLeftGameplayMenuInput;
		bool rightGameplayMenuInput;
		bool previousRightGameplayMenuInput;
		bool gameplayMenuInput;
		bool previousGameplayMenuInput;

		public override void Awake ()
		{
			base.Awake ();
			if (instance != this)
				return;
			gameModifierDict.Clear();
			for (int i = 0; i < gameModifiers.Length; i ++)
			{
				GameModifier gameModifier = gameModifiers[i];
				gameModifierDict.Add(gameModifier.name, gameModifier);
			}
			SaveAndLoadManager.Load ();
			for (int i = 0; i < SaveAndLoadManager.saveData.gameModifiers.Length; i ++)
			{
				GameModifier gameModifier = SaveAndLoadManager.saveData.gameModifiers[i];
				gameModifierDict.Add(gameModifier.name, gameModifier);
			}
			SceneManager.sceneLoaded += OnSceneLoaded;
		}

		void OnDestroy ()
		{
			if (instance == this)
				SceneManager.sceneLoaded -= OnSceneLoaded;
		}
		
		void OnSceneLoaded (Scene scene = new Scene(), LoadSceneMode loadMode = LoadSceneMode.Single)
		{
			StopAllCoroutines();
			framesSinceLevelLoaded = 0;
			totalTimeSinceLevelLoad = 0;
			pausedTimeSinceLevelLoad = 0;
		}

		void Update ()
		{
			for (int i = 0; i < updatables.Length; i ++)
			{
				IUpdatable updatable = updatables[i];
				updatable.DoUpdate ();
			}
			// if (Time.deltaTime > 0)
			// 	Physics.Simulate(Time.deltaTime);
			if (ObjectPool.Instance != null && ObjectPool.instance.enabled)
				ObjectPool.instance.DoUpdate ();
			InputSystem.Update ();
			leftGameplayMenuInput = InputManager.LeftGameplayMenuInput;
			rightGameplayMenuInput = InputManager.RightGameplayMenuInput;
			gameplayMenuInput = InputManager.GameplayMenuInput;
			HandleGameplayMenu ();
			HandleRestart ();
			previousLeftGameplayMenuInput = leftGameplayMenuInput;
			previousRightGameplayMenuInput = rightGameplayMenuInput;
			previousGameplayMenuInput = gameplayMenuInput;
			if (framesSinceLevelLoaded > LAGGY_FRAMES_AFTER_SCENE_LOAD)
			{
				float deltaTime = Time.unscaledDeltaTime;
				if (paused)
					pausedTimeSinceLevelLoad += deltaTime;
				totalTimeSinceLevelLoad += deltaTime;
			}
			framesSinceLevelLoaded ++;
		}

		void HandleGameplayMenu ()
		{
			if (GameplayMenu.instance == null || GameplayMenu.instance.gameObject.activeSelf)
				return;
			if (leftGameplayMenuInput && !previousLeftGameplayMenuInput)
				GameplayMenu.selectorTrs = VRCameraRig.instance.leftHandTrs;
			else if (rightGameplayMenuInput && !previousRightGameplayMenuInput)
				GameplayMenu.selectorTrs = VRCameraRig.instance.rightHandTrs;
			else if (gameplayMenuInput && !previousGameplayMenuInput)
				GameplayMenu.selectorTrs = VRCameraRig.instance.eyesTrs;
			else
				return;
			GameplayMenu.instance.SetOrientation ();
			GameplayMenu.instance.gameObject.SetActive(true);
		}

		void HandleRestart ()
		{
			if (InputManager.RestartInput)
				_SceneManager.instance.RestartSceneWithoutTransition ();
		}

		public void Quit ()
		{
			Application.Quit();
		}

		void OnApplicationQuit ()
		{
			// PlayerPrefs.DeleteAll();
			isQuittingGame = true;
			SaveAndLoadManager.Save ();
		}
		
#if UNITY_EDITOR
		public static void DestroyOnNextEditorUpdate (Object obj)
		{
			EditorApplication.update += () => { if (obj == null) return; DestroyObject (obj); };
		}

		static void DestroyObject (Object obj)
		{
			if (obj == null)
				return;
			EditorApplication.update -= () => { DestroyObject (obj); };
			Undo.RecordObject(obj, "Delete " + obj.name);
			DestroyImmediate(obj);
		}
#endif
		
		public static bool ModifierExistsAndIsActive (string name)
		{
			GameModifier gameModifier;
			if (gameModifierDict.TryGetValue(name, out gameModifier))
				return gameModifier.isActive;
			else
				return false;
		}

		public static bool ModifierIsActive (string name)
		{
			return gameModifierDict[name].isActive;
		}

		public static bool ModifierExists (string name)
		{
			return gameModifierDict.ContainsKey(name);
		}

		[Serializable]
		public class GameModifier
		{
			public string name;
			public bool isActive;
		}
	}
}