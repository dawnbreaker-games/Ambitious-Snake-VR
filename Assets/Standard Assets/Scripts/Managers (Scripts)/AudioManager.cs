﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;

namespace AmbitiousSnake
{
	public class AudioManager : SingletonMonoBehaviour<AudioManager>
	{
		public SoundEffect soundEffectPrefab;
		
		public SoundEffect MakeSoundEffect (AudioClip audioClip, Vector3 position)
		{
			return MakeSoundEffect(audioClip, position, soundEffectPrefab.settings.Volume);
		}
		
		public SoundEffect MakeSoundEffect (AudioClip audioClip, Vector3 position, float volume)
		{
			SoundEffect.Settings soundEffectSettings = soundEffectPrefab.settings;
			soundEffectSettings.audioClip = audioClip;
			soundEffectSettings.Position = position;
			soundEffectSettings.Volume = volume;
			return MakeSoundEffect(soundEffectSettings);
		}
		
		public SoundEffect MakeSoundEffect (SoundEffect.Settings soundEffectSettings)
		{
			SoundEffect output = ObjectPool.instance.SpawnComponent<SoundEffect>(soundEffectPrefab.prefabIndex, soundEffectSettings.Position, soundEffectSettings.Rotation);
			output.settings = soundEffectSettings;
			output.Play();
			return output;
		}
	}
}