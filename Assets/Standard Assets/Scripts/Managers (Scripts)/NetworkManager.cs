using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

namespace AmbitiousSnake
{
	public class NetworkManager : SingletonMonoBehaviour<NetworkManager>
	{
		public MultiplayerSnake multiplayerSnakePrefab;
		public TemporaryActiveText notificationText;
		// public UnityClient client;
		public static Dictionary<ushort, MultiplayerSnake> multiplayerSnakesDict = new Dictionary<ushort, MultiplayerSnake>();
		public static bool IsOnline
		{
			get
			{
				return PlayerPrefsExtensions.GetBool("Is online", false);
			}
			set
			{
				PlayerPrefsExtensions.SetBool("Is online", value);
			}
		}
		public static bool lookForPlayers;
		public static byte targetPlayerCount = 2;

		void Connect ()
		{
			// client.MessageReceived += OnMessageReceived;
			// client.ConnectInBackground (client.Host, client.Port, false, OnConnected);
		}

		void Disconnect ()
		{
			// client.MessageReceived -= OnMessageReceived;
			// client.Disconnect();
		}

		void OnConnected (Exception e)
		{
			if (e != null)
				print(e.Message);
			SetTargetPlayerCount (targetPlayerCount);
		}

		public void SetLookForPlayers (bool lookForPlayers)
		{
			NetworkManager.lookForPlayers = lookForPlayers;
			if (lookForPlayers)
				Connect ();
			else
				Disconnect ();
		}

		public void SetTargetPlayerCount (float playerCount)
		{
			targetPlayerCount = (byte) playerCount;
			// if (client.ConnectionState != ConnectionState.Connected)
			// 	return;
			// using (DarkRiftWriter writer = DarkRiftWriter.Create())
			// {
			// 	writer.Write(client.ID);
			// 	writer.Write(targetPlayerCount);
			// 	using (Message message = Message.Create(MessageTags.TARGET_PLAYER_COUNT_SET, writer))
			// 		client.SendMessage(message, SendMode.Reliable);
			// }
		}

		// void OnSpawnPlayerMessage (object sender, Message message)
		// {
		// 	if (MultiplayerLevel.isPlaying)
		// 		SpawnPlayer (message);
		// 	else
		// 	{
		// 		spawnPlayerMessages.Add(message);
		// 		if (MultiplayerLevel.isWaitingForAnotherPlayer && spawnPlayerMessages.Count > 1)
		// 		{
		// 			MultiplayerLevel.isWaitingForAnotherPlayer = false;
		// 			OnAnotherPlayerJoined ();
		// 		}
		// 	}
		// }

		// void OnAnotherPlayerJoined ()
		// {
		// 	StartCoroutine(LoadRaceAfterNotificationRoutine ());
		// }

		// IEnumerator LoadRaceAfterNotificationRoutine ()
		// {
		// 	notificationText.text.text = "Another player joined online! You will be transported to a race cource after this message disappears.";
		// 	StopCoroutine(notificationText.DoRoutine ());
		// 	notificationText.go.SetActive(true);
		// 	StartCoroutine(notificationText.DoRoutine ());
		// 	yield return new WaitUntil(() => (!notificationText.go.activeSelf));
		// 	SceneManager.sceneLoaded += OnSceneLoaded;
		// 	_SceneManager.instance.LoadSceneWithoutTransition ("Procedural Racing");
		// }

		// void OnSceneLoaded (Scene scene = new Scene(), LoadSceneMode loadMode = LoadSceneMode.Single)
		// {
		// 	SceneManager.sceneLoaded -= OnSceneLoaded;
		// 	if (MultiplayerLevel.Instance == null)
		// 	{
		// 		StartCoroutine(LoadRaceAfterNotificationRoutine ());
		// 		return;
		// 	}
		// 	GameManager.paused = true;
		// 	notificationText.text.text = "Waiting for another player to be trasported to this race...";
		// 	// StopCoroutine(notificationText.DoRoutine ());
		// 	// StartCoroutine(notificationText.DoRoutine ());
		// 	for (int i = 0; i < spawnPlayerMessages.Count; i ++)
		// 	{
		// 		Message spawnPlayerMessage = spawnPlayerMessages[i];
		// 		SpawnPlayer (spawnPlayerMessage);
		// 	}
		// 	spawnPlayerMessages.Clear();
		// 	MultiplayerLevel.isPlaying = true;
		// }

		// void SpawnPlayer (Message message)
		// {
		// 	MultiplayerLevel multiplayerLevel = MultiplayerLevel.instance;
		// 	if (multiplayerLevel == null)
		// 		return;
		// 	using (DarkRiftReader reader = message.GetReader())
		// 	{
		// 		ushort id = reader.ReadUInt16();
		// 		byte spawnPointIndex = reader.ReadByte();
		// 		MultiplayerSnake multiplayerSnake = multiplayerLevel.SpawnSnake(multiplayerLevel.spawnPoints[(int) spawnPointIndex], id);
		// 		multiplayerSnakesDict.Add(id, multiplayerSnake);
		// 		if (id == client.ID)
		// 			multiplayerSnake.enabled = true;
		// 	}
		// }

		// void OnMessageReceived (object sender, MessageReceivedEventArgs eventArgs)
		// {
		// 	using (Message message = eventArgs.GetMessage())
		// 	{
		// 		if (message.Tag == MessageTags.START_GAME)
		// 			OnStartGame (eventArgs);
		// 		else if (message.Tag == MessageTags.PLAYER_ADDED_HEAD_PIECE)
		// 			OnPlayerAddedHeadPiece (eventArgs);
		// 		else if (message.Tag == MessageTags.PLAYER_ADDED_TAIL_PIECE)
		// 			OnPlayerAddedTailPiece (eventArgs);
		// 		else if (message.Tag == MessageTags.PLAYER_REMOVED_TAIL_PIECE)
		// 			OnPlayerRemovedTailPiece (eventArgs);
		// 		else if (message.Tag == MessageTags.PLAYER_MOVED)
		// 			OnPlayerMoved (eventArgs);
		// 		else if (message.Tag == MessageTags.PLAYER_ROTATED)
		// 			OnPlayerRotated (eventArgs);
		// 		else if (message.Tag == MessageTags.PLAYER_LEFT)
		// 			OnPlayerLeft (eventArgs);
		// 		else// if (message.Tag == MessageTags.CHECKPOINT_PASSED)
		// 			OnCheckpointPassed (eventArgs);
		// 	}
		// }

		// void OnStartGame (MessageReceivedEventArgs eventArgs)
		// {
		// 	startGameMessage = eventArgs.GetMessage();
		// 	SceneManager.sceneLoaded += OnSceneLoaded;
		// 	_SceneManager.instance.LoadSceneWithoutTransition ("Procedural Racing");
		// }

		void OnSceneLoaded (Scene scene = new Scene(), LoadSceneMode loadSceneMode = LoadSceneMode.Single)
		{
			SceneManager.sceneLoaded -= OnSceneLoaded;
			StartCoroutine(StartGameRoutine ());
		}

		IEnumerator StartGameRoutine ()
		{
			// using (DarkRiftReader reader = startGameMessage.GetReader())
			// {
			// 	ushort[] clientIds = reader.ReadUInt16s();
			// 	string startTimeText = reader.ReadString();
			// 	yield return new WaitUntil(() => (ProceduralEndlessRacingLevel.furthestLevelPiece != null && ProceduralEndlessRacingLevel.furthestLevelPiece.isInitialized));
			// 	MultiplayerLevel multiplayerLevel = MultiplayerLevel.instance;
			// 	for (int i = 0; i < clientIds.Length; i ++)
			// 	{
			// 		ushort clientId = clientIds[i];
			// 		MultiplayerSnakeSpawnPoint[] multiplayerSnakeSpawnPoints = ProceduralEndlessRacingLevel.furthestLevelPiece.snakeSpawnPointGroupsDict[(byte) clientIds.Length].snakeSpawnPoints;
			// 		MultiplayerSnake multiplayerSnake = multiplayerLevel.SpawnSnake(multiplayerSnakeSpawnPoints[i], clientId);
			// 		multiplayerSnakesDict.Add(clientId, multiplayerSnake);
			// 		if (clientId == client.ID)
			// 			multiplayerSnake.enabled = true;
			// 	}
			// }
			yield break;
		}

		// void OnPlayerAddedHeadPiece (MessageReceivedEventArgs eventArgs)
		// {
		// 	using (Message message = eventArgs.GetMessage())
		// 	{
		// 		using (DarkRiftReader reader = message.GetReader())
		// 		{
		// 			ushort id = reader.ReadUInt16();
		// 			Vector3 localPosition = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
		// 			MultiplayerSnake multiplayerSnake = multiplayerSnakesDict[id];
		// 			multiplayerSnake.AddHeadPiece (multiplayerSnake.trs.position + localPosition);
		// 		}
		// 	}
		// }

		// void OnPlayerAddedTailPiece (MessageReceivedEventArgs eventArgs)
		// {
		// 	using (Message message = eventArgs.GetMessage())
		// 	{
		// 		using (DarkRiftReader reader = message.GetReader())
		// 		{
		// 			ushort id = reader.ReadUInt16();
		// 			Vector3 localPosition = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
		// 			MultiplayerSnake multiplayerSnake = multiplayerSnakesDict[id];
		// 			multiplayerSnake.AddTailPiece (multiplayerSnake.trs.position + localPosition);
		// 		}
		// 	}
		// }

		// void OnPlayerRemovedTailPiece (MessageReceivedEventArgs eventArgs)
		// {
		// 	using (Message message = eventArgs.GetMessage())
		// 	{
		// 		using (DarkRiftReader reader = message.GetReader())
		// 		{
		// 			ushort id = reader.ReadUInt16();
		// 			MultiplayerSnake multiplayerSnake = multiplayerSnakesDict[id];
		// 			multiplayerSnake.RemoveTailPiece ();
		// 		}
		// 	}
		// }

		// void OnPlayerMoved (MessageReceivedEventArgs eventArgs)
		// {
		// 	using (Message message = eventArgs.GetMessage())
		// 	{
		// 		using (DarkRiftReader reader = message.GetReader())
		// 		{
		// 			ushort id = reader.ReadUInt16();
		// 			Vector3 position = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
		// 			MultiplayerSnake multiplayerSnake = multiplayerSnakesDict[id];
		// 			multiplayerSnake.trs.position = position;
		// 		}
		// 	}
		// }

		// void OnPlayerRotated (MessageReceivedEventArgs eventArgs)
		// {
		// 	using (Message message = eventArgs.GetMessage())
		// 	{
		// 		using (DarkRiftReader reader = message.GetReader())
		// 		{
		// 			ushort id = reader.ReadUInt16();
		// 			Vector3 eulerAngles = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
		// 			MultiplayerSnake multiplayerSnake = multiplayerSnakesDict[id];
		// 			multiplayerSnake.trs.eulerAngles = eulerAngles;
		// 		}
		// 	}
		// }

		// void OnPlayerLeft (MessageReceivedEventArgs eventArgs)
		// {
		// 	using (Message message = eventArgs.GetMessage())
		// 	{
		// 		using (DarkRiftReader reader = message.GetReader())
		// 		{
		// 			ushort id = reader.ReadUInt16();
		// 		}
		// 	}
		// }

		// void OnCheckpointPassed (MessageReceivedEventArgs eventArgs)
		// {
		// 	using (Message message = eventArgs.GetMessage())
		// 	{
		// 		using (DarkRiftReader reader = message.GetReader())
		// 		{
		// 			byte[] levelPiecesIndicies = reader.ReadBytes();
		// 			ProceduralEndlessRacingLevel.futureLevelPiecesIndicies.AddRange(levelPiecesIndicies);
		// 		}
		// 	}
		// }

		public void SendCheckpointPassedMessage (byte[] newLevelPieces)
		{
			// using (DarkRiftWriter writer = DarkRiftWriter.Create())
			// {
			// 	using (Message message = Message.Create(MessageTags.CHECKPOINT_PASSED, writer))
			// 		client.SendMessage(message, SendMode.Reliable);
			// }
		}

		public struct MessageTags
		{
			public const ushort START_GAME = 0;
			public const ushort PLAYER_ADDED_HEAD_PIECE = 1;
			public const ushort PLAYER_ADDED_TAIL_PIECE = 2;
			public const ushort PLAYER_REMOVED_TAIL_PIECE = 3;
			public const ushort PLAYER_MOVED = 4;
			public const ushort PLAYER_ROTATED = 5;
			public const ushort TARGET_PLAYER_COUNT_SET = 6;
			public const ushort PLAYER_LEFT = 7;
			public const ushort CHECKPOINT_PASSED = 8;
		}
	}
}