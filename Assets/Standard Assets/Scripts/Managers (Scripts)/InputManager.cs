﻿using System;
using Extensions;
using UnityEngine;
using System.Collections;
using Unity.XR.Oculus.Input;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.XR;
using System.Collections.Generic;
using UnityEngine.XR.OpenXR.Features.Interactions;

namespace AmbitiousSnake
{
	public class InputManager : SingletonMonoBehaviour<InputManager>, IUpdatable
	{
		public InputDevice inputDevice;
		public InputSettings settings;
		public static Vector2? LeftThumbstick
		{
			get
			{
				if (LeftOculusTouchController != null)
					return Vector2.ClampMagnitude(LeftOculusTouchController.thumbstick.ReadValue(), 1);
				else
				{
					if (LeftValveIndexController != null)
						return Vector2.ClampMagnitude(LeftValveIndexController.thumbstick.ReadValue(), 1);
					else
					{
						if (LeftHTCViveController != null)
							return Vector2.ClampMagnitude(LeftValveIndexController.thumbstick.ReadValue(), 1);
						else
							return null;
					}
				}
			}
		}
		public static Vector2? RightThumbstick
		{
			get
			{
				if (RightOculusTouchController != null)
					return Vector2.ClampMagnitude(RightOculusTouchController.thumbstick.ReadValue(), 1);
				else
				{
					if (RightValveIndexController != null)
						return Vector2.ClampMagnitude(RightValveIndexController.thumbstick.ReadValue(), 1);
					else
					{
						if (RightHTCViveController != null)
							return Vector2.ClampMagnitude(RightValveIndexController.thumbstick.ReadValue(), 1);
						else
							return null;
					}
				}
			}
		}
		public static bool LeftGripInput
		{
			get
			{
				return (LeftOculusTouchController != null && LeftOculusTouchController.gripPressed.isPressed) || 
					(LeftValveIndexController != null && LeftValveIndexController.gripPressed.isPressed) || 
					(LeftHTCViveController != null && LeftHTCViveController.gripPressed.isPressed);
			}
		}
		public static bool RightGripInput
		{
			get
			{
				return (RightOculusTouchController != null && RightOculusTouchController.gripPressed.isPressed) || 
					(RightValveIndexController != null && RightValveIndexController.gripPressed.isPressed) || 
					(RightHTCViveController != null && RightHTCViveController.gripPressed.isPressed);
			}
		}
		public static bool LeftTriggerInput
		{
			get
			{
				return (LeftOculusTouchController != null && LeftOculusTouchController.triggerPressed.isPressed) || 
					(LeftValveIndexController != null && LeftValveIndexController.triggerPressed.isPressed) || 
					(LeftHTCViveController != null && LeftHTCViveController.triggerPressed.isPressed);
			}
		}
		public static bool RightTriggerInput
		{
			get
			{
				return (RightOculusTouchController != null && RightOculusTouchController.triggerPressed.isPressed) || 
					(RightValveIndexController != null && RightValveIndexController.triggerPressed.isPressed) || 
					(RightHTCViveController != null && RightHTCViveController.triggerPressed.isPressed);
			}
		}
		public static bool LeftPrimaryButtonInput
		{
			get
			{
				return (LeftOculusTouchController != null && LeftOculusTouchController.primaryButton.isPressed) || 
					(LeftValveIndexController != null && LeftValveIndexController.primaryButton.isPressed);
			}
		}
		public static bool RightPrimaryButtonInput
		{
			get
			{
				return (RightOculusTouchController != null && RightOculusTouchController.primaryButton.isPressed) || 
					(RightValveIndexController != null && RightValveIndexController.primaryButton.isPressed);
			}
		}
		public static bool LeftSecondaryButtonInput
		{
			get
			{
				return (LeftOculusTouchController != null && LeftOculusTouchController.secondaryButton.isPressed) || 
					(LeftValveIndexController != null && LeftValveIndexController.secondaryButton.isPressed);
			}
		}
		public static bool RightSecondaryButtonInput
		{
			get
			{
				return (RightOculusTouchController != null && RightOculusTouchController.secondaryButton.isPressed) || 
					(RightValveIndexController != null && RightValveIndexController.secondaryButton.isPressed);
			}
		}
		public static bool LeftThumbstickClickedInput
		{
			get
			{
				return (LeftOculusTouchController != null && LeftOculusTouchController.thumbstickClicked.isPressed) || 
					(LeftValveIndexController != null && LeftValveIndexController.thumbstickClicked.isPressed) || 
					(LeftHTCViveController != null && LeftHTCViveController.trackpadClicked.isPressed);
			}
		}
		public static bool RightThumbstickClickedInput
		{
			get
			{
				return (RightOculusTouchController != null && RightOculusTouchController.thumbstickClicked.isPressed) || 
					(RightValveIndexController != null && RightValveIndexController.thumbstickClicked.isPressed) || 
					(RightHTCViveController != null && RightHTCViveController.trackpadClicked.isPressed);
			}
		}
		public static Vector3? HeadPosition
		{
			get
			{
				if (Hmd != null)
					return Hmd.devicePosition.ReadValue();
				else
					return null;
			}
		}
		public static Quaternion? HeadRotation
		{
			get
			{
				if (Hmd != null)
					return Hmd.deviceRotation.ReadValue();
				else
					return null;
			}
		}
		public static Vector3? LeftHandPosition
		{
			get
			{
				if (LeftOculusTouchController != null)
					return LeftOculusTouchController.devicePosition.ReadValue();
				else if (LeftValveIndexController != null)
					return LeftValveIndexController.devicePosition.ReadValue();
				else if (LeftHTCViveController != null)
					return LeftHTCViveController.devicePosition.ReadValue();
				else
					return null;
			}
		}
		public static Quaternion? LeftHandRotation
		{
			get
			{
				if (LeftOculusTouchController != null)
					return LeftOculusTouchController.deviceRotation.ReadValue();
				else if (LeftValveIndexController != null)
					return LeftValveIndexController.deviceRotation.ReadValue();
				else if (LeftHTCViveController != null)
					return LeftHTCViveController.deviceRotation.ReadValue();
				else
					return null;
			}
		}
		public static Vector3? RightHandPosition
		{
			get
			{
				if (RightOculusTouchController != null)
					return RightOculusTouchController.devicePosition.ReadValue();
				else if (RightValveIndexController != null)
					return RightValveIndexController.devicePosition.ReadValue();
				else if (RightHTCViveController != null)
					return RightHTCViveController.devicePosition.ReadValue();
				else
					return null;
			}
		}
		public static Quaternion? RightHandRotation
		{
			get
			{
				if (RightOculusTouchController != null)
					return RightOculusTouchController.deviceRotation.ReadValue();
				else if (RightValveIndexController != null)
					return RightValveIndexController.deviceRotation.ReadValue();
				else if (RightHTCViveController != null)
					return RightHTCViveController.deviceRotation.ReadValue();
				else
					return null;
			}
		}
		public static XRHMD Hmd
		{
			get
			{
				return InputSystem.GetDevice<XRHMD>();
			}
		}
		public static OculusTouchControllerProfile.OculusTouchController LeftOculusTouchController
		{
			get
			{
				return OculusTouchControllerProfile.OculusTouchController.leftHand as OculusTouchControllerProfile.OculusTouchController;
			}
		}
		public static OculusTouchControllerProfile.OculusTouchController RightOculusTouchController
		{
			get
			{
				return OculusTouchControllerProfile.OculusTouchController.rightHand as OculusTouchControllerProfile.OculusTouchController;
			}
		}
		public static ValveIndexControllerProfile.ValveIndexController LeftValveIndexController
		{
			get
			{
				return ValveIndexControllerProfile.ValveIndexController.leftHand as ValveIndexControllerProfile.ValveIndexController;
			}
		}
		public static ValveIndexControllerProfile.ValveIndexController RightValveIndexController
		{
			get
			{
				return ValveIndexControllerProfile.ValveIndexController.rightHand as ValveIndexControllerProfile.ValveIndexController;
			}
		}
		public static HTCViveControllerProfile.ViveController LeftHTCViveController
		{
			get
			{
				return HTCViveControllerProfile.ViveController.leftHand as HTCViveControllerProfile.ViveController;
			}
		}
		public static HTCViveControllerProfile.ViveController RightHTCViveController
		{
			get
			{
				return HTCViveControllerProfile.ViveController.rightHand as HTCViveControllerProfile.ViveController;
			}
		}
		public static bool LeftMoveInput
		{
			get
			{
				return LeftGripInput;
			}
		}
		public static bool RightMoveInput
		{
			get
			{
				return RightGripInput;
			}
		}
		public static Vector3 MoveInput
		{
			get
			{
				Vector3 output = Vector3.zero;
				if (instance.inputDevice == InputDevice.KeyboardAndMouse && Keyboard.current.wKey.isPressed)
					output = Quaternion.Inverse(VRCameraRig.instance.eyesTrs.rotation) * VRCameraRig.instance.eyesTrs.forward;
				if (!GameManager.gameModifierDict["Move hand to change direction"].isActive)
				{
					if (LeftMoveInput && LeftHandRotation != null)
						output = (Quaternion) LeftHandRotation * Vector3.forward;
					if (RightMoveInput && RightHandRotation != null)
						output += (Quaternion) RightHandRotation * Vector3.forward;
				}
				else
				{
					if (LeftMoveInput)
						output = VRCameraRig.instance.leftHandTrs.position - instance.leftHandTrsAtBeginMove.position;
					if (RightMoveInput)
						output += VRCameraRig.instance.rightHandTrs.position - instance.rightHandTrsAtBeginMove.position;
					output = Quaternion.Inverse(VRCameraRig.instance.eyesTrs.rotation) * output.normalized;
				}
				return Vector3.ClampMagnitude(output, 1);
			}
		}
		public static float ChangeLengthInput
		{
			get
			{
				float output = 0;
				if (instance.inputDevice == InputDevice.KeyboardAndMouse)
					output = Mouse.current.leftButton.isPressed.GetHashCode() - Mouse.current.rightButton.isPressed.GetHashCode();
				else
				{
					Vector2? leftThumbstick = LeftThumbstick;
					if (leftThumbstick != null)
						output = ((Vector2) leftThumbstick).y;
					Vector2? rightThumbstick = RightThumbstick;
					if (rightThumbstick != null)
						output += ((Vector2) rightThumbstick).y;
				}
				output = Mathf.Clamp(output, -1, 1);
				if (Mathf.Abs(output) <= instance.settings.defaultDeadzoneMin)
					output = 0;
				return output;
			}
		}
		public static bool SetOrientationInput
		{
			get
			{
				if (instance.inputDevice == InputDevice.KeyboardAndMouse)
					return Keyboard.current.spaceKey.isPressed;
				else
					return LeftThumbstickClickedInput || RightThumbstickClickedInput;
			}
		}
		public static bool RestartInput
		{
			get
			{
#if UNITY_EDITOR
				return Keyboard.current.rKey.isPressed;
#endif
				if (instance.inputDevice == InputDevice.KeyboardAndMouse)
					return Keyboard.current.rKey.isPressed;
				else
					return false;
			}
		}
		public static bool LeftGameplayMenuInput
		{
			get
			{
				if (instance.inputDevice == InputDevice.KeyboardAndMouse)
					return Keyboard.current.leftArrowKey.isPressed;
				else
					return LeftPrimaryButtonInput || LeftSecondaryButtonInput || LeftTriggerInput;
			}
		}
		public static bool RightGameplayMenuInput
		{
			get
			{
				if (instance.inputDevice == InputDevice.KeyboardAndMouse)
					return Keyboard.current.rightArrowKey.isPressed;
				else
					return RightPrimaryButtonInput || RightSecondaryButtonInput || RightTriggerInput;
			}
		}
		public static bool GameplayMenuInput
		{
			get
			{
				if (instance.inputDevice == InputDevice.KeyboardAndMouse)
					return Keyboard.current.escapeKey.isPressed;
				else
					return LeftGameplayMenuInput || RightGameplayMenuInput;
			}
		}
		public static bool SkipCinematicInput
		{
			get
			{
				if (instance.inputDevice == InputDevice.KeyboardAndMouse)
					return Keyboard.current.spaceKey.isPressed;
				else
					return LeftPrimaryButtonInput || RightPrimaryButtonInput;
			}
		}
		public static bool leftMoveInput;
		public static bool previousLeftMoveInput;
		public static bool rightMoveInput;
		public static bool previousRightMoveInput;
		public Transform trs;
		public Transform leftHandTrsAtBeginMove;
		public Transform rightHandTrsAtBeginMove;

		public override void Awake ()
		{
			base.Awake ();
			trs.SetParent(null);
			if (VRCameraRig.Instance == null)
				return;
			leftHandTrsAtBeginMove.SetParent(VRCameraRig.instance.trs);
			rightHandTrsAtBeginMove.SetParent(VRCameraRig.instance.trs);
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public void DoUpdate ()
		{
			leftMoveInput = LeftMoveInput;
			rightMoveInput = RightMoveInput;
			if (GameManager.gameModifierDict["Move hand to change direction"].isActive)
			{
				if (!previousLeftMoveInput && leftMoveInput)
				{
					leftHandTrsAtBeginMove.position = VRCameraRig.instance.leftHandTrs.position;
					leftHandTrsAtBeginMove.gameObject.SetActive(true);
				}
				else if (previousLeftMoveInput && !leftMoveInput)
					leftHandTrsAtBeginMove.gameObject.SetActive(false);
				if (!previousRightMoveInput && rightMoveInput)
				{
					rightHandTrsAtBeginMove.position = VRCameraRig.instance.rightHandTrs.position;
					rightHandTrsAtBeginMove.gameObject.SetActive(true);
				}
				else if (previousRightMoveInput && !rightMoveInput)
					rightHandTrsAtBeginMove.gameObject.SetActive(false);
			}
			else
			{
				leftHandTrsAtBeginMove.gameObject.SetActive(false);
				rightHandTrsAtBeginMove.gameObject.SetActive(false);
			}
			previousLeftMoveInput = leftMoveInput;
			previousRightMoveInput = rightMoveInput;
		}

		void OnDestroy ()
		{
			GameManager.updatables = GameManager.updatables.Remove(this);
		}

		public static float GetAxis (InputControl<float> positiveButton, InputControl<float> negativeButton)
		{
			return positiveButton.ReadValue() - negativeButton.ReadValue();
		}

		public static Vector2 GetAxis2D (InputControl<float> positiveXButton, InputControl<float> negativeXButton, InputControl<float> positiveYButton, InputControl<float> negativeYButton)
		{
			Vector2 output = new Vector2();
			output.x = positiveXButton.ReadValue() - negativeXButton.ReadValue();
			output.y = positiveYButton.ReadValue() - negativeYButton.ReadValue();
			output = Vector2.ClampMagnitude(output, 1);
			return output;
		}
		
		public enum InputDevice
		{
			KeyboardAndMouse,
			VR
		}
	}
}