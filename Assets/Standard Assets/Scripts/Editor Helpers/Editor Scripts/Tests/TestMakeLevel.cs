#if PROBUILDER
#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Unity.EditorCoroutines.Editor;
using static ThreadingUtilities;

namespace AmbitiousSnake
{
	public class TestMakeLevel : EditorScript
	{
		public ProceduralLevel proceduralLevel;
		public BoundsInt boundsInt;

		public override void Do ()
		{
			if (proceduralLevel == null)
				proceduralLevel = ProceduralLevel.Instance;
			EditorCoroutineUtility.StartCoroutine(DoRoutine (), this);
		}

		IEnumerator DoRoutine ()
		{
			proceduralLevel.Init ();
			EditorCoroutineWithData coroutineWithData = new EditorCoroutineWithData(this, ProceduralLevel.MakeLevel (boundsInt, proceduralLevel._spawnEntries.ToArray()));
			yield return new WaitForReturnedValueOfType_Editor<ProceduralLevel>(coroutineWithData);
			print("Yay");
		}
	}
}
#else
namespace AmbitiousSnake
{
	public class TestMakeLevel : EditorScript
	{
	}
}
#endif
#endif