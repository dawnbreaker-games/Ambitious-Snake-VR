#if PROBUILDER
#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;
using CoroutineWithData = ThreadingUtilities.CoroutineWithData;
using WaitForReturnedValueOfType = ThreadingUtilities.WaitForReturnedValueOfType;
using UnityEditor;

namespace AmbitiousSnake
{
	public class TestProceduralTest : EditorScript
	{
		public ProceduralLevel.Test test;
		public ProceduralLevel.Test.InputEvent[] inputEvents = new ProceduralLevel.Test.InputEvent[0];
		[HideInInspector]
		public bool shouldDo;

		public override void Do ()
		{
			shouldDo = true;
			EditorApplication.isPlaying = true;
		}

		void Start ()
		{
			if (!Application.isPlaying || !shouldDo)
				return;
			shouldDo = false;
			StartCoroutine(DoRoutine ());
		}

		IEnumerator DoRoutine ()
		{
			test = new ProceduralLevel.Test(test);
			CoroutineWithData coroutineWithData = new CoroutineWithData(this, test.TestRoutine(inputEvents));
			yield return new ThreadingUtilities.WaitForReturnedValueOfType<ProceduralLevel.Test.Result>(coroutineWithData);
			ProceduralLevel.Test.Result result = (ProceduralLevel.Test.Result) coroutineWithData.result;
			print(result.endEvent + " " + result.timePassed);
		}
	}
}
#else
namespace AmbitiousSnake
{
	public class TestProceduralTest : EditorScript
	{
	}
}
#endif
#endif