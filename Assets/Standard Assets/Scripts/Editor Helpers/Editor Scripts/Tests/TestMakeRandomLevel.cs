#if PROBUILDER
#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AmbitiousSnake
{
	public class TestMakeRandomLevel : EditorScript
	{
		public BoundsInt boundsInt;
		public List<ProceduralLevel.SpawnEntry> spawnEntries = new List<ProceduralLevel.SpawnEntry>();
		public ProceduralLevel.LevelPieceSpawnEntry[] levelPieceSpawnEntries = new ProceduralLevel.LevelPieceSpawnEntry[0];

		public override void Do ()
		{
			spawnEntries.AddRange(levelPieceSpawnEntries);
			ProceduralLevel.Test.MakeTestForRandomLevel (boundsInt, spawnEntries.ToArray());
		}
	}
}
#else
namespace AmbitiousSnake
{
	public class TestMakeRandomLevel : EditorScript
	{
	}
}
#endif
#endif