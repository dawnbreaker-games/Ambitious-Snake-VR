#if PROBUILDER
#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;
using CoroutineWithData = ThreadingUtilities.CoroutineWithData;
using WaitForReturnedValueOfType = ThreadingUtilities.WaitForReturnedValueOfType;
using UnityEditor;

namespace AmbitiousSnake
{
	public class TestIsGapTraversable : EditorScript
	{
		public Vector3 gapVector;
		public float movementAngleFromGround;
		[HideInInspector]
		public bool shouldDo;

		public override void Do ()
		{
			shouldDo = true;
			EditorApplication.isPlaying = true;
		}

		void Start ()
		{
			if (!Application.isPlaying || !shouldDo)
				return;
			shouldDo = false;
			StartCoroutine(DoRoutine ());
		}

		IEnumerator DoRoutine ()
		{
			CoroutineWithData coroutineWithData = new CoroutineWithData(this, ProceduralLevel.IsGapTraversable(gapVector, movementAngleFromGround));
			yield return new ThreadingUtilities.WaitForReturnedValueOfType<bool>(coroutineWithData);
			bool isGapTraversable = (bool) coroutineWithData.result;
			print(isGapTraversable);
		}
	}
}
#else
namespace AmbitiousSnake
{
	public class TestIsGapTraversable : EditorScript
	{
	}
}
#endif
#endif