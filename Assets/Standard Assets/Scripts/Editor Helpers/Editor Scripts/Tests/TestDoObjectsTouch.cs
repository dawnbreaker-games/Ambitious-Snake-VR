#if PROBUILDER
#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AmbitiousSnake
{
	public class TestDoObjectsTouch : EditorScript
	{
		public ProceduralLevel proceduralLevel;

		public override void Do ()
		{
			if (proceduralLevel == null)
				proceduralLevel = ProceduralLevel.Instance;
			print(proceduralLevel.objectsMustTouch.IsValid(proceduralLevel));
		}
	}
}
#else
namespace AmbitiousSnake
{
	public class TestDoObjectsTouch : EditorScript
	{
	}
}
#endif
#endif