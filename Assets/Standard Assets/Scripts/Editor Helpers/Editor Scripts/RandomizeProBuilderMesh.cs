#if PROBUILDER
#if UNITY_EDITOR
using System;
using Extensions;
using UnityEngine;
using UnityEditor;
using UnityEngine.ProBuilder;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace AmbitiousSnake
{
	public class RandomizeProBuilderMesh : EditorScript
	{
		public ProBuilderMesh[] proBuilderMeshes = new ProBuilderMesh[0];
		public FloatRange offsetRange = new FloatRange(0, Mathf.Infinity);

		public override void Do ()
		{
			for (int i = 0; i < proBuilderMeshes.Length; i ++)
			{
				ProBuilderMesh proBuilderMesh = proBuilderMeshes[i];
				_Do (proBuilderMesh, offsetRange);
			}
		}

		static void _Do (ProBuilderMesh proBuilderMesh, FloatRange offsetRange)
		{
			Vertex[] vertices = proBuilderMesh.GetVertices();
			for (int i = 0; i < proBuilderMesh.sharedVertices.Count; i ++)
			{
				SharedVertex sharedVertex = proBuilderMesh.sharedVertices[i];
				Vector3 offset = Random.onUnitSphere * offsetRange.Get(Random.value);
				for (int i2 = 0; i2 < sharedVertex.Count; i2 ++)
				{
					int sharedVertexIndex = sharedVertex[i2];
					Vertex vertex = vertices[sharedVertexIndex];
					vertex.position += offset;
				}
			}
			proBuilderMesh.SetVertices(new List<Vertex>(vertices), true);
		}
	}
}
#else
namespace AmbitiousSnake
{
	public class RandomizeProBuilderMesh : EditorScript
	{
	}
}
#endif
#endif