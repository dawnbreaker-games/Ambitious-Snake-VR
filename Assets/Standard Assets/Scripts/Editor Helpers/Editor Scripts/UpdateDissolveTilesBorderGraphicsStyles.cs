#if UNITY_EDITOR
using UnityEngine;

namespace AmbitiousSnake
{
	public class UpdateDissolveTilesBorderGraphicsStyles : EditorScript
	{
		public DissolveTile[] dissolveTiles = new DissolveTile[0];

		public override void Do ()
		{
			if (dissolveTiles.Length == 0)
				dissolveTiles = FindObjectsOfType<DissolveTile>();
			_Do (dissolveTiles);
		}

		static void _Do (DissolveTile[] dissolveTiles)
		{
			for (int i = 0; i < dissolveTiles.Length; i ++)
			{
				DissolveTile dissolveTile = dissolveTiles[i];
				bool prevoiusAutoSetBorder = dissolveTile.autoSetBorder;
				dissolveTile.autoSetBorder = true;
				dissolveTile.OnValidate ();
				dissolveTile.autoSetBorder = prevoiusAutoSetBorder;
			}
		}
	}
}
#else
namespace AmbitiousSnake
{
	public class UpdateDissolveTilesBorderGraphicsStyles : EditorScript
	{
	}
}
#endif