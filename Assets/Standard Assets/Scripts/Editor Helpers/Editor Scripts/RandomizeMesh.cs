#if UNITY_EDITOR
using System;
using Extensions;
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace AmbitiousSnake
{
	public class RandomizeMesh : EditorScript
	{
		public MeshFilter[] meshFilters = new MeshFilter[0];
		public bool useSharedMesh;
		public FloatRange offsetRange = new FloatRange(0, Mathf.Infinity);

		public override void Do ()
		{
			for (int i = 0; i < meshFilters.Length; i ++)
			{
				MeshFilter meshFilter = meshFilters[i];
				_Do (meshFilter, useSharedMesh, offsetRange);
			}
		}

		static void _Do (MeshFilter meshFilter, bool useSharedMesh, FloatRange offsetRange)
		{
			if (useSharedMesh)
				_Do (meshFilter.sharedMesh, offsetRange);
			else
				_Do (meshFilter.mesh, offsetRange);
		}

		static void _Do (Mesh mesh, FloatRange offsetRange)
		{
			List<Vector3> vertices = new List<Vector3>();
			mesh.GetVertices(vertices);
			bool[] movedVertices = new bool[vertices.Count];
			for (int i = 0; i < vertices.Count; i ++)
			{
				if (!movedVertices[i])
				{
					Vector3 vertex = vertices[i];
					Vector3 offset = Random.onUnitSphere * offsetRange.Get(Random.value);
					for (int i2 = 0; i2 < vertices.Count; i2 ++)
					{
						Vector3 vertex2 = vertices[i2];
						if (vertex2 == vertex)
						{
							vertices[i2] += offset;
							movedVertices[i2] = true;
						}
					}
				}
			}
			mesh.SetVertices(vertices);
		}
	}
}
#else
namespace AmbitiousSnake
{
	public class RandomizeMesh : EditorScript
	{
	}
}
#endif