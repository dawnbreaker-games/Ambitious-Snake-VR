#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using Extensions;

namespace AmbitiousSnake
{
	public class SetRendererRandomColor : EditorScript
	{
		public Renderer renderer;

		public override void Do ()
		{
			if (renderer == null)
				renderer = GetComponent<Renderer>();
			_Do (renderer);
		}

		public static void _Do (Renderer renderer)
		{
			Material material = new Material(renderer.sharedMaterial);
			material.color = ColorExtensions.RandomColor();
			renderer.sharedMaterial = material;
		}

		[MenuItem("Tools/Center selected children on parent collider bounds")]
		static void _Do ()
		{
			Transform[] selectedTransforms = Selection.transforms;
			for (int i = 0; i < selectedTransforms.Length; i ++)
			{
				Transform selectedTrs = selectedTransforms[i];
				Renderer renderer = selectedTrs.GetComponent<Renderer>();
				if (renderer != null)
					_Do (renderer);
			}
		}
	}
}
#else
namespace AmbitiousSnake
{
	public class SetRendererRandomColor : EditorScript
	{
	}
}
#endif