#if UNITY_EDITOR
using Extensions;
using UnityEngine;
using UnityEditor;

namespace AmbitiousSnake
{
	public class MeasureDifferencesBetweenTransforms : EditorScript
	{
		[MenuItem("Tools/Print differences between selected Transforms")]
		static void _Do ()
		{
			Transform[] selectedTransforms = Selection.transforms;
			for (int i = 0; i < selectedTransforms.Length; i ++)
			{
				Transform selectedTrs = selectedTransforms[i];
				for (int i2 = i + 1; i2 < selectedTransforms.Length; i2 ++)
				{
					Transform selectedTrs2 = selectedTransforms[i2];
					string output = "" + (selectedTrs2.position - selectedTrs.position)._ToString() + " is the position offset from " + selectedTrs.name + " to " + selectedTrs2.name;
					output += "\n" + QuaternionExtensions.GetDeltaAngles(selectedTrs.eulerAngles, selectedTrs2.eulerAngles)._ToString() + " is the eulerAngles offset from " + selectedTrs.name + " to " + selectedTrs2.name;
					output += "\n" + (selectedTrs2.lossyScale - selectedTrs.lossyScale)._ToString() + " is the lossyScale offset from " + selectedTrs.name + " to " + selectedTrs2.name;
					print(output);
				}
			}
		}
	}
}
#else
namespace AmbitiousSnake
{
	public class MeasureDifferencesBetweenTransforms : EditorScript
	{
	}
}
#endif