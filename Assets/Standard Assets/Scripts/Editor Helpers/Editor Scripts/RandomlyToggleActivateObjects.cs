#if UNITY_EDITOR
using UnityEngine;
using System.Collections.Generic;

namespace AmbitiousSnake
{
	public class RandomlyToggleActivateObjects : EditorScript
	{
		public GameObject[] gos = new GameObject[0];
		public Mode mode;
		public int count;
		public float chance;

		public override void Do ()
		{
			if (mode == Mode.Count)
			{
				List<GameObject> gosRemaining = new List<GameObject>(gos);
				for (int i = 0; i < count; i ++)
				{
					int randomIndex = Random.Range(0, gosRemaining.Count);
					GameObject go = gosRemaining[randomIndex];
					go.SetActive(!go.activeSelf);
					gosRemaining.RemoveAt(randomIndex);
				}
			}
			else// if (mode == Mode.Chance)
			{
				for (int i = 0; i < gos.Length; i ++)
				{
					GameObject go = gos[i];
					if (Random.value < chance)
						go.SetActive(!go.activeSelf);
				}
			}
		}

		public enum Mode
		{
			Count,
			Chance
		}
	}
}
#else
namespace AmbitiousSnake
{
	public class RandomlyToggleActivateObjects : EditorScript
	{
	}
}
#endif