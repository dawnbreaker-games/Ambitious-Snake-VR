#if PROBUILDER
#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.ProBuilder;

namespace AmbitiousSnake
{
	[ExecuteInEditMode]
	public class ChangeTileSize : EditorScript
	{
		public Tile tile;
		public BoundsOffset boundsOffset;

		public override void Do ()
		{
			if (tile == null)
				tile = GetComponent<Tile>();
			_Do (tile, boundsOffset);
		}

		public static void _Do (Tile tile, BoundsOffset boundsOffset)
		{
			Bounds bounds = boundsOffset.Apply(tile.meshRenderer.bounds);
			Transform[] transforms = MakeGridOfTransformPrefabsInBounds._Do(PrefabUtility.GetCorrespondingObjectFromSource(tile.trs), bounds, Vector3.one, new BoundsOffset(Vector3.one / 2, Vector3.zero), tile.trs.parent);
			ProBuilderMesh[] proBuilderMeshes = new ProBuilderMesh[transforms.Length];
			for (int i = 0; i < transforms.Length; i ++)
			{
				Transform trs = transforms[i];
				proBuilderMeshes[i] = trs.GetComponent<ProBuilderMesh>();
			}
			proBuilderMeshes = MergeProBuilderMeshes._Do(proBuilderMeshes, 2);
			for (int i = 0; i < proBuilderMeshes.Length; i ++)
			{
				ProBuilderMesh proBuilderMesh = proBuilderMeshes[i];
				DeleteProBuilderMeshSharedFaces.Do (proBuilderMesh);
			}
			GameManager.DestroyOnNextEditorUpdate (tile.gameObject);
		}
	}
}
#else
namespace AmbitiousSnake
{
	public class ChangeTileSize : EditorScript
	{
	}
}
#endif
#endif