#if UNITY_EDITOR
using UnityEngine;

namespace AmbitiousSnake
{
	public class ChangeSceneArea : ChangeSceneMaterials
	{
		public Tile floorTilePrefab;

		public override void Do ()
		{
			if (renderers.Length == 0)
				renderers = FindObjectsOfType<Renderer>();
			_Do (renderers, materialChangeEntries, floorTilePrefab);
		}

		static void _Do (Renderer[] renderers, MaterialChangeEntry[] materialChangeEntries, Tile floorTilePrefab)
		{
			_Do (renderers, materialChangeEntries);
			Lava lava = FindObjectOfType<Lava>();
			ReplaceWithTile replaceWithTile = lava.GetComponent<ReplaceWithTile>();
			replaceWithTile.replaceWithTilePrefab = floorTilePrefab;
			replaceWithTile.Do ();
			Lava[] lavas = FindObjectsOfType<Lava>();
			lavas[0].GetComponentInChildren<TileRendererMaterial>().Do ();
		}
	}
}
#else
namespace AmbitiousSnake
{
	public class ChangeSceneArea : ChangeSceneMaterials
	{
	}
}
#endif