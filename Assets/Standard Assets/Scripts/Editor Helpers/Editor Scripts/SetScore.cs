#if UNITY_EDITOR
using System;
using UnityEditor;

namespace AmbitiousSnake
{
	public class SetScore : EditorScript
	{
		public uint score;

		public override void Do ()
		{
			_Do (score);
		}

		[MenuItem("Game/Set score to max")]
		static void _Do ()
		{
			_Do ((uint) LevelSelect.Instance.levels.Length * 3);
		}

		static void _Do (uint score)
		{
			SaveAndLoadManager.saveData.score = score;
		}
	}
}
#else
namespace AmbitiousSnake
{
	public class SetScore : EditorScript
	{
	}
}
#endif