#if PROBUILDER
#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEngine.ProBuilder;
using System.Collections.Generic;
using Unity.EditorCoroutines.Editor;

namespace AmbitiousSnake
{
	public class UnmergeCubeProBuilderMeshes : EditorScript
	{
		public ProBuilderMesh proBuilderMesh;
		public Transform cubePrefab;
		const float SMALL_VALUE = 0.1f;

		public override void Do ()
		{
			EditorCoroutineUtility.StartCoroutine(DoRoutine (proBuilderMesh, cubePrefab), this);
		}

		static IEnumerator DoRoutine (ProBuilderMesh proBuilderMesh, Transform cubePrefab)
		{
			print("UnmergeCubeProBuilderMeshes operation began");
			HashSet<Vector3> cubePositions = new HashSet<Vector3>();
			Vector3[] surroundingPositions = new Vector3[] { new Vector3(-1, -1, -1), new Vector3(0, -1, -1), new Vector3(0, 0, -1), new Vector3(-1, 0, -1), new Vector3(-1, 0, 0), new Vector3(-1, -1, 0), new Vector3(0, -1, 0) };
			Transform trs = proBuilderMesh.GetComponent<Transform>();
			for (int i = 0; i < proBuilderMesh.positions.Count; i ++)
			{
				Vector3 position = proBuilderMesh.positions[i];
				MakeCubeIfShould (ref cubePositions, position, surroundingPositions, Vector3.zero, proBuilderMesh.positions, cubePrefab, trs);
				MakeCubeIfShould (ref cubePositions, position, surroundingPositions, Vector3.right, proBuilderMesh.positions, cubePrefab, trs);
				MakeCubeIfShould (ref cubePositions, position, surroundingPositions, Vector3.up, proBuilderMesh.positions, cubePrefab, trs);
				MakeCubeIfShould (ref cubePositions, position, surroundingPositions, Vector3.forward, proBuilderMesh.positions, cubePrefab, trs);
				MakeCubeIfShould (ref cubePositions, position, surroundingPositions, Vector3.one, proBuilderMesh.positions, cubePrefab, trs);
				MakeCubeIfShould (ref cubePositions, position, surroundingPositions, new Vector3(1, 1, 0), proBuilderMesh.positions, cubePrefab, trs);
				MakeCubeIfShould (ref cubePositions, position, surroundingPositions, new Vector3(1, 0, 1), proBuilderMesh.positions, cubePrefab, trs);
				MakeCubeIfShould (ref cubePositions, position, surroundingPositions, new Vector3(0, 1, 1), proBuilderMesh.positions, cubePrefab, trs);
				yield return new WaitForEndOfFrame();
			}
			print("UnmergeCubeProBuilderMeshes operation ended");
		}

		static void MakeCubeIfShould (ref HashSet<Vector3> cubePositions, Vector3 position, Vector3[] surroundingPositions, Vector3 offset, IList<Vector3> vertexPositions, Transform cubePrefab, Transform meshTrs)
		{
			if (cubePositions.Contains(meshTrs.TransformPoint(position + new Vector3(-0.5f, -0.5f, -0.5f) + offset)))
				return;
			for (int i = 0; i < surroundingPositions.Length; i ++)
			{
				Vector3 surroundingPosition = position + surroundingPositions[i];
				if (((surroundingPosition + offset) - position).sqrMagnitude <= SMALL_VALUE * SMALL_VALUE)
					surroundingPosition += offset;
				bool hasPosition = false;
				for (int i2 = 0; i2 < vertexPositions.Count; i2 ++)
				{
					Vector3 vertexPosition = vertexPositions[i2];
					if ((vertexPosition - (surroundingPosition + offset)).sqrMagnitude <= SMALL_VALUE * SMALL_VALUE)
					{
						hasPosition = true;
						break;
					}
				}
				if (!hasPosition)
					return;
			}
			Transform trs = (Transform) PrefabUtility.InstantiatePrefab(cubePrefab);
			trs.position = meshTrs.TransformPoint(position + new Vector3(-0.5f, -0.5f, -0.5f) + offset);
			cubePositions.Add(trs.position);
			// print("UnmergeCubeProBuilderMeshes operation made a cube");
		}
	}
}
#else
namespace AmbitiousSnake
{
	public class UnmergeCubeProBuilderMeshes : EditorScript
	{
	}
}
#endif
#endif