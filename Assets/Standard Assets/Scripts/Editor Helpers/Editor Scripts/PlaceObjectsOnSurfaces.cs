#if PROBUILDER
#if UNITY_EDITOR
using System;
using Extensions;
using UnityEngine;
using UnityEditor;
using UnityEngine.ProBuilder;
using System.Collections.Generic;
using Random = UnityEngine.Random;
using Math = UnityEngine.ProBuilder.Math;

namespace AmbitiousSnake
{
	public class PlaceObjectsOnSurfaces : EditorScript
	{
		public ObjectEntry[] objectEntries = new ObjectEntry[0];
		public ProBuilderMesh[] proBuilderMeshes = new ProBuilderMesh[0];
		public int targetPlaceCount;
		public Transform[] placePoints = new Transform[0];
		public bool useDistanceRange;
		public FloatRange distanceRange = new FloatRange(0, Mathf.Infinity);
		public int maxRetries = 100;

		public override void Do ()
		{
			List<Vector3> positions = new List<Vector3>();
			for (int i = 0; i < targetPlaceCount; i ++)
			{
				for (int i2 = 0; i2 < maxRetries; i2 ++)
				{
					(Transform trs, ObjectEntry objectEntry) trsAndObjectEntry = _Do(placePoints[Random.Range(0, placePoints.Length)].position, Random.onUnitSphere, objectEntries, proBuilderMeshes);
					Transform trs = trsAndObjectEntry.trs;
					if (trs != null)
					{
						if (useDistanceRange)
						{
							Vector3 position = trs.TransformPoint(-trsAndObjectEntry.objectEntry.offsetPosition);
							if (positions.Count > 0)
							{
								Vector3 closestPosition = VectorExtensions.GetClosestPoint(position, positions.ToArray());
								if (!distanceRange.Contains((position - closestPosition).magnitude))
								{
									GameManager.DestroyOnNextEditorUpdate (trs.gameObject);
									continue;
								}
							}
							positions.Add(position);
						}
						break;
					}
				}
			}
		}

		public static (Transform, ObjectEntry) _Do (Vector3 position, Vector3 direction, ObjectEntry[] objectEntries, ProBuilderMesh[] proBuilderMeshes)
		{
			ObjectEntry objectEntry;
			do
			{
				objectEntry = objectEntries[Random.Range(0, objectEntries.Length)];
			} while (Random.value > objectEntry.chance);
			return ( _Do(position, direction, objectEntry, proBuilderMeshes), objectEntry );
		}

		public static Transform _Do (Vector3 position, Vector3 direction, ObjectEntry objectEntry, ProBuilderMesh[] proBuilderMeshes)
		{
			Transform output = null;
			ContactPoint contactPoint = Raycast(position, direction, proBuilderMeshes);
			if (contactPoint != null)
			{
				bool isValidFacing = false;
				for (int i = 0; i < objectEntry.validFacingRanges.Length; i ++)
				{
					ObjectEntry.ValidFacingRange validFacingRange = objectEntry.validFacingRanges[i];
					if (Vector3.Angle(validFacingRange.facing, contactPoint.normal) <= validFacingRange.maxDegreesOffset)
					{
						isValidFacing = true;
						break;
					}
				}
				if (!isValidFacing)
					return null;
				output = Instantiate(objectEntry.trs, contactPoint.position, objectEntry.trs.rotation);
				if (objectEntry.rotateBehaviour == RotateBehaviour.RandomButKeepSameUpFacing)
				{
					Vector3 forward = Random.onUnitSphere;
					Vector3.OrthoNormalize(ref contactPoint.normal, ref forward);
					output.rotation = Quaternion.LookRotation(forward, contactPoint.normal);
				}
				else if (objectEntry.rotateBehaviour == RotateBehaviour.RandomButKeepSameUpFacing)
					output.rotation = Random.rotation;
				else// if (objectEntry.rotateBehaviour == RotateBehaviour.NoRandomRotation)
					output.rotation = Quaternion.LookRotation(contactPoint.normal, Vector3.up);
				output.Rotate(objectEntry.offsetEulerAngles);
				output.localScale *= objectEntry.sizeMultiplyRange.Get(Random.value);
				output.position = output.TransformPoint(objectEntry.offsetPosition);
			}
			return output;
		}

		public static ContactPoint Raycast (Vector3 position, Vector3 direction, ProBuilderMesh[] proBuilderMeshes)
		{
			ContactPoint output = null;
			float minDistance = Mathf.Infinity;
			Ray ray = new Ray(position, direction);
			for (int i = 0; i < proBuilderMeshes.Length; i ++)
			{
				ProBuilderMesh proBuilderMesh = proBuilderMeshes[i];
				Transform trs = proBuilderMesh.GetComponent<Transform>();
				for (int i2 = 0; i2 < proBuilderMesh.faces.Count; i2 += 3)
				{
					Face face = proBuilderMesh.faces[i2];
					for (int i3 = 0; i3 < face.indexes.Count; i3 += 3)
					{
						int index = face.indexes[i3];
						int index2 = face.indexes[i3 + 1];
						int index3 = face.indexes[i3 + 2];
						Vector3 point = trs.TransformPoint(proBuilderMesh.positions[index]);
						Vector3 point2 = trs.TransformPoint(proBuilderMesh.positions[index2]);
						Vector3 point3 = trs.TransformPoint(proBuilderMesh.positions[index3]);
						float distance;
						Vector3 hitPoint;
						if (Math.RayIntersectsTriangle(ray, point, point2, point3, out distance, out hitPoint) && distance < minDistance)
						{
							minDistance = distance;
							output = new ContactPoint(hitPoint, Vector3.Cross(point2 - point, point3 - point));
						}
					}
				}
			}
			return output;
		}

		public enum RotateBehaviour
		{
			Random,
			RandomButKeepSameUpFacing,
			NoRandomRotation
		}

		[Serializable]
		public struct ObjectEntry
		{
			public Transform trs;
			public Vector3 offsetPosition;
			public Vector3 offsetEulerAngles;
			public RotateBehaviour rotateBehaviour;
			public FloatRange sizeMultiplyRange;
			[Range(0, 1)]
			public float chance;
			public ValidFacingRange[] validFacingRanges;
			
			[Serializable]
			public struct ValidFacingRange
			{
				public Vector3 facing;
				public float maxDegreesOffset;
			}
		}

		public class ContactPoint
		{
			public Vector3 position;
			public Vector3 normal;

			public ContactPoint (Vector3 position, Vector3 normal)
			{
				this.position = position;
				this.normal = normal;
			}
		}
	}
}
#else
namespace AmbitiousSnake
{
	public class PlaceObjectsOnSurfaces : EditorScript
	{
	}
}
#endif
#endif