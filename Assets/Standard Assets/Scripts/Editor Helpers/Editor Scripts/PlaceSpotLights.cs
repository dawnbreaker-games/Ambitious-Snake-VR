#if PROBUILDER
#if UNITY_EDITOR
using System;
using Extensions;
using UnityEngine;
using UnityEditor;
using UnityEngine.ProBuilder;
using System.Collections.Generic;
using Random = UnityEngine.Random;
using Math = UnityEngine.ProBuilder.Math;

namespace AmbitiousSnake
{
	public class PlaceSpotLights : EditorScript
	{
		public ProBuilderMesh[] proBuilderMeshes = new ProBuilderMesh[0];
		public int targetPlaceCount;
		public Transform[] placePoints = new Transform[0];
		public FloatRange totalNoticabilityRange = new FloatRange(0, Mathf.Infinity);
		public FloatRange eachLightNoticabilityRange = new FloatRange(0, Mathf.Infinity);
		public FloatRange distanceFromContactPointRange = new FloatRange(0, float.MaxValue);
		public FloatRange rangeMultiplierRange = new FloatRange(0, float.MaxValue);
		public FloatRange intensityRange = new FloatRange(0, float.MaxValue);
		public FloatRange outerSpotAngleRange = new FloatRange(0, 180);
		public FloatRange innerSpotAngleDifferenceRange = new FloatRange(0, 180);
		public float raycastsPerDegree;
		public bool useValidColorsGradient;
		public Gradient validColorsGraidient;
		public ColorPalette validColorPalette;
		public int maxRetries = 100;

		public override void Do ()
		{
			float totalNoticability = 0;
			for (int i = 0; i < targetPlaceCount; i ++)
			{
				for (int i2 = 0; i2 < maxRetries; i2 ++)
				{
					Vector3 position = placePoints[Random.Range(0, placePoints.Length)].position;
					Light light = new GameObject().AddComponent<Light>();
					Vector3 forward = Random.onUnitSphere;
					ContactPoint contactPoint = Raycast(position, forward, proBuilderMeshes);
					if (contactPoint == null || contactPoint.distance < distanceFromContactPointRange.min)
					{
						GameManager.DestroyOnNextEditorUpdate (light.gameObject);
						continue;
					}
					light.type = LightType.Spot;
					light.intensity = intensityRange.Get(Random.value);
					light.spotAngle = outerSpotAngleRange.Get(Random.value);
					light.innerSpotAngle = Mathf.Clamp(light.spotAngle - innerSpotAngleDifferenceRange.Get(Random.value), 0, light.spotAngle);
					if (useValidColorsGradient)
						light.color = validColorsGraidient.Evaluate(Random.value);
					else
						light.color = validColorPalette.Get(Random.value);
					float distance = contactPoint.distance;
					float newDistance = distanceFromContactPointRange.Get(Random.value);
					position += forward * distance - (forward * newDistance);
					light.range = distance * rangeMultiplierRange.Get(Random.value);
					Transform trs = light.GetComponent<Transform>();
					trs.position = position;
					trs.forward = forward;
					float noticability = GetNoticability(light, raycastsPerDegree, proBuilderMeshes);
					if (!eachLightNoticabilityRange.Contains(noticability))
					{
						GameManager.DestroyOnNextEditorUpdate (light.gameObject);
						continue;
					}
					float newTotalNoticability = totalNoticability + noticability;
					if (!totalNoticabilityRange.Contains(newTotalNoticability))
					{
						GameManager.DestroyOnNextEditorUpdate (light.gameObject);
						continue;
					}
					totalNoticability = newTotalNoticability;
					break;
				}
			}
		}

		public static ContactPoint Raycast (Vector3 position, Vector3 direction, ProBuilderMesh[] proBuilderMeshes)
		{
			ContactPoint output = null;
			float minDistance = Mathf.Infinity;
			Ray ray = new Ray(position, direction);
			for (int i = 0; i < proBuilderMeshes.Length; i ++)
			{
				ProBuilderMesh proBuilderMesh = proBuilderMeshes[i];
				Transform trs = proBuilderMesh.GetComponent<Transform>();
				for (int i2 = 0; i2 < proBuilderMesh.faces.Count; i2 += 3)
				{
					Face face = proBuilderMesh.faces[i2];
					for (int i3 = 0; i3 < face.indexes.Count; i3 += 3)
					{
						int index = face.indexes[i3];
						int index2 = face.indexes[i3 + 1];
						int index3 = face.indexes[i3 + 2];
						Vector3 point = trs.TransformPoint(proBuilderMesh.positions[index]);
						Vector3 point2 = trs.TransformPoint(proBuilderMesh.positions[index2]);
						Vector3 point3 = trs.TransformPoint(proBuilderMesh.positions[index3]);
						float distance;
						Vector3 hitPoint;
						if (Math.RayIntersectsTriangle(ray, point, point2, point3, out distance, out hitPoint) && distance < minDistance)
						{
							minDistance = distance;
							Vector2 uv = proBuilderMesh.textures[index];
							Vector2 uv2 = proBuilderMesh.textures[index2];
							Vector2 uv3 = proBuilderMesh.textures[index3];
							Texture2D texture = (Texture2D) proBuilderMesh.GetComponent<MeshRenderer>().material.mainTexture;
							Color color = texture.GetPixelBilinear(uv.x, uv.y);
							Color color2 = texture.GetPixelBilinear(uv2.x, uv2.y);
							Color color3 = texture.GetPixelBilinear(uv3.x, uv3.y);
							output = new ContactPoint(distance, ColorExtensions.GetAverage(color, color2, color3));
						}
					}
				}
			}
			return output;
		}

		public static (ContactPoint[], int) GetContactPoints (Light light, float raycastsPerDegree, ProBuilderMesh[] proBuilderMeshes)
		{
			Transform trs = light.GetComponent<Transform>();
			Vector3 up = Random.onUnitSphere;
			Vector3 forward = trs.forward;
			Vector3.OrthoNormalize(ref forward, ref up);
			List<ContactPoint> contactPoints = new List<ContactPoint>();
			int raycastCount = 0;
			float rotateAmount = 1f / raycastsPerDegree;
			for (float angle = 0; angle <= light.spotAngle; angle += rotateAmount)
			{
				for (float angle2 = 0; angle2 < 360; angle2 += rotateAmount)
				{
					Vector3 direction = Vector3.RotateTowards(forward, up, angle, 0);
					direction = Quaternion.AngleAxis(angle2, forward) * direction;
					ContactPoint contactPoint = Raycast(trs.position, direction, proBuilderMeshes);
					if (contactPoint != null)
						contactPoints.Add(contactPoint);
					raycastCount ++;
				}
			}
			return (contactPoints.ToArray(), raycastCount);
		}

		public static float GetNoticability (Light light, float raycastsPerDegree, ProBuilderMesh[] proBuilderMeshes)
		{
			(ContactPoint[] contactPoints, int raycastCount) contactPointsAndRaycastCount = GetContactPoints(light, raycastsPerDegree, proBuilderMeshes);
			return GetNoticability(light, contactPointsAndRaycastCount.contactPoints, contactPointsAndRaycastCount.raycastCount);
		}

		public static float GetNoticability (Light light, ContactPoint[] contactPoints, int raycastCount)
		{
			float output = 0;
			for (int i = 0; i < contactPoints.Length; i ++)
			{
				ContactPoint contactPoint = contactPoints[i];
				float distance = contactPoint.distance;
				Color lightColor = light.color;
				output += light.intensity * lightColor.GetBrightness() * (1f / (distance * distance)) * (1f - contactPoint.color.GetSimilarity(lightColor));
			}
			output /= raycastCount;
			return output;
		}

		[Serializable]
		public struct ColorPalette
		{
			public AnimationCurve redCurve;
			public AnimationCurve greenCurve;
			public AnimationCurve blueCurve;

			public Color Get (float value)
			{
				return new Color(redCurve.Evaluate(value), greenCurve.Evaluate(value), blueCurve.Evaluate(value));
			}
		}

		public class ContactPoint
		{
			public float distance;
			public Color color;

			public ContactPoint (float distance, Color color)
			{
				this.distance = distance;
				this.color = color;
			}
		}
	}
}
#else
namespace AmbitiousSnake
{
	public class PlaceSpotLights : EditorScript
	{
	}
}
#endif
#endif