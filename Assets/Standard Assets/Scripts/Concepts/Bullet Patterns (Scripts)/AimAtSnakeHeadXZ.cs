using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;

namespace AmbitiousSnake
{
	[CreateAssetMenu]
	public class AimAtSnakeHeadXZ : BulletPattern
	{
		public override Vector3 GetShootDirection (Transform spawner)
		{
			return (Snake.instance.HeadPosition - spawner.position).GetXZ();
		}
	}
}