﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AmbitiousSnake
{
	[CreateAssetMenu]
	public class AimAtSnakeHead : BulletPattern
	{
		public override Vector3 GetShootDirection (Transform spawner)
		{
			return Snake.instance.HeadPosition - spawner.position;
		}
	}
}