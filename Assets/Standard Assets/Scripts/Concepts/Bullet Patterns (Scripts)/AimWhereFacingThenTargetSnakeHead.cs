﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AmbitiousSnake
{
	[CreateAssetMenu]
	public class AimWhereFacingThenTargetSnakeHead : AimWhereFacing
	{
		// [MakeConfigurable]
		public float retargetTime;
		
		public override Bullet[] Shoot (Transform spawner, Bullet bulletPrefab, float positionOffset = 0)
		{
			Bullet[] output = base.Shoot (spawner, bulletPrefab, positionOffset);
			foreach (Bullet bullet in output)
				bullet.StartCoroutine(RetargetAfterDelay (bullet, retargetTime));
			return output;
		}
		
		public override Vector3 GetRetargetDirection (Bullet bullet)
		{
			return Snake.instance.HeadPosition - bullet.trs.position;
		}
	}
}