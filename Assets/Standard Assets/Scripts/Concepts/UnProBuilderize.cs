#if PROBUILDER
using UnityEngine;
using System.Collections;
using UnityEngine.ProBuilder;
using System.Collections.Generic;
#if UNITY_EDITOR
using Extensions;
using UnityEditor;
#endif

namespace AmbitiousSnake
{
	public class UnProBuilderize
	{
		public static void Do (ProBuilderMesh proBuilderMesh)
		{
			ProBuilderMesh proBuilderMeshClone = Object.Instantiate(proBuilderMesh, proBuilderMesh.GetComponent<Transform>().parent);
			MeshFilter meshFilter = proBuilderMeshClone.GetComponent<MeshFilter>();
			Object.DestroyImmediate (proBuilderMeshClone);
			List<int> faceIndices = new List<int>();
			for (int i = 0; i < proBuilderMesh.faceCount; i ++)
			{
				Face face = proBuilderMesh.faces[i];
				faceIndices.AddRange(face.indexes);
			}
			Mesh mesh = new Mesh();
			mesh.vertices = new List<Vector3>(proBuilderMesh.positions).ToArray();
			mesh.triangles = faceIndices.ToArray();
			mesh.normals = proBuilderMesh.GetNormals();
			mesh.uv = new List<Vector2>(proBuilderMesh.textures).ToArray();
			mesh.tangents = proBuilderMesh.GetTangents();
			meshFilter.sharedMesh = mesh;
			MeshCollider meshCollider = meshFilter.GetComponent<MeshCollider>();
			if (meshCollider != null && proBuilderMesh.GetComponent<MeshCollider>().sharedMesh == proBuilderMesh.GetComponent<MeshFilter>().sharedMesh)
				meshCollider.sharedMesh = mesh;
		}

#if UNITY_EDITOR
		[MenuItem("Tools/UnProBuilderize selected ProBuilderMeshes")]
		static void _DoToSelected ()
		{
			ProBuilderMesh[] proBuilderMeshes = SelectionExtensions.GetSelected<ProBuilderMesh>();
			for (int i = 0; i < proBuilderMeshes.Length; i ++)
			{
				ProBuilderMesh proBuilderMesh = proBuilderMeshes[i];
				Do (proBuilderMesh);
			}
		}
#endif
	}
}
#endif