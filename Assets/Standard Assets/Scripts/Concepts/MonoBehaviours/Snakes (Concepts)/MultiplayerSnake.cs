using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace AmbitiousSnake
{
	public class MultiplayerSnake : Snake, ISpawnable
	{
		public int prefabIndex;
		public int PrefabIndex
		{
			get
			{
				return prefabIndex;
			}
		}
		public MultiplayerSnakeTeam owner;
		public ushort id;
		public VRCameraRig vrCameraRig;
		public float minMovementToSyncSqr;
		public float minRotationToSync;
		Vector3 syncedPosition;
		Quaternion syncedRotation;

		public override void DoUpdate ()
		{
			base.DoUpdate ();
			if ((trs.position - syncedPosition).sqrMagnitude >= minMovementToSyncSqr)
			{
				syncedPosition = trs.position;
				SendPlayerMovedMessage ();
			}
			if (Quaternion.Angle(trs.rotation, syncedRotation) >= minRotationToSync)
			{
				syncedRotation = trs.rotation;
				SendPlayerRotatedMessage ();
			}
		}

		public override void AddHeadPiece (Vector3 position, float distanceToPreviousPiece)
		{
			base.AddHeadPiece (position, distanceToPreviousPiece);
			if (!enabled)
				return;
			// using (DarkRiftWriter writer = DarkRiftWriter.Create())
			// {
			// 	writer.Write(id);
			// 	Vector3 localPosition = position - trs.position;
			// 	writer.Write(localPosition.x);
			// 	writer.Write(localPosition.y);
			// 	writer.Write(localPosition.z);
			// 	using (Message message = Message.Create(NetworkManager.MessageTags.PLAYER_ADDED_HEAD_PIECE, writer))
			// 		NetworkManager.instance.client.SendMessage(message, SendMode.Reliable);
			// }
		}

		public override void AddTailPiece (Vector3 position, float distanceToPreviousPiece)
		{
			base.AddTailPiece (position, distanceToPreviousPiece);
			if (!enabled)
				return;
			// using (DarkRiftWriter writer = DarkRiftWriter.Create())
			// {
			// 	writer.Write(id);
			// 	Vector3 localPosition = position - trs.position;
			// 	writer.Write(localPosition.x);
			// 	writer.Write(localPosition.y);
			// 	writer.Write(localPosition.z);
			// 	using (Message message = Message.Create(NetworkManager.MessageTags.PLAYER_ADDED_TAIL_PIECE, writer))
			// 		NetworkManager.instance.client.SendMessage(message, SendMode.Reliable);
			// }
		}

		public override void RemoveTailPiece ()
		{
			base.RemoveTailPiece ();
			if (!enabled)
				return;
			// using (DarkRiftWriter writer = DarkRiftWriter.Create())
			// {
			// 	writer.Write(id);
			// 	using (Message message = Message.Create(NetworkManager.MessageTags.PLAYER_REMOVED_TAIL_PIECE, writer))
			// 		NetworkManager.instance.client.SendMessage(message, SendMode.Reliable);
			// }
		}

		void SendPlayerMovedMessage ()
		{
			// using (DarkRiftWriter writer = DarkRiftWriter.Create())
			// {
			// 	writer.Write(id);
			// 	writer.Write(trs.position.x);
			// 	writer.Write(trs.position.y);
			// 	writer.Write(trs.position.z);
			// 	using (Message message = Message.Create(NetworkManager.MessageTags.PLAYER_MOVED, writer))
			// 		NetworkManager.instance.client.SendMessage(message, SendMode.Reliable);
			// }
		}

		void SendPlayerRotatedMessage ()
		{
			// using (DarkRiftWriter writer = DarkRiftWriter.Create())
			// {
			// 	writer.Write(id);
			// 	writer.Write(trs.eulerAngles.x);
			// 	writer.Write(trs.eulerAngles.y);
			// 	writer.Write(trs.eulerAngles.z);
			// 	using (Message message = Message.Create(NetworkManager.MessageTags.PLAYER_ROTATED, writer))
			// 		NetworkManager.instance.client.SendMessage(message, SendMode.Reliable);
			// }
		}

		public override void OnEnable ()
		{
			syncedPosition = trs.position;
			syncedRotation = trs.rotation;
			vrCameraRig.gameObject.SetActive(true);
			base.OnEnable ();
		}
	}
}