#if PROBUILDER
using UnityEngine;

namespace AmbitiousSnake
{
	public class TestSnake : Snake, ISpawnable
	{
		public int prefabIndex;
		public int PrefabIndex
		{
			get
			{
				return prefabIndex;
			}
		}
		public ProceduralLevel.Test testIAmDoing;
		[HideInInspector]
		public ProceduralLevel.Test.InputEvent[] inputEvents = new ProceduralLevel.Test.InputEvent[0];
		[HideInInspector]
		public ProceduralLevel.Test.InputEvent currentInputEvent = new ProceduralLevel.Test.InputEvent();
		public float totalReward;
		int currentInputEventIndex = -1;

		public override void OnEnable ()
		{
			NextInputEvent ();
			base.OnEnable ();
		}
		
		public override void DoUpdate ()
		{
			if (Time.time - testIAmDoing.StartTime >= testIAmDoing.duration)
			{
				if (testIAmDoing.type == ProceduralLevel.Test.Type.Survival)
					testIAmDoing.End (ProceduralLevel.Test.Result.EndEvent.Survived);
				else
					testIAmDoing.End (ProceduralLevel.Test.Result.EndEvent.TimedOut);
			}
			else if (Time.time >= currentInputEvent.time)
				NextInputEvent ();
			HandleMovement ();
			SetLength (Mathf.Clamp(length.value + currentInputEvent.changeLength * changeLengthRate * Time.deltaTime, length.valueRange.min, length.valueRange.max));
			CheckForDidMovement ();
			rigid.ResetCenterOfMass();
			rigid.ResetInertiaTensor();
		}

		public override void HandleMovement ()
		{
			if (currentInputEvent.move.sqrMagnitude == 0)
				return;
			Move (currentInputEvent.move);
		}

		public override void OnSetLength ()
		{
		}

		void NextInputEvent ()
		{
			if (currentInputEventIndex + 1 >= inputEvents.Length)
				return;
			currentInputEventIndex ++;
			currentInputEvent = inputEvents[currentInputEventIndex];
		}

		void CheckForDidMovement ()
		{
			if (testIAmDoing.type != ProceduralLevel.Test.Type.Movement)
				return;
			for (int i = 0; i < pieces.Count; i ++)
			{
				if ((GetPiecePosition(i) - testIAmDoing.destination).sqrMagnitude <= (SnakePiece.RADIUS + Physics.defaultContactOffset) * (SnakePiece.RADIUS + Physics.defaultContactOffset))
				{
					testIAmDoing.End (ProceduralLevel.Test.Result.EndEvent.DidMovement);
					return;
				}
			}
		}

		public void OnHitEnd ()
		{
			if (testIAmDoing.type == ProceduralLevel.Test.Type.WinLevel)
				testIAmDoing.End (ProceduralLevel.Test.Result.EndEvent.WonLevel);
		}

		public override void TakeDamage (float amount, Hazard source)
		{
			testIAmDoing.End (ProceduralLevel.Test.Result.EndEvent.Died);
		}
	}
}
#endif