#if PROBUILDER
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using static ThreadingUtilities;

namespace AmbitiousSnake
{
	public class ProceduralEndlessRacingLevelCheckpoint : MonoBehaviour
	{
		public bool canSeeSkybox;
#if UNITY_EDITOR
		public bool activate;
#endif

#if UNITY_EDITOR
		void OnValidate ()
		{
			if (!activate)
				return;
			activate = false;
			if (ProceduralEndlessRacingLevel.furthestCheckpoint == this)
			{
				ProceduralEndlessRacingLevel.instance.DeactivatePastLevelPieces ();
				byte[] newLevelPiecesIndicies = ProceduralEndlessRacingLevel.instance.MakeNextLevelPieces();
				NetworkManager.instance.SendCheckpointPassedMessage (newLevelPiecesIndicies);
			}
			else
				ProceduralEndlessRacingLevel.instance.ActivatePastLevelPieces ();
		}
#endif

		void OnTriggerEnter (Collider other)
		{
			if (ProceduralEndlessRacingLevel.furthestCheckpoint == this)
			{
				MultiplayerSnake multiplayerSnake = other.GetComponentInParent<MultiplayerSnake>();
				if (multiplayerSnake.enabled)
				{
					ProceduralEndlessRacingLevel.instance.DeactivatePastLevelPieces ();
					byte[] newLevelPiecesIndicies = ProceduralEndlessRacingLevel.instance.MakeNextLevelPieces();
					NetworkManager.instance.SendCheckpointPassedMessage (newLevelPiecesIndicies);
				}
			}
			else
				ProceduralEndlessRacingLevel.instance.ActivatePastLevelPieces ();
		}
	}
}
#endif