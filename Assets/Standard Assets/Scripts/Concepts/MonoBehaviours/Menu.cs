using UnityEngine;

namespace AmbitiousSnake
{
	public class Menu : MonoBehaviour
	{
		public Transform trs;

		void OnEnable ()
		{
			for (int i = 0; i < VR_UIPointer.instances.Length; i ++)
			{
				VR_UIPointer vR_UIPointer = VR_UIPointer.instances[i];
				vR_UIPointer.uiPlaneTrs = trs;
			}
		}

		void OnDisable ()
		{
			for (int i = 0; i < VR_UIPointer.instances.Length; i ++)
			{
				VR_UIPointer vR_UIPointer = VR_UIPointer.instances[i];
				vR_UIPointer.uiPlaneTrs = GameplayMenu.instance.trs;
			}
		}
	}
}