#if PROBUILDER
using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace AmbitiousSnake
{
	public class ProceduralLevelPiece : Spawnable
	{
		public ProceduralLevel level;
		public List<Tile> tiles = new List<Tile>();
		public List<Path> paths = new List<Path>();
		public List<ProceduralLevelPiece> neighbors = new List<ProceduralLevelPiece>();
		public ProceduralLevelPiece[] children = new ProceduralLevelPiece[0];
		public FloatRange difficultyRange;
		public SerializableDictionary<Renderer, MaterialGroup> alternateMaterialGroupsDict = new SerializableDictionary<Renderer, MaterialGroup>();
		public ProceduralLevelPieceReorientTransform[] reorientTransforms = new ProceduralLevelPieceReorientTransform[0];
		public ProceduralLevelPieceToggleGameObjects[] toggleGameObjects = new ProceduralLevelPieceToggleGameObjects[0];
		[HideInInspector]
		public bool isInitialized;
#if UNITY_EDITOR
		public bool shouldReorientTransforms;
		public bool shouldToggleGameObjects;
#endif

		public override void Start ()
		{
			base.Start ();
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				level = GetComponentInParent<ProceduralLevel>();
				Tile[] _tiles = GetComponentsInChildren<Tile>(true);
				if (_tiles.Length > 0)
					tiles = new List<Tile>(_tiles);
				children = GetComponentsInChildren<ProceduralLevelPiece>().Remove(this);
				float[] pathDifficulties = new float[paths.Count];
				for (int i = 0; i < paths.Count; i ++)
				{
					Path path = paths[i];
					pathDifficulties[i] = path.difficulty;
				}
				difficultyRange = new FloatRange(Mathf.Min(pathDifficulties), Mathf.Max(pathDifficulties));
				return;
			}
#endif
		}

		public virtual void Init ()
		{
			alternateMaterialGroupsDict.Init ();
			foreach (KeyValuePair<Renderer, MaterialGroup> keyValuePair in alternateMaterialGroupsDict)
				keyValuePair.Key.sharedMaterial = keyValuePair.Value.materials[level.alternateAreaIndex];
			isInitialized = true;
		}

		void OnValidate ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				if (shouldReorientTransforms)
				{
					shouldReorientTransforms = false;
					ReorientTransforms ();
				}
				if (shouldToggleGameObjects)
				{
					shouldToggleGameObjects = false;
					ToggleGameObjects ();
				}
				return;
			}
#endif
		}

		public void ReorientTransforms ()
		{
			for (int i = 0; i < reorientTransforms.Length; i ++)
			{
				ProceduralLevelPieceReorientTransform reorientTransform = reorientTransforms[i];
				reorientTransform.Do ();
			}
		}

		public void ToggleGameObjects ()
		{
			for (int i = 0; i < toggleGameObjects.Length; i ++)
			{
				ProceduralLevelPieceToggleGameObjects toggleGameObject = toggleGameObjects[i];
				toggleGameObject.Do ();
			}
		}

		public static bool AreOverlapping (ProceduralLevelPiece levelPiece, ProceduralLevelPiece levelPiece2, BoundsOffset? tileBoundsOffset = null, BoundsOffset? tile2BoundsOffset = null)
		{
			for (int i = 0; i < levelPiece.tiles.Count; i ++)
			{
				Tile tile = levelPiece.tiles[i];
				for (int i2 = 0; i2 < levelPiece2.tiles.Count; i2 ++)
				{
					Tile tile2 = levelPiece2.tiles[i2];
					if (tile != tile2 && Tile.AreOverlapping(tile, tile2, tileBoundsOffset, tile2BoundsOffset))
						return true;
				}
			}
			return false;
		}

		[Serializable]
		public struct Path
		{
			public Transform[] transforms;
			public bool isReversable;
			public float difficulty;
		}

		[Serializable]
		public struct MaterialGroup
		{
			public Material[] materials;
		}
	}
}
#endif