#if PROBUILDER
using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace AmbitiousSnake
{
	public class ProceduralEndlessRacingLevelPiece : ProceduralLevelPiece
	{
		public ProceduralEndlessRacingLevel racingLevel;
		// public MultiplayerSnakeSpawnPoint[] snakeSpawnPoints = new MultiplayerSnakeSpawnPoint[0];
		public SerializableDictionary<byte, SnakeSpawnPointGroup> snakeSpawnPointGroupsDict = new SerializableDictionary<byte, SnakeSpawnPointGroup>();
		// public ProceduralEndlessRacingLevelPieceConnectPoint[] connectionPoints = new ProceduralEndlessRacingLevelPieceConnectPoint[0];
		public ProceduralEndlessRacingLevelPieceConnectPoint startConnectPoint;
		public ProceduralEndlessRacingLevelPieceConnectPoint endConnectPoint;
		public ProceduralEndlessRacingLevelCheckpoint[] checkpoints = new ProceduralEndlessRacingLevelCheckpoint[0];
        public bool hasCheckpointThatCantSeeSkybox;
		public BoxCollider boundsCollider;
		public bool isStartingPiece;

		public override void Start ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				// connectionPointsDict.Clear();
				// for (int i = 0; i < connectionPoints.Length; i ++)
				// {
				// 	ProceduralEndlessRacingLevelPieceConnectPoint connectionPoint = connectionPoints[i];
				// 	List<ProceduralEndlessRacingLevelPiece> canConnectTo = new List<ProceduralEndlessRacingLevelPiece>(connectionPoint.canConnectTo);
				// 	for (int i2 = 0; i2 < canConnectTo.Count; i2 ++)
				// 	{
				// 		ProceduralEndlessRacingLevelPiece levelPiece = canConnectTo[i2];
				// 		connectionPointsDict[levelPiece] = ;
				// 	}
				// }
				return;
			}
#endif
		}

		public override void Init ()
		{
			snakeSpawnPointGroupsDict.Init ();
			alternateMaterialGroupsDict.Init ();
			foreach (KeyValuePair<Renderer, MaterialGroup> keyValuePair in alternateMaterialGroupsDict)
				keyValuePair.Key.sharedMaterial = keyValuePair.Value.materials[racingLevel.alternateAreaIndex];
			isInitialized = true;
		}

		[Serializable]
		public struct SnakeSpawnPointGroup
		{
			public MultiplayerSnakeSpawnPoint[] snakeSpawnPoints;
		}
	}
}
#endif