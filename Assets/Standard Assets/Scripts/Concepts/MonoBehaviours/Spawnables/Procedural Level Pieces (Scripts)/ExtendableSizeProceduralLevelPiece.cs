#if PROBUILDER
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace AmbitiousSnake
{
	public class ExtendableSizeProceduralLevelPiece : ProceduralLevelPiece
	{
		public Tile[] lastAddedTiles = new Tile[0];
		public Path[] lastAddedPaths = new Path[0];
		public Vector3 extendDirection;
		public bool isLocalDirection;
		[HideInInspector]
		public int timesExtended;

		public override void Start ()
		{
			base.Start ();
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				lastAddedTiles = tiles.ToArray();
				for (int i = 0; i < lastAddedTiles.Length; i ++)
				{
					Tile lastAddedTile = lastAddedTiles[i];
					int indexOfSuffix = lastAddedTile.name.IndexOf(" (");
					if (indexOfSuffix != -1)
						lastAddedTile.name = lastAddedTile.name.Remove(indexOfSuffix);
				}
				lastAddedPaths = paths.ToArray();
				return;
			}
#endif
		}

		public Vector3 GetExtendDirection (Vector3? extendDirection = null, bool? isLocalDirection = null)
		{
			if (extendDirection == null)
				extendDirection = this.extendDirection;
			if (isLocalDirection == null)
				isLocalDirection = this.isLocalDirection;
			if ((bool) isLocalDirection)
				return trs.rotation * (Vector3) extendDirection;
			else
				return (Vector3) extendDirection;
		}

		public bool CanExtendInDirection (Vector3? extendDirection = null, bool? isLocalDirection = null)
		{
			extendDirection = GetExtendDirection(extendDirection, isLocalDirection);
			for (int i2 = 0; i2 < level.levelPieces.Count; i2 ++)
			{
				ProceduralLevelPiece levelPiece = level.levelPieces[i2];
				if (AreOverlapping(this, levelPiece, new BoundsOffset(Vector3.one / 2 + (Vector3) extendDirection, (Vector3) extendDirection)))
					return false;
			}
			return true;
		}

		public void ExtendInDirection (Vector3? extendDirection = null, bool? isLocalDirection = null)
		{
			extendDirection = GetExtendDirection(extendDirection, isLocalDirection);
			Dictionary<Vector3, Transform> tileTrsPositionsDict = new Dictionary<Vector3, Transform>();
			for (int i = 0; i < tiles.Count; i ++)
			{
				Tile tile = tiles[i];
				if (tile == null)
				{
					tiles.RemoveAt(i);
					i --;
					continue;
				}
				Vector3[] pointsInsideTile = tile.meshRenderer.bounds.GetPointsInside(Vector3.one, new BoundsOffset(Vector3.one / 2, Vector3.zero));
				for (int i2 = 0; i2 < pointsInsideTile.Length; i2 ++)
				{
					Vector3 pointInsideTile = pointsInsideTile[i2];
					tileTrsPositionsDict.Add(pointInsideTile, tile.trs);
				}
			}
			List<Tile> addedTiles = new List<Tile>();
			List<Tile> _lastAddedTiles = new List<Tile>(lastAddedTiles);
			for (int i = 0; i < _lastAddedTiles.Count; i ++)
			{
				Tile tile = _lastAddedTiles[i];
				Vector3[] pointsInsideTile = tile.meshRenderer.bounds.GetPointsInside(Vector3.one, new BoundsOffset(Vector3.one / 2, Vector3.zero));
				for (int i2 = 0; i2 < pointsInsideTile.Length; i2 ++)
				{
					Vector3 point = pointsInsideTile[i2] + (Vector3) extendDirection;
					if (!tileTrsPositionsDict.ContainsKey(point))
					{
						ProceduralLevel.SpawnEntry spawnEntry = level.spawnEntriesDict[tile.name];
						Transform newTrs = spawnEntry.Spawn(point);
						Tile newTile = newTrs.GetComponent<Tile>();
						tileTrsPositionsDict.Add(point, newTrs);
						addedTiles.Add(newTile);
						if (level.TryToMergeTransformWithNeighbors(newTrs, ref tileTrsPositionsDict, ref level.tiles))
						{
							int indexOfNull;
							do
							{
								indexOfNull = _lastAddedTiles.IndexOf(null);
								if (indexOfNull != -1)
								{
									if (indexOfNull <= i)
										i --;
									_lastAddedTiles.RemoveAt(indexOfNull);
									tiles.Remove(null);
									addedTiles.Remove(null);
								}
							} while (indexOfNull != -1);
						}
					}
				}
			}
			tiles.AddRange(addedTiles);
			level.tiles.AddRange(addedTiles);
			lastAddedTiles = addedTiles.ToArray();
			timesExtended ++;
		}
	}
}
#endif