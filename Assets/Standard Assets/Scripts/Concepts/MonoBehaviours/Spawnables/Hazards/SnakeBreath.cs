using UnityEngine;
using System.Collections.Generic;

namespace AmbitiousSnake
{
	public class SnakeBreath : Hazard
	{
		[Tooltip("particleSystemWithTrigger")]
		public ParticleSystem particleSystemWithTrigger;
		[Tooltip("particleSystemWithCollision")]
		public ParticleSystem particleSystemWithCollision;
		
		void OnParticleCollision (GameObject go)
		{
			List<ParticleCollisionEvent> collisions = new List<ParticleCollisionEvent>();
			int collisionCount = particleSystemWithCollision.GetCollisionEvents(go, collisions);
			for (int i = 0; i < collisionCount; i ++)
			{
				ParticleCollisionEvent collision = collisions[i];
				IDestructable destructable = collision.colliderComponent.GetComponentInParent<IDestructable>();
				if (destructable != null && !(destructable is Snake))
					ApplyDamage (destructable, damage);
			}
		}
	}
}