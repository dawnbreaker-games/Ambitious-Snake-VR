#if PROBUILDER
using System;
using Extensions;
using UnityEngine;
using System.Collections;
using UnityEngine.ProBuilder;
using System.Collections.Generic;
using UnityEngine.ProBuilder.MeshOperations;
using Random = UnityEngine.Random;
using Object = UnityEngine.Object;
using static ThreadingUtilities;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace AmbitiousSnake
{
	public class ProceduralLevel : Level, ISpawnable
	{
		public new static ProceduralLevel instance;
		public new static ProceduralLevel Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<ProceduralLevel>();
				return instance;
			}
		}
		public int prefabIndex;
		public int PrefabIndex
		{
			get
			{
				return prefabIndex;
			}
			set
			{
				prefabIndex = value;
			}
		}
		public Spikes spikesPrefab;
		public Turret turretPrefab;
		public Tile wallTilePrefab;
		public AcceleratorTile acceleratorTilePrefab;
		public DissolveTile dissolveTilePrefab;
		public StickyTile stickyTilePrefab;
		public BouncyTile bouncyTilePrefab;
		public RecorderTile recorderTilePrefab;
		public PlaybackTile playbackTilePrefab;
		public FollowWaypoints followWaypointsPrefab;
		public Transform suspensionRodTrsPrefab;
		public TestSnake testSnakePrefab;
		public Transform trs;
		public Transform boundariesTrs;
		public TileRendererMaterial[] tileRendererMaterials = new TileRendererMaterial[0];
		public List<Tile> tiles = new List<Tile>();
		public List<ProceduralLevelPiece> levelPieces = new List<ProceduralLevelPiece>();
		[Header("Spawn Entries")]
		public SpawnEntry[] spawnEntries = new SpawnEntry[0];
		public LevelPieceSpawnEntry[] levelPieceSpawnEntries = new LevelPieceSpawnEntry[0];
		public ExtendableSizeLevelPieceSpawnEntry[] extendableSizeLevelPieceSpawnEntries = new ExtendableSizeLevelPieceSpawnEntry[0];
		public Dictionary<string, SpawnEntry> spawnEntriesDict = new Dictionary<string, SpawnEntry>();
		[Header("Make Rules")]
		public List<MakeRule> makeRules = new List<MakeRule>();
		public ObjectsMustTouch objectsMustTouch;
		public PiecesCantOverlap piecesCantOverlap;
		[Header("Rewards")]
		public Test.Result.Reward diedReward;
		public Test.Result.Reward didMovementReward;
		public Test.Result.Reward survivedReward;
		public Test.Result.Reward timedOutReward;
		public Test.Result.Reward wonLevelReward;
		[HideInInspector]
		public List<SpawnEntry> _spawnEntries = new List<SpawnEntry>();
		public int alternateMaterialCount;
		[HideInInspector]
		public int alternateAreaIndex;
		public AudioSource musicAudioSource;
		public AudioClip[] alternateAreaMusics = new AudioClip[0];

		public override void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				if (trs == null)
					trs = GetComponent<Transform>();
				return;
			}
#endif
			base.OnEnable ();
			Init ();
			instance = this;
		}

		public void Init ()
		{
			alternateAreaIndex = Random.Range(0, alternateMaterialCount);
			musicAudioSource.clip = alternateAreaMusics[alternateAreaIndex];
			for (int i = 0; i < tileRendererMaterials.Length; i ++)
			{
				TileRendererMaterial tileRendererMaterial = tileRendererMaterials[i];
				tileRendererMaterial.Do ();
			}
			spawnEntriesDict.Clear();
			_spawnEntries.Clear();
			for (int i = 0; i < extendableSizeLevelPieceSpawnEntries.Length; i ++)
			{
				ExtendableSizeLevelPieceSpawnEntry extendableSizeLevelPieceSpawnEntry = extendableSizeLevelPieceSpawnEntries[i];
				// if (extendableSizeLevelPieceSpawnEntry.chance > 0)
				// {
					// _extendableSizeLevelPieceSpawnEntries.Add(extendableSizeLevelPieceSpawnEntry);
					// _levelPieceSpawnEntries.Add(extendableSizeLevelPieceSpawnEntry);
					_spawnEntries.Add(extendableSizeLevelPieceSpawnEntry);
					spawnEntriesDict.Add(extendableSizeLevelPieceSpawnEntry.trsPrefab.name, extendableSizeLevelPieceSpawnEntry);
				// }
			}
			for (int i = 0; i < levelPieceSpawnEntries.Length; i ++)
			{
				LevelPieceSpawnEntry levelPieceSpawnEntry = levelPieceSpawnEntries[i];
				// if (levelPieceSpawnEntry.chance > 0)
				// {
					// _levelPieceSpawnEntries.Add(levelPieceSpawnEntry);
					_spawnEntries.Add(levelPieceSpawnEntry);
					spawnEntriesDict.Add(levelPieceSpawnEntry.trsPrefab.name, levelPieceSpawnEntry);
				// }
			}
			for (int i = 0; i < spawnEntries.Length; i ++)
			{
				SpawnEntry spawnEntry = spawnEntries[i];
				// if (spawnEntry.chance > 0)
				// {
					_spawnEntries.Add(spawnEntry);
					spawnEntriesDict.Add(spawnEntry.trsPrefab.name, spawnEntry);
				// }
			}
			makeRules.Clear();
			makeRules.Add(objectsMustTouch);
			makeRules.Add(piecesCantOverlap);
			tiles = new List<Tile>(GetComponentsInChildren<Tile>(true));
		}

		bool IsValid ()
		{
			makeRules.Sort(MakeRuleSorter);
			for (int i = 0; i < makeRules.Count; i ++)
			{
				MakeRule makeRule = makeRules[i];
				if (!makeRule.IsValid(this))
					return false;
			}
			return true;
		}

		int MakeRuleSorter (MakeRule m1, MakeRule m2)
		{
			if (m1.priority > m2.priority)
				return 1;
			else if (m1.priority < m2.priority)
				return -1;
			else
				return 0;
		}

		public static IEnumerator<ProceduralLevel> MakeLevel (BoundsInt boundsInt, SpawnEntry[] spawnEntries)
		{
			ProceduralLevel level = ObjectPool.Instance.SpawnComponent<ProceduralLevel>(ProceduralLevel.Instance.prefabIndex);
			level.enabled = false;
			level.spawnEntries = spawnEntries;
			level.Init ();
			level.boundariesTrs.localScale = boundsInt.size;
			if (boundsInt.size.x % 2 == 0)
				level.boundariesTrs.position = level.boundariesTrs.position.SetX(0.5f);
			if (boundsInt.size.y % 2 == 0)
				level.boundariesTrs.position = level.boundariesTrs.position.SetY(0.5f);
			if (boundsInt.size.z % 2 == 0)
				level.boundariesTrs.position = level.boundariesTrs.position.SetZ(0.5f);
			Dictionary<Vector3, Transform> allTransforms = new Dictionary<Vector3, Transform>();
			List<Vector3> positions = new List<Vector3>(boundsInt.ToBounds().GetPointsInside(Vector3.one, new BoundsOffset(Vector3.one / 2, Vector3.zero)));
			while (positions.Count > 0)
			{
				// int indexOfNull;
				// do
				// {
				// 	indexOfNull = level.tiles.IndexOf(null);
				// 	if (indexOfNull != -1)
				// 		level.tiles.RemoveAt(indexOfNull);
				// } while (indexOfNull != -1);
				int positionIndex = Random.Range(0, positions.Count);
				Vector3 position = positions[positionIndex];
				SpawnEntry spawnEntry;
				do
				{
					spawnEntry = level._spawnEntries[Random.Range(0, level._spawnEntries.Count)];
				} while (Random.value > spawnEntry.chance);
				Transform trs = spawnEntry.Spawn(position);
				trs.SetParent(level.trs);
				LevelPieceSpawnEntry levelPieceSpawnEntry = spawnEntry as LevelPieceSpawnEntry;
				if (levelPieceSpawnEntry != null)
				{
					ProceduralLevelPiece levelPiece = trs.GetComponent<ProceduralLevelPiece>();
					levelPiece.level = level;
					List<Tile> addedTiles = new List<Tile>(levelPiece.tiles);
					List<ProceduralLevelPiece> addedPieces = new List<ProceduralLevelPiece>();
					addedPieces.Add(levelPiece);
					List<ProceduralLevelPiece> childrenPieces = new List<ProceduralLevelPiece>(levelPiece.children);
					while (childrenPieces.Count > 0)
					{
						ProceduralLevelPiece childPiece = childrenPieces[0];
						childPiece.level = level;
						addedTiles.AddRange(childPiece.tiles);
						addedPieces.Add(childPiece);
						childrenPieces.AddRange(childPiece.children);
						childrenPieces.RemoveAt(0);
					}
					level.tiles.AddRange(addedTiles);
					level.levelPieces.AddRange(addedPieces);
					if (!level.piecesCantOverlap.IsValid(level))
					{
						DestroyImmediate(levelPiece.gameObject);
						level.tiles = level.tiles.TrimEnd(addedTiles.Count);
						level.levelPieces = level.levelPieces.TrimEnd(addedPieces.Count);
						continue;
					}
					HandleExtendLevelPiece (levelPiece);
					for (int i = 0; i < levelPiece.tiles.Count; i ++)
					{
						Tile tile = levelPiece.tiles[i];
						// if (tile == null)
						// {
						// 	levelPiece.tiles.RemoveAt(i);
						// 	i --;
						// 	continue;
						// }
						if (level.TryToMergeTransformWithNeighbors(tile.trs, ref allTransforms, ref level.tiles))
						{
							yield return null;
							DeleteProBuilderMeshSharedFaces.Do (tile.trs.GetComponentInChildren<ProBuilderMesh>());
						}
						else
							allTransforms[tile.trs.position] = tile.trs;
						yield return null;
					}
				}
				else
				{
					Tile tile = trs.GetComponent<Tile>();
					if (tile != null)
						level.tiles.Add(tile);
					if (level.TryToMergeTransformWithNeighbors(trs, ref allTransforms, ref level.tiles))
					{
						yield return null;
						DeleteProBuilderMeshSharedFaces.Do (trs.GetComponentInChildren<ProBuilderMesh>());
					}
					else
						allTransforms[position] = trs;
				}
				positions.RemoveAt(positionIndex);
				yield return null;
			}
			yield return level;
		}

		static void HandleExtendLevelPiece (ProceduralLevelPiece levelPiece)
		{
			ExtendableSizeProceduralLevelPiece extendableSizeLevelPiece = levelPiece as ExtendableSizeProceduralLevelPiece;
			if (extendableSizeLevelPiece != null)
			{
				ExtendableSizeLevelPieceSpawnEntry extendableSizeLevelPieceSpawnEntry = (ExtendableSizeLevelPieceSpawnEntry) levelPiece.level.spawnEntriesDict[extendableSizeLevelPiece.name];
				HandleExtendExtendableSizeLevelPiece (extendableSizeLevelPiece, extendableSizeLevelPieceSpawnEntry.maxTimesToExtend);
			}
			for (int i = 0; i < levelPiece.children.Length; i ++)
			{
				ProceduralLevelPiece childPiece = levelPiece.children[i];
				HandleExtendLevelPiece (childPiece);
			}
		}

		static void HandleExtendExtendableSizeLevelPiece (ExtendableSizeProceduralLevelPiece extendableSizeLevelPiece, int maxTimesToExtend)
		{
			for (int i = 0; i < maxTimesToExtend; i ++)
			{
				if (!extendableSizeLevelPiece.CanExtendInDirection(extendableSizeLevelPiece.GetExtendDirection() * (i + 1)))
				{
					for (int i2 = 0; i2 < i; i2 ++)
					{
						extendableSizeLevelPiece.ExtendInDirection ();
						while (extendableSizeLevelPiece.tiles.Contains(null))
						{
							extendableSizeLevelPiece.tiles.Remove(null);
							extendableSizeLevelPiece.lastAddedTiles.Remove(null);
							extendableSizeLevelPiece.level.tiles.Remove(null);
						}
					}
					return;
				}
			}
		}

		public static IEnumerator<bool> IsGapTraversable (Vector3 gapVector)
		{
			if (gapVector.y > Snake.instance.length.valueRange.max)
				yield return false;
			else if (gapVector.y >= 0)
			{
				if (gapVector.magnitude <= Snake.instance.length.valueRange.max)
					yield return true;
				else
				{
					throw new NotImplementedException();
				}
			}
			else
			{
				throw new NotImplementedException();
			}
		}

		public static IEnumerator IsGapTraversable (Vector3 gapVector, float movementAngleFromGround, float duration = Mathf.Infinity)
		{
			Vector3 move = Vector3.RotateTowards(gapVector.GetXZ(), Vector3.up, movementAngleFromGround * Mathf.Deg2Rad, 0);
			Test test = Test.MakeTestForGap(gapVector);
			test.type = Test.Type.Movement;
			test.destination = gapVector;
			test.duration = duration;
			test.timeScale = 1;
			Test.InputEvent[] inputEvents = new Test.InputEvent[1] { new Test.InputEvent(move.normalized, 1) };
#if UNITY_EDITOR
			EditorCoroutineWithData coroutineWithData = new EditorCoroutineWithData(Instance, test.TestRoutine(inputEvents));
			yield return new WaitForReturnedValueOfType_Editor<Test.Result>(coroutineWithData);
#else
			CoroutineWithData coroutineWithData = new CoroutineWithData(Instance, test.TestRoutine(inputEvents));
			yield return new WaitForReturnedValueOfType<Test.Result>(coroutineWithData);
#endif
			Test.Result result = (Test.Result) coroutineWithData.result;
			yield return result.endEvent == Test.Result.EndEvent.DidMovement;
		}

		public bool TryToMergeTransformWithNeighbors (Transform trs, ref Dictionary<Vector3, Transform> allTransforms, ref List<Tile> tiles)
		{
			bool mergedWithNeighbor = false;
			Transform neighbor;
			if (allTransforms.TryGetValue(trs.position + Vector3.up, out neighbor))
				mergedWithNeighbor |= TryToMergeTransformWithNeighbor (trs, neighbor, ref allTransforms, ref tiles);
			if (allTransforms.TryGetValue(trs.position + Vector3.down, out neighbor))
				mergedWithNeighbor |= TryToMergeTransformWithNeighbor (trs, neighbor, ref allTransforms, ref tiles);
			if (allTransforms.TryGetValue(trs.position + Vector3.left, out neighbor))
				mergedWithNeighbor |= TryToMergeTransformWithNeighbor (trs, neighbor, ref allTransforms, ref tiles);
			if (allTransforms.TryGetValue(trs.position + Vector3.right, out neighbor))
				mergedWithNeighbor |= TryToMergeTransformWithNeighbor (trs, neighbor, ref allTransforms, ref tiles);
			if (allTransforms.TryGetValue(trs.position + Vector3.forward, out neighbor))
				mergedWithNeighbor |= TryToMergeTransformWithNeighbor (trs, neighbor, ref allTransforms, ref tiles);
			if (allTransforms.TryGetValue(trs.position + Vector3.back, out neighbor))
				mergedWithNeighbor |= TryToMergeTransformWithNeighbor (trs, neighbor, ref allTransforms, ref tiles);
			return mergedWithNeighbor;
		}

		bool TryToMergeTransformWithNeighbor (Transform trs, Transform neighbor, ref Dictionary<Vector3, Transform> allTransforms, ref List<Tile> tiles)
		{
			if (trs.name == neighbor.name)
			{
				ProBuilderMesh proBuilderMesh = trs.GetComponentInChildren<ProBuilderMesh>();
				ProBuilderMesh proBuilderMesh2 = neighbor.GetComponentInChildren<ProBuilderMesh>();
				Bounds proBuilderMesh2Bounds = proBuilderMesh2.GetComponent<MeshRenderer>().bounds;
				if (TryToMergeProBuilderMeshes (proBuilderMesh, proBuilderMesh2, proBuilderMesh))
				{
					Vector3[] positions = proBuilderMesh2Bounds.GetPointsInside(Vector3.one, new BoundsOffset(Vector3.one / 2, Vector3.zero));
					for (int i = 0; i < positions.Length; i ++)
					{
						Vector3 position = positions[i];
						allTransforms[position] = trs;
					}
					tiles.Remove(neighbor.GetComponent<Tile>());
					DestroyImmediate(neighbor.gameObject);
					return true;
				}
			}
			return false;
		}

		bool TryToMergeProBuilderMeshes (ProBuilderMesh proBuilderMesh, ProBuilderMesh proBuilderMesh2, ProBuilderMesh targetProBuilderMesh)
		{
			Bounds proBuilderMeshBounds = proBuilderMesh.GetComponent<MeshRenderer>().bounds;
			Bounds proBuilderMesh2Bounds = proBuilderMesh2.GetComponent<MeshRenderer>().bounds;
			Bounds combinedBounds = new Bounds[2] { proBuilderMeshBounds, proBuilderMesh2Bounds }.Combine();
			if (combinedBounds.GetVolume() != proBuilderMeshBounds.GetVolume() + proBuilderMesh2Bounds.GetVolume())
				return false;
			CombineMeshes.Combine(new ProBuilderMesh[2] { proBuilderMesh, proBuilderMesh2 }, targetProBuilderMesh);
			// DestroyImmediate(proBuilderMesh2.gameObject);
			return true;
		}

		[Serializable]
		public class SpawnEntry
		{
			public Transform trsPrefab;
			public int prefabIndex;
			[Range(0, 1)]
			public float chance;

			public virtual Transform Spawn (Vector3 position, Quaternion? rotation = null)
			{
				if (rotation == null)
					rotation = trsPrefab.rotation;
				Transform trs;
				if (prefabIndex == -1)
					trs = Instantiate(trsPrefab, position, (Quaternion) rotation);
				else
					trs = ObjectPool.instance.SpawnComponent<Transform>(prefabIndex, position, (Quaternion) rotation);
				trs.name = trs.name.RemoveStartAt("(Clone)");
				return trs;
			}
		}

		[Serializable]
		public class LevelPieceSpawnEntry : SpawnEntry
		{
			public ProceduralLevelPiece levelPiecePrefab;
			public Vector3[] allowedRotations = new Vector3[0];

			public override Transform Spawn (Vector3 position, Quaternion? rotation = null)
			{
				if (rotation == null || !allowedRotations.Contains(((Quaternion) rotation).eulerAngles))
					rotation = Quaternion.Euler(allowedRotations[Random.Range(0, allowedRotations.Length)]);
				return base.Spawn(position, rotation);
			}
		}

		[Serializable]
		public class ExtendableSizeLevelPieceSpawnEntry : LevelPieceSpawnEntry
		{
			public int maxTimesToExtend;
		}

		[Serializable]
		public struct Test
		{
			public ProceduralLevel level;
			public Type type;
			public Transform trs;
			public Vector3 destination;
			public float duration;
			public float timeScale;
			public TestSnake tester;
			public Dictionary<Result.EndEvent, Result.Reward> endRewards;
			public float StartTime { get; private set; }
			List<Result> results;
			Result? endResult;

			public Test (Test test) : this(test.level, test.type, test.trs, test.destination, test.duration, test.timeScale, test.tester, test.endRewards)
			{
			}

			public Test (ProceduralLevel level = null, Type type = Type.Movement, Transform trs = null, Vector3 destination = new Vector3(), float duration = Mathf.Infinity, float timeScale = 100, TestSnake tester = null, Dictionary<Result.EndEvent, Result.Reward> endRewards = null)
			{
				if (level == null)
					level = ProceduralLevel.Instance;
				this.level = level;
				this.type = type;
				if (trs == null)
					// trs = new GameObject().GetComponent<Transform>();
					trs = level.trs;
				this.trs = trs;
				this.destination = destination;
				this.duration = duration;
				this.timeScale = timeScale;
				this.tester = tester;
				if (endRewards == null)
				{
					endRewards = new Dictionary<Result.EndEvent, Result.Reward>();
					endRewards.Add(Result.EndEvent.Died, level.diedReward);
					endRewards.Add(Result.EndEvent.DidMovement, level.didMovementReward);
					endRewards.Add(Result.EndEvent.Survived, level.survivedReward);
					endRewards.Add(Result.EndEvent.TimedOut, level.timedOutReward);
					endRewards.Add(Result.EndEvent.WonLevel, level.wonLevelReward);
				}
				this.endRewards = endRewards;
				StartTime = Time.time;
				results = new List<Result>();
				endResult = null;
			}

			public IEnumerator<Result?> TestRoutine (InputEvent[] inputEvents)
			{
				Time.timeScale = timeScale;
				StartTime = Time.time;
				tester.inputEvents = inputEvents;
				tester.testIAmDoing = this;
				tester.enabled = true;
				while (tester.testIAmDoing.endResult == null)
					yield return tester.testIAmDoing.endResult;
				yield return tester.testIAmDoing.endResult;
			}

			public void End (Result.EndEvent endEvent)
			{
				End (endEvent, endRewards[endEvent]);
			}

			void End (Result.EndEvent endEvent, Result.Reward reward)
			{
				Result result = new Result();
				result.timePassed = Time.time - StartTime;
				result.isEnd = true;
				result.endEvent = endEvent;
				result.reward = reward;
				reward.GiveTo (tester);
				endResult = result;
			}

			public static IEnumerator MakeTestForRandomLevel (BoundsInt boundsInt, SpawnEntry[] spawnEntries)
			{
#if UNITY_EDITOR
				EditorCoroutineWithData coroutineWithData = new EditorCoroutineWithData(Instance, ProceduralLevel.MakeLevel(boundsInt, spawnEntries));
				yield return new WaitForReturnedValueOfType_Editor<ProceduralLevel>(coroutineWithData);
#else
				CoroutineWithData coroutineWithData = new CoroutineWithData(Instance, ProceduralLevel.MakeLevel(boundsInt, spawnEntries));
				yield return new WaitForReturnedValueOfType<ProceduralLevel>(coroutineWithData);
#endif
				ProceduralLevel level = (ProceduralLevel) coroutineWithData.result;
				Test output = new Test(level);
				output.tester = ObjectPool.instance.SpawnComponent<TestSnake>(level.testSnakePrefab.prefabIndex);
				output.tester.trs.SetParent(output.trs);
				yield return output;
			}

			public static Test MakeTestForGap (Vector3 gapVector)
			{
				ProceduralLevel level = ObjectPool.Instance.SpawnComponent<ProceduralLevel>(ProceduralLevel.Instance.prefabIndex);
				Test output = new Test(level);
				Vector3 spawnPosition = new Vector3(-1, 0, -1) * SnakePiece.RADIUS;
				Quaternion spawnRotation = Quaternion.LookRotation(gapVector.GetXZ());
				output.tester = ObjectPool.Instance.SpawnComponent<TestSnake>(level.testSnakePrefab.prefabIndex, spawnPosition, spawnRotation);
				level.boundariesTrs.localScale = gapVector + Vector3.one * output.tester.length.valueRange.max * 2;
				Tile wallTile1 = Instantiate(level.wallTilePrefab, -new Vector3(Mathf.Sign(gapVector.x), 2, Mathf.Sign(gapVector.z)) / 2, Quaternion.identity);
				Tile wallTile2 = Instantiate(level.wallTilePrefab, gapVector + new Vector3(Mathf.Sign(gapVector.x), -2, Mathf.Sign(gapVector.z)) / 2, Quaternion.identity);
				output.tester.trs.SetParent(output.trs);
				wallTile1.trs.SetParent(output.trs);
				wallTile2.trs.SetParent(output.trs);
				level.tiles = new List<Tile>() { wallTile1, wallTile2 };
				return output;
			}

			[Serializable]
			public struct InputEvent
			{
				public Vector3 move;
				public float changeLength;
				public float time;

				public InputEvent (Vector3 move = new Vector3(), float changeLength = 0, float time = 0)
				{
					this.move = move;
					this.changeLength = changeLength;
					this.time = time;
				}
			}
			
			public enum Type
			{
				Movement,
				Survival,
				WinLevel
			}

			public struct Result
			{
				public float timePassed;
				public bool isEnd;
				public EndEvent endEvent;
				public Reward reward;

				public Result (float timePassed, bool isEnd, EndEvent endEvent, Reward reward)
				{
					this.timePassed = timePassed;
					this.isEnd = isEnd;
					this.endEvent = endEvent;
					this.reward = reward;
				}

				[Serializable]
				public struct Reward
				{
					public float reward;
					public Type type;

					public void GiveTo (TestSnake testSnake)
					{
						if (type == Type.Add)
						{
							// testSnake.agent.AddReward(reward);
							testSnake.totalReward += reward;
						}
						else// if (type == Type.Set)
						{
							// testSnake.agent.SetReward(reward);
							testSnake.totalReward = reward;
						}
						print(testSnake.totalReward);
					}

					public enum Type
					{
						Add,
						Set
					}
				}

				public enum EndEvent
				{
					Undetermined,
					Died,
					DidMovement,
					Survived,
					TimedOut,
					WonLevel
				}
			}
		}
		
		public class MakeRule
		{
			public int priority;

			public virtual bool IsValid (ProceduralLevel proceduralLevel)
			{
				throw new NotImplementedException();
			}
		}

		[Serializable]
		public class ObjectsMustTouch : MakeRule
		{
			public override bool IsValid (ProceduralLevel proceduralLevel)
			{
				Tile[] tiles = proceduralLevel.tiles.ToArray();
				UpdateTiles.Do (tiles);
				if (Tile.GetConnectedGroup(tiles[0]).Length == tiles.Length)
					return true;
				else
					return false;
			}
		}

		[Serializable]
		public class LevelMustBePossible : MakeRule
		{
			public override bool IsValid (ProceduralLevel proceduralLevel)
			{
				throw new NotImplementedException();
			}
		}

		[Serializable]
		public class LevelMustBeInDifficultyRange : MakeRule
		{
			public FloatRange difficultyRange;

			public override bool IsValid (ProceduralLevel proceduralLevel)
			{
				throw new NotImplementedException();
			}
		}

		[Serializable]
		public class LevelMustBeAboveCertainFairness : MakeRule
		{
			public float minFairness;

			public override bool IsValid (ProceduralLevel proceduralLevel)
			{
				throw new NotImplementedException();
			}
		}

		[Serializable]
		public class PiecesCantOverlap : MakeRule
		{
			public override bool IsValid (ProceduralLevel proceduralLevel)
			{
				return IsValid(proceduralLevel.levelPieces.ToArray());
			}

			public bool IsValid (ProceduralLevelPiece[] levelPieces)
			{
				for (int i = 0; i < levelPieces.Length; i ++)
				{
					ProceduralLevelPiece levelPiece = levelPieces[i];
					for (int i2 = i + 1; i2 < levelPieces.Length; i2 ++)
					{
						ProceduralLevelPiece levelPiece2 = levelPieces[i2];
						if (ProceduralLevelPiece.AreOverlapping(levelPiece, levelPiece2))
							return false;
					}
				}
				return true;
			}
		}
	}
}
#endif