using UnityEngine;
using System;
using Extensions;
using System.Collections.Generic;
using Random = UnityEngine.Random;
using TMPro;

namespace AmbitiousSnake
{
	public class SurvivalLevel : Level
	{
		public EnemySpawnEntry[] enemySpawnEntries = new EnemySpawnEntry[0];
		public int currentDifficulty;
		public int difficultyPerWave;
		public int currentWave;
		public TMP_Text currentWaveText;
		public TMP_Text highestWaveReachedText;
		int enemiesRemaining;

		public override void Awake ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			base.Awake ();
			highestWaveReachedText.text = "Best wave: " + SaveAndLoadManager.saveData.highestWaveReached;
			NextWave ();
		}

		void NextWave ()
		{
			currentWave ++;
			currentWaveText.text = "Wave: " + currentWave;
			if (currentWave > SaveAndLoadManager.saveData.highestWaveReached)
			{
				SaveAndLoadManager.saveData.highestWaveReached = (uint) currentWave;
				highestWaveReachedText.text = "Best wave: " + currentWave;
			}
			currentDifficulty += difficultyPerWave;
			float remainingDifficulty = currentDifficulty;
			List<EnemySpawnEntry> remainingEnemySpawnEntries = new List<EnemySpawnEntry>(enemySpawnEntries);
			while (remainingDifficulty > 0)
			{
				int enemySpawnEntryIndex = Random.Range(0, remainingEnemySpawnEntries.Count);
				EnemySpawnEntry enemySpawnEntry = remainingEnemySpawnEntries[enemySpawnEntryIndex];
				if (enemySpawnEntry.difficulty <= remainingDifficulty)
				{
					remainingDifficulty -= enemySpawnEntry.difficulty;
					SpawnEnemy (enemySpawnEntry);
				}
				else
				{
					remainingEnemySpawnEntries.RemoveAt(enemySpawnEntryIndex);
					if (remainingEnemySpawnEntries.Count == 0)
						return;
				}
			}
		}

		Enemy SpawnEnemy (EnemySpawnEntry enemySpawnEntry)
		{
			Vector3 spawnPosition;
			do
			{
				BoxCollider spawnBoxCollider = enemySpawnEntry.spawnBoxColliders[Random.Range(0, enemySpawnEntry.spawnBoxColliders.Length)];
				spawnPosition = spawnBoxCollider.bounds.RandomPoint();
			} while (Snake.instance.GetDistanceSqrToClosestPiece(spawnPosition) < enemySpawnEntry.minSpawnDistanceToSnake * enemySpawnEntry.minSpawnDistanceToSnake);
			Enemy enemy = ObjectPool.instance.SpawnComponent<Enemy>(enemySpawnEntry.enemyPrefab, spawnPosition);
			enemy.onDeath += OnDeath;
			enemy.trs.forward = (Snake.instance.TailPosition - enemy.eyesTrs.position).GetXZ();
			enemy.WakeUp ();
			enemiesRemaining ++;
			return enemy;
		}
		
		void OnDeath ()
		{
			enemiesRemaining --;
			if (enemiesRemaining == 0)
				NextWave ();
		}

		[Serializable]
		public struct EnemySpawnEntry
		{
			public Enemy enemyPrefab;
			public float difficulty;
			public BoxCollider[] spawnBoxColliders;
			public float minSpawnDistanceToSnake;
		}
	}
}