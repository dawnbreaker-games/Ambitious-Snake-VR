using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace AmbitiousSnake
{
	public class MultiplayerLevel : Level
	{
		public new static MultiplayerLevel instance;
		public new static MultiplayerLevel Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<MultiplayerLevel>();
				return instance;
			}
		}
		public MultiplayerSnake multiplayerSnakePrefab;
		public MultiplayerSnakeSpawnPoint[] spawnPoints = new MultiplayerSnakeSpawnPoint[0];
		public static bool isPlaying;
		public static bool isWaitingForAnotherPlayer;

		public override void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			base.OnEnable ();
			instance = this;
		}

		public MultiplayerSnake SpawnSnake (MultiplayerSnakeSpawnPoint spawnPoint, ushort id)
		{
			MultiplayerSnake output = ObjectPool.instance.SpawnComponent<MultiplayerSnake>(multiplayerSnakePrefab.prefabIndex, spawnPoint.trs.position, spawnPoint.trs.rotation);
			output.id = id;
			return output;
		}
	}
}