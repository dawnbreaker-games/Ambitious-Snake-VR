#if PROBUILDER
using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace AmbitiousSnake
{
	public class ProceduralEndlessRacingLevel : MultiplayerLevel
	{
		public static new ProceduralEndlessRacingLevel instance;
		public LevelPieceGroup levelPiecesGroup;
		// public static MultiplayerSnake furthestSnake;
		// public static MultiplayerSnake leastFarSnake;
		public static ProceduralEndlessRacingLevelCheckpoint furthestCheckpoint;
		public static List<byte> futureLevelPiecesIndicies = new List<byte>();
		public static List<ProceduralEndlessRacingLevelPiece> madeLevelPieces = new List<ProceduralEndlessRacingLevelPiece>();
		public static List<Bounds> levelPiecesBounds = new List<Bounds>();
		public static ProceduralEndlessRacingLevelPiece furthestLevelPiece;
		const int MIN_LEVEL_PIECES_TO_PLAN_AHEAD = 1;
		ProceduralEndlessRacingLevelPiece furthestLevelPiecePrefab;
		List<ProceduralEndlessRacingLevelCheckpoint> madeCheckpoints = new List<ProceduralEndlessRacingLevelCheckpoint>();
		int furthestDeactivatedLevelPieceIndex;
		public int alternateMaterialCount;
		[HideInInspector]
		public int alternateAreaIndex;
		public AudioSource musicAudioSource;
		public AudioClip[] alternateAreaMusics = new AudioClip[0];

		public override void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			instance = this;
			alternateAreaIndex = Random.Range(0, alternateMaterialCount);
			musicAudioSource.clip = alternateAreaMusics[alternateAreaIndex];
			furthestLevelPiecePrefab = levelPiecesGroup.startingLevelPieces[Random.Range(0, levelPiecesGroup.startingLevelPieces.Length)];
			furthestLevelPiece = Instantiate(furthestLevelPiecePrefab);
			furthestLevelPiece.ReorientTransforms ();
			furthestLevelPiece.ToggleGameObjects ();
			furthestLevelPiece.racingLevel = this;
			furthestLevelPiece.Init ();
			furthestCheckpoint = furthestLevelPiece.checkpoints[furthestLevelPiece.checkpoints.Length - 1];
			madeLevelPieces.Add(furthestLevelPiece);
			madeCheckpoints.AddRange(furthestLevelPiece.checkpoints);
			levelPiecesBounds.Add(furthestLevelPiece.boundsCollider.bounds);
			base.OnEnable ();
		}

		public byte[] MakeNextLevelPieces ()
		{
			List<byte> output = new List<byte>();
			for (int i = 0; i < futureLevelPiecesIndicies.Count; i ++)
			{
				byte futureLevelPieceIndex = futureLevelPiecesIndicies[i];
				ProceduralEndlessRacingLevelPiece futureLevelPiece = levelPiecesGroup.nonStartingLevelPieces[futureLevelPieceIndex];
				TryToMakeNextLevelPiece (futureLevelPiece, false);
				futureLevelPiecesIndicies.RemoveAt(i);
				i --;
				output.Add(futureLevelPieceIndex);
				if (futureLevelPiece.hasCheckpointThatCantSeeSkybox)
					return output.ToArray();
			}
			int indexOfFurthestLevelPiecePrefabInStartingLevelPieces = levelPiecesGroup.startingLevelPieces.IndexOf(furthestLevelPiecePrefab);
			int indexOfFurthestLevelPiecePrefabInNonStartingLevelPieces = -1;
			if (indexOfFurthestLevelPiecePrefabInStartingLevelPieces == -1)
				indexOfFurthestLevelPiecePrefabInNonStartingLevelPieces = levelPiecesGroup.nonStartingLevelPieces.IndexOf(furthestLevelPiecePrefab);
			byte? levelPieceIndex = null;
			while (output.Count < MIN_LEVEL_PIECES_TO_PLAN_AHEAD || levelPieceIndex == null || !furthestLevelPiece.hasCheckpointThatCantSeeSkybox)
			{
				levelPieceIndex = TryToMakeNextLevelPiece();
				if (levelPieceIndex == null)
				{
					for (int i = 0; i < output.Count; i ++)
					{
						levelPiecesBounds.RemoveAt(levelPiecesBounds.Count - 1);
						ProceduralEndlessRacingLevelPiece levelPiece = madeLevelPieces[madeLevelPieces.Count - 1];
						for (int i2 = 0; i2 < levelPiece.checkpoints.Length; i2 ++)
							madeCheckpoints.RemoveAt(madeCheckpoints.Count - 1);
						Destroy(levelPiece.gameObject);
						madeLevelPieces.RemoveAt(madeLevelPieces.Count - 1);
					}
					output.Clear();
					if (indexOfFurthestLevelPiecePrefabInStartingLevelPieces == -1)
						furthestLevelPiecePrefab = levelPiecesGroup.nonStartingLevelPieces[indexOfFurthestLevelPiecePrefabInNonStartingLevelPieces];
					else
						furthestLevelPiecePrefab = levelPiecesGroup.startingLevelPieces[indexOfFurthestLevelPiecePrefabInStartingLevelPieces];
					furthestLevelPiece = madeLevelPieces[madeLevelPieces.Count - 1];
				}
				else
					output.Add((byte) levelPieceIndex);
			}
			furthestCheckpoint = madeCheckpoints[madeCheckpoints.Count - 1];
			return output.ToArray();
		}

		byte? TryToMakeNextLevelPiece ()
		{
			ProceduralEndlessRacingLevelPiece[] levelPieces = levelPiecesGroup.nonStartingLevelPieces;
			byte levelPieceIndex = (byte) Random.Range(0, levelPieces.Length);
			ProceduralEndlessRacingLevelPiece levelPiece = TryToMakeNextLevelPiece(levelPieces[levelPieceIndex]);
			if (levelPiece == null)
				return null;
			else
				return levelPieceIndex;
		}

		ProceduralEndlessRacingLevelPiece TryToMakeNextLevelPiece (ProceduralEndlessRacingLevelPiece levelPiecePrefab, bool checkIfIntersectsOtherLevelPieces = true)
		{
			ProceduralEndlessRacingLevelPiece levelPiece = Instantiate(levelPiecePrefab);
			levelPiece.ReorientTransforms ();
			levelPiece.ToggleGameObjects ();
			levelPiece.racingLevel = this;
			levelPiece.Init ();
			ProceduralEndlessRacingLevelPieceConnectPoint furthestLevelPieceEndConnectPoint = furthestLevelPiece.endConnectPoint;
			ProceduralEndlessRacingLevelPieceConnectPoint levelPieceStartConnectPoint = levelPiece.startConnectPoint;
			levelPiece.trs.rotation = Quaternion.FromToRotation(levelPieceStartConnectPoint.trs.forward, furthestLevelPieceEndConnectPoint.trs.forward);
			levelPiece.trs.up = Vector3.up;
			levelPiece.trs.position += furthestLevelPieceEndConnectPoint.trs.position - levelPieceStartConnectPoint.trs.position;
			if (checkIfIntersectsOtherLevelPieces)
			{
				for (int i = 0; i < levelPiecesBounds.Count - 1; i ++)
				{
					Bounds levelPieceBounds = levelPiecesBounds[i];
					if (levelPiece.boundsCollider.bounds.Shrink(Vector3.one * Physics.defaultContactOffset).Intersects(levelPieceBounds.Shrink(Vector3.one * Physics.defaultContactOffset)))
					{
						Destroy(levelPiece.gameObject);
						return null;
					}
				}
			}
			levelPiece.gameObject.SetActive(true);
			madeLevelPieces.Add(levelPiece);
			madeCheckpoints.AddRange(levelPiece.checkpoints);
			levelPiecesBounds.Add(levelPiece.boundsCollider.bounds);
			furthestLevelPiece = levelPiece;
			furthestLevelPiecePrefab = levelPiecePrefab;
			return levelPiece;
		}

		public void DeactivatePastLevelPieces ()
		{
			bool reachedCheckpointThatCantSeeSkybox = false;
			for (int i = madeLevelPieces.Count - 2; i >= 0; i --)
			{
				ProceduralEndlessRacingLevelPiece levelPiece = madeLevelPieces[i];
				if (reachedCheckpointThatCantSeeSkybox)
				{
					levelPiece.gameObject.SetActive(false);
					furthestDeactivatedLevelPieceIndex = i;
				}
				if (levelPiece.hasCheckpointThatCantSeeSkybox)
				{
					if (reachedCheckpointThatCantSeeSkybox)
						return;
					reachedCheckpointThatCantSeeSkybox = true;
				}
			}
		}

		public void ActivatePastLevelPieces ()
		{
			ProceduralEndlessRacingLevelPiece furthestDeactivatedLevelPiece = madeLevelPieces[furthestDeactivatedLevelPieceIndex];
			furthestDeactivatedLevelPiece.gameObject.SetActive(true);
			furthestDeactivatedLevelPieceIndex --;
			for (int i = furthestDeactivatedLevelPieceIndex; i >= 0; i --)
			{
				ProceduralEndlessRacingLevelPiece levelPiece = madeLevelPieces[i];
				if (!levelPiece.hasCheckpointThatCantSeeSkybox)
				{
					levelPiece.gameObject.SetActive(true);
					furthestDeactivatedLevelPieceIndex --;
				}
				else
					return;
			}
		}

		[Serializable]
		public struct LevelPieceGroup
		{
			public ProceduralEndlessRacingLevelPiece[] nonStartingLevelPieces;
			public ProceduralEndlessRacingLevelPiece[] startingLevelPieces;
		}
	}
}
#endif