using UnityEngine;
using Extensions;

namespace AmbitiousSnake
{
	public class SkinPerk : Perk
	{
		public Snake.Skin skin;
		public static SkinPerk currentEquipped;

		public override void Equip ()
		{
			base.Equip ();
			if (currentEquipped != null)
				currentEquipped.Unequip ();
			Snake.instance.SetSkin (skin);
			currentEquipped = this;
			PerksMenu.CurrentSkinPerkIndex = PerksMenu.instance.perks.IndexOf<Perk>(this);
		}

		public override void Unequip ()
		{
			base.Unequip ();
			Snake.instance.SetSkin (PerksMenu.defaultSkin);
			currentEquipped = null;
			PerksMenu.CurrentSkinPerkIndex = -1;
		}

		public void StartPreview ()
		{
			Snake.instance.SetSkin (skin);
			Snake.instance.animator.updateMode = AnimatorUpdateMode.UnscaledTime;
		}

		public void StopPreview ()
		{
			if (currentEquipped != null)
				Snake.instance.SetSkin (currentEquipped.skin);
			else
				Snake.instance.SetSkin (PerksMenu.defaultSkin);
			Snake.instance.animator.updateMode = AnimatorUpdateMode.Normal;
		}

		void OnDisable ()
		{
			Snake.instance.animator.updateMode = AnimatorUpdateMode.Normal;
		}
	}
}