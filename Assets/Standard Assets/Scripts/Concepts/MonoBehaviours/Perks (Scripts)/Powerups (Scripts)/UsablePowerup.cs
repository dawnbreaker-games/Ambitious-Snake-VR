using UnityEngine;
using System.Collections;

namespace AmbitiousSnake
{
	public class UsablePowerup : Powerup
	{
		public int useCount;
		int usesRemaining;

		public virtual void Awake ()
		{
			usesRemaining = useCount;
		}

		public virtual void OnEnable ()
		{
			base.OnEnable ();
			toggleEquipButton.interactable &= Equipped || usesRemaining > 0;
		}

		public override void Equip ()
		{
			base.Equip ();
			GameManager.instance.StartCoroutine(DelayUseRoutine ());
		}

		IEnumerator DelayUseRoutine ()
		{
			yield return new WaitUntil(() => (!GameManager.paused));
			Use ();
		}

		public virtual void Use ()
		{
			Unequip ();
			usesRemaining --;
		}
	}
}