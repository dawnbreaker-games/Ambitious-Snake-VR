using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace AmbitiousSnake
{
	public class ExtraLifePowerup : Powerup, IUpdatable
	{
		public float retryAtTimeBeforeLastGrounded;
		SnakeRecording recording;
		SnakeRecording.Frame currentFrame;
		List<Vector3> newHeadPositions = new List<Vector3>();
		List<Vector3> newTailPositions = new List<Vector3>();
		int removedTailPiecesCount;

		void Awake ()
		{
			if (Equipped)
				Equip ();
			else
				Unequip ();
			StartRecording ();
		}

		public override void Equip ()
		{
			base.Equip ();
			Snake.instance.onDeath += Use;
		}

		public override void Unequip ()
		{
			base.Unequip ();
			Snake.instance.onDeath -= Use;
		}

		void Use ()
		{
			Snake.instance.onDeath -= Use;
			StopRecording ();
			GameManager.paused = true;
			// Time.timeScale = 0;
			GameManager.instance.StartCoroutine(PlaybackRecordingRoutine ());
		}
		
		void StartRecording ()
		{
			recording = new SnakeRecording();
			SnakeRecording.Frame frame = new SnakeRecording.Frame();
			frame.newHeadPositions = new Vector3[Snake.instance.pieces.Count];
			for (int i = 0; i < Snake.instance.pieces.Count; i ++)
				frame.newHeadPositions[i] = Snake.instance.GetPiecePosition(i);
			frame.trsPosition = Snake.instance.trs.position;
			frame.trsRotation = Snake.instance.trs.eulerAngles;
			recording.frames = new List<SnakeRecording.Frame>();
			recording.frames.Add(frame);
			Snake.instance.onAddHeadPiece += OnAddHeadPiece;
			Snake.instance.onAddTailPiece += OnAddTailPiece;
			Snake.instance.onRemoveTailPiece += OnRemoveTailPiece;
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public void DoUpdate ()
		{
			SnakeRecording.Frame frame = new SnakeRecording.Frame();
			frame.newHeadPositions = newHeadPositions.ToArray();
			newHeadPositions.Clear();
			frame.newTailPositions = newTailPositions.ToArray();
			newTailPositions.Clear();
			frame.removedTailPiecesCount = removedTailPiecesCount;
			removedTailPiecesCount = 0;
			frame.trsPosition = Snake.instance.trs.position;
			frame.trsRotation = Snake.instance.trs.eulerAngles;
			frame.timeSinceCreated = GameManager.UnpausedTimeSinceLevelLoad;
			recording.frames.Add(frame);
		}

		void OnAddHeadPiece (Vector3 position)
		{
			newHeadPositions.Add(position);
		}

		void OnAddTailPiece (Vector3 position)
		{
			newTailPositions.Add(position);
		}

		void OnRemoveTailPiece ()
		{
			removedTailPiecesCount ++;
		}

		void StopRecording ()
		{
			Snake.instance.onAddHeadPiece -= OnAddHeadPiece;
			Snake.instance.onAddTailPiece -= OnAddTailPiece;
			Snake.instance.onRemoveTailPiece -= OnRemoveTailPiece;
			GameManager.updatables = GameManager.updatables.Remove(this);
		}

		IEnumerator PlaybackRecordingRoutine ()
		{
			float timeLastGrounded = 0;
			int i = recording.frames.Count - 1;
			for (i = i; i >= 0; i --)
			{
				currentFrame = recording.frames[i];
				UpdateSnake ();
				yield return new WaitForEndOfFrame();
				if (IsGrounded())
				{
					timeLastGrounded = currentFrame.timeSinceCreated;
					break;
				}
			}
			for (i = i; i >= 0; i --)
			{
				currentFrame = recording.frames[i];
				UpdateSnake ();
				if (timeLastGrounded - currentFrame.timeSinceCreated >= retryAtTimeBeforeLastGrounded)
					break;
				yield return new WaitForEndOfFrame();
			}
			// Time.timeScale = 1;
			GameManager.paused = false;
		}

		void UpdateSnake ()
		{
			Translate ();
			Rotate ();
			RemoveHeadPieces ();
			RemoveTailPieces ();
			AddTailPieces ();
			OnSetLength ();
		}
		
		void Translate ()
		{
			Snake.instance.trs.position = currentFrame.trsPosition;
		}

		void Rotate ()
		{
			Snake.instance.trs.eulerAngles = currentFrame.trsRotation;
		}

		void RemoveHeadPieces ()
		{
			for (int i = 0; i < currentFrame.newHeadPositions.Length; i ++)
				RemoveHeadPiece ();
		}

		void RemoveTailPieces ()
		{
			for (int i = 0; i < currentFrame.newTailPositions.Length; i ++)
				Snake.instance.RemoveTailPiece ();
		}

		void AddTailPieces ()
		{
			float totalMoveAmount = 0;
			for (int i = 0; i < currentFrame.removedTailPiecesCount; i ++)
				totalMoveAmount += Snake.instance.pieces[i].distanceToPreviousPiece;
			Vector3 tailPosition = Snake.instance.TailPosition;
			Vector3 move = (tailPosition - Snake.instance.pieces[1].trs.position).normalized;
			while (totalMoveAmount > 0)
			{
				float moveAmount = Mathf.Min(Snake.instance.maxDistanceBetweenPieces, totalMoveAmount);
				if (Physics.Raycast(tailPosition, move, moveAmount + SnakePiece.RADIUS + Physics.defaultContactOffset, Snake.instance.whatICrashIntoExcludingSnakes, QueryTriggerInteraction.Ignore))
				{
					for (int i = 0; i < Snake.instance.pieces.Count; i ++)
					{
						SnakePiece piece = Snake.instance.pieces[i];
						if (Physics.Raycast(piece.trs.position, -move, moveAmount + SnakePiece.RADIUS + Physics.defaultContactOffset, Snake.instance.whatICrashIntoExcludingSnakes, QueryTriggerInteraction.Ignore))
						{
							OnSetLength ();
							return;
						}
					}
				}
				Vector3 position = tailPosition + (move * moveAmount);
				Snake.instance.AddTailPiece (position, moveAmount);
				tailPosition = position;
				totalMoveAmount -= moveAmount;
			}
		}

		void OnSetLength ()
		{
			Snake.instance.SetSkinParts (Snake.instance.skin);
		}

		void RemoveHeadPiece ()
		{
			SnakePiece headPiece = Snake.instance.HeadPiece;
			ObjectPool.instance.Despawn (headPiece.prefabIndex, headPiece.gameObject, headPiece.trs);
			Snake.instance.pieces.RemoveAt(Snake.instance.pieces.Count - 1);
			Snake.instance.currentLength -= headPiece.distanceToPreviousPiece;
		}

		bool IsGrounded ()
		{
			if (!Snake.instance.rigid.useGravity)
				return true;
			for (int i = 0; i < Snake.instance.pieces.Count; i ++)
			{
				if (Physics.Raycast(Snake.instance.GetPiecePosition(i), Vector3.down, SnakePiece.RADIUS + Physics.defaultContactOffset, Snake.instance.whatICrashIntoExcludingSnakes, QueryTriggerInteraction.Ignore))
					return true;
			}
			return false;
		}
	}
}