using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace AmbitiousSnake
{
	public class InvulernabilityPowerup : UsableDurationalPowerup
	{
		public override void ApplyEffect ()
		{
			base.ApplyEffect ();
			Snake.instance.onDeath += delegate {  };
		}

		public override void UnapplyEffect ()
		{
			base.UnapplyEffect ();
			Snake.instance.onDeath -= delegate {  };
		}
	}
}