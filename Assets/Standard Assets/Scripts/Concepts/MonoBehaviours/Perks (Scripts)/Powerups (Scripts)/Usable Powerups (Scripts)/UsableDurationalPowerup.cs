using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Extensions;

namespace AmbitiousSnake
{
	public class UsableDurationalPowerup : UsablePowerup, IUpdatable
	{
		public float duration;
		float startTime;

		public override void Use ()
		{
			base.Use ();
			startTime = Time.time;
			ApplyEffect ();
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public void DoUpdate ()
		{
			if (Time.time - startTime >= duration)
			{
				Unequip ();
				GameManager.updatables = GameManager.updatables.Remove(this);
			}
		}

		public override void Unequip ()
		{
			base.Unequip ();
			UnapplyEffect ();
		}

		public virtual void ApplyEffect ()
		{
		}

		public virtual void UnapplyEffect ()
		{
		}
	}
}