using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace AmbitiousSnake
{
	public class AddToVelocityPowerup : UsableDurationalPowerup
	{
		public float speed;

		public override void ApplyEffect ()
		{
			base.ApplyEffect ();
			GameManager.instance.StartCoroutine(HandleEffectRoutine ());
		}

		public override void UnapplyEffect ()
		{
			base.UnapplyEffect ();
			GameManager.instance.StopCoroutine(HandleEffectRoutine ());
		}

		IEnumerator HandleEffectRoutine ()
		{
			while (true)
			{
				Snake.instance.rigid.velocity += Snake.instance.move * speed * Time.deltaTime;
				yield return new WaitForEndOfFrame();
			}
		}
	}
}