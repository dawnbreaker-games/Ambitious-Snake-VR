namespace AmbitiousSnake
{
	public class AddToMoveSpeedAndChangeLengthRatePowerup : Powerup
	{
		public float amount;

		void Awake ()
		{
			if (Equipped)
				Equip ();
			else
				Unequip ();
		}

		public override void Equip ()
		{
			if (previouslyEquipped)
				return;
			base.Equip ();
			Snake.instance.moveSpeed += amount;
			Snake.instance.changeLengthRate += amount;
		}

		public override void Unequip ()
		{
			if (!previouslyEquipped)
				return;
			base.Unequip ();
			Snake.instance.moveSpeed -= amount;
			Snake.instance.changeLengthRate -= amount;
		}
	}
}