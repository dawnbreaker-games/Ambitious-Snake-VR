using TMPro;
using Extensions;
using UnityEngine;
using UnityEngine.UI;

namespace AmbitiousSnake
{
	public class Perk : MonoBehaviour
	{
		public int scoreRequired;
		public TMP_Text scoreRequiredText;
		public TMP_Text descriptionText;
		public Button toggleEquipButton;
		public TMP_Text toggleEquipText;
		public bool Equipped
		{
			get
			{
				return PlayerPrefsExtensions.GetBool(name +  " equipped", false);
			}
			set
			{
				PlayerPrefsExtensions.SetBool(name +  " equipped", value);
			}
		}
		[HideInInspector]
		public bool previouslyEquipped;

		public virtual void OnEnable ()
		{
			toggleEquipButton.interactable = SaveAndLoadManager.saveData.score >= scoreRequired;
			scoreRequiredText.text = "Score Required: " + scoreRequired;
			if (Equipped)
				toggleEquipText.text = "Unequip";
			else
				toggleEquipText.text = "Equip";
		}

		public void ToggleEquip ()
		{
			if (Equipped)
				Unequip ();
			else
				Equip ();
		}

		public virtual void Equip ()
		{
			if (previouslyEquipped)
				return;
			toggleEquipText.text = "Unequip";
			Equipped = true;
			previouslyEquipped = true;
		}

		public virtual void Unequip ()
		{
			if (!previouslyEquipped)
				return;
			toggleEquipText.text = "Equip";
			Equipped = false;
			previouslyEquipped = false;
		}
	}
}