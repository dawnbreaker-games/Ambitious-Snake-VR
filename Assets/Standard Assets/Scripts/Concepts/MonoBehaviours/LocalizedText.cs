using TMPro;
using Extensions;
using UnityEngine;

namespace AmbitiousSnake
{
	[ExecuteInEditMode]
	[DisallowMultipleComponent]
	[RequireComponent(typeof(TMP_Text))]
	public class LocalizedText : MonoBehaviour
	{
		public TMP_Text text;
#if UNITY_EDITOR
		public bool addTranslations;
#endif

		void OnValidate ()
		{
#if UNITY_EDITOR
			if (addTranslations)
			{
				addTranslations = false;
				AddTranslations ();
			}
#endif
		}

		public void AddTranslations ()
		{
			for (int i = 0; i < LanguageManager.instance.nonDefaultLanguages.Length; i ++)
			{
				LanguageManager.Language language = LanguageManager.instance.nonDefaultLanguages[i];
				AddTranslation (language);
			}
		}

		public void AddTranslation (LanguageManager.Language language)
		{
			LanguageManager.instance.AddTranslation(language, text.text);
		}

		public void Translate ()
		{
			text.text = LanguageManager.Instance.translationsDict[text.text][LanguageManager.instance.currentLanguage].endText;
		}
	}
}