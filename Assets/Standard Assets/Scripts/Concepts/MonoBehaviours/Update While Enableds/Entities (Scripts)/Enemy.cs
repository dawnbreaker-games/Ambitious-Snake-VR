using UnityEngine;
using Extensions;
using System.Collections.Generic;

namespace AmbitiousSnake
{
	public class Enemy : Entity
	{
        public Collider[] colliders = new Collider[0];
		public BulletPatternEntry[] bulletPatternEntries = new BulletPatternEntry[0];
		public BulletPatternEntryDontCollideWithShooter[] bulletPatternEntriesDontCollideWithShooter = new BulletPatternEntryDontCollideWithShooter[0];
		public Dictionary<string, BulletPatternEntry> bulletPatternEntriesDict = new Dictionary<string, BulletPatternEntry>();
		[Tooltip("targetDistanceFromSnake")]
		public float targetDistanceFromSnake;
		public Transform eyesTrs;
		public float visionDegrees;
		public float visionSphereCastRadius;
		public LayerMask whatBlocksVision;
		public float minSnakePositionChangeToRepath;
		public Patrol patrol;
		protected Vector3 lastKnownSnakePiecePosition;
		protected float targetDistanceFromSnakeSqr;
		protected bool canSeeSnake;
		protected Transform targetSnakePieceTrs;
		float minSnakePositionChangeToRepathSqr;

		public override void Awake ()
		{
			base.Awake ();
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				if (eyesTrs == null)
					eyesTrs = trs.Find("Eyes");
				return;
			}
#endif
			for (int i = 0; i < colliders.Length; i ++)
			{
				Collider collider = colliders[i];
				for (int i2 = i + 1; i2 < colliders.Length; i2 ++)
				{
					Collider collider2 = colliders[i2];
					Physics.IgnoreCollision(collider, collider2, true);
				}
			}
			for (int i = 0; i < bulletPatternEntries.Length; i ++)
			{
				BulletPatternEntry bulletPatternEntry = bulletPatternEntries[i];
				bulletPatternEntriesDict.Add(bulletPatternEntry.name, bulletPatternEntry);
			}
			for (int i = 0; i < bulletPatternEntriesDontCollideWithShooter.Length; i ++)
			{
				BulletPatternEntryDontCollideWithShooter bulletPatternEntryDontCollideWithShooter = bulletPatternEntriesDontCollideWithShooter[i];
				bulletPatternEntriesDict.Add(bulletPatternEntryDontCollideWithShooter.name, bulletPatternEntryDontCollideWithShooter);
			}
			targetDistanceFromSnakeSqr = targetDistanceFromSnake * targetDistanceFromSnake;
			minSnakePositionChangeToRepathSqr = minSnakePositionChangeToRepath * minSnakePositionChangeToRepath;
		}

		public void ShootBulletPatternEntry (string name)
		{
			bulletPatternEntriesDict[name].Shoot ();
		}
		
		public override void DoUpdate ()
		{
			if (GameManager.paused)
				return;
			canSeeSnake = CanSeeSnake();
			HandleAttacking ();
			base.DoUpdate ();
		}

		public virtual void HandleAttacking ()
		{
		}

		public override void HandlePathfinding ()
		{
			if (canSeeSnake)
			{
				Vector3 snakePiecePosition = targetSnakePieceTrs.position;
				if ((lastKnownSnakePiecePosition - snakePiecePosition).sqrMagnitude >= minSnakePositionChangeToRepathSqr)
				{
					SetDestination (snakePiecePosition);
					lastKnownSnakePiecePosition = snakePiecePosition;
				}
			}
			base.HandlePathfinding ();
		}
		
		public override void HandleRotating ()
		{
			if (canSeeSnake)
				trs.forward = (targetSnakePieceTrs.position - trs.position).GetXZ();
			else
				base.HandleRotating ();
		}
		
		public override void HandleMoving ()
		{
			if (canSeeSnake && (trs.position - targetSnakePieceTrs.position).sqrMagnitude < targetDistanceFromSnakeSqr)
				move = (trs.position - targetSnakePieceTrs.position).normalized;
			else
				base.HandleMoving ();
		}

		void OnTriggerEnter (Collider other)
		{
			OnTriggerStay (other);
		}

		void OnTriggerStay (Collider other)
		{
			Snake snake = other.GetComponentInParent<Snake>();
			if (snake != null)
			{
				Transform snakePieceTrs = other.GetComponent<Transform>();
				Vector3 eyesToSnakePiece = snakePieceTrs.position - eyesTrs.position;
				if (Vector3.Angle(eyesTrs.forward, eyesToSnakePiece) <= visionDegrees / 2)
				{
					RaycastHit hit;
					if (Physics.SphereCast(eyesTrs.position, visionSphereCastRadius, eyesToSnakePiece, out hit, eyesToSnakePiece.magnitude, whatBlocksVision, QueryTriggerInteraction.Ignore))
					{
						if (hit.collider == other)
							WakeUp ();
						else
						{
							Snake hitSnake = hit.collider.GetComponentInParent<Snake>();
							if (hitSnake != null)
								WakeUp ();
						}
					}
				}
			}
		}

		public void WakeUp ()
		{
			patrol.enabled = false;
			enabled = true;
		}

		bool CanSeeSnake ()
		{
			// for (int i = Snake.instance.pieces.Count - 1; i >= 0; i --)
			for (int i = 0; i < Snake.instance.pieces.Count; i ++)
			{
				Vector3 eyesToSnakePiece = Snake.instance.GetPiecePosition(i) - eyesTrs.position;
				if (Vector3.Angle(eyesTrs.forward, eyesToSnakePiece) <= visionDegrees / 2)
				{
					RaycastHit hit;
					if (Physics.SphereCast(eyesTrs.position, visionSphereCastRadius, eyesToSnakePiece, out hit, eyesToSnakePiece.magnitude, whatBlocksVision, QueryTriggerInteraction.Ignore))
					{
						SnakePiece snakePiece = hit.collider.GetComponent<SnakePiece>();
						if (snakePiece != null)
						{
							targetSnakePieceTrs = snakePiece.trs;
							return true;
						}
					}
				}
			}
			return false;
		}
	}
}