using TMPro;
using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace AmbitiousSnake
{
	[ExecuteInEditMode]
	public class Level : SingletonUpdateWhileEnabled<Level>
	{
		public TMP_Text notificationText;
		public float parTime;
		public TMP_Text timerText;
		public Color pastParTimeTimerColor;
		public RectTransform canvasRectTrs;
		public bool HasWon
		{
			get
			{
				return PlayerPrefsExtensions.GetBool(name + " won");
			}
			set
			{
				PlayerPrefsExtensions.SetBool(name + " won", value);
			}
		}
		public float FastestTime
		{
			get
			{
				return PlayerPrefs.GetFloat(name + " best time", Mathf.Infinity);
			}
			set
			{
				PlayerPrefs.SetFloat(name + " best time", value);
			}
		}
		public bool CollectedStar
		{
			get
			{
				return PlayerPrefsExtensions.GetBool(name + " star collected", false);
			}
			set
			{
				PlayerPrefsExtensions.SetBool(name + " star collected", value);
			}
		}
		public bool GotParTime
		{
			get
			{
				return FastestTime <= parTime;
			}
		}
		public bool HasWonInMasteryMode
		{
			get
			{
				return PlayerPrefsExtensions.GetBool(name + " won in mastery mode");
			}
			set
			{
				PlayerPrefsExtensions.SetBool(name + " won in mastery mode", value);
			}
		}
		public float FastestTimeInMasteryMode
		{
			get
			{
				return PlayerPrefs.GetFloat(name + " best time in mastery mode", Mathf.Infinity);
			}
			set
			{
				PlayerPrefs.SetFloat(name + " best time in mastery mode", value);
			}
		}
		public bool CollectedStarInMasteryMode
		{
			get
			{
				return PlayerPrefsExtensions.GetBool(name + " star collected in mastery mode", false);
			}
			set
			{
				PlayerPrefsExtensions.SetBool(name + " star collected in mastery mode", value);
			}
		}
		public uint TriesRemainingInMasteryMode
		{
			get
			{
				return (uint) PlayerPrefs.GetInt(name + " tries remaining in mastery mode", 2);
			}
			set
			{
				PlayerPrefs.SetInt(name + " tries remaining in mastery mode", (int) value);
			}
		}
		public bool GotParTimeInMasteryMode
		{
			get
			{
				return FastestTimeInMasteryMode <= parTime;
			}
		}
		
		public override void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			base.OnEnable ();
		}

		public override void DoUpdate ()
		{
			float timeSinceLevelLoad = GameManager.UnpausedTimeSinceLevelLoad;
			timerText.text = string.Format("{0:0.#}", timeSinceLevelLoad);
			if (timeSinceLevelLoad > parTime)
				timerText.color = pastParTimeTimerColor;
		}
	}
}