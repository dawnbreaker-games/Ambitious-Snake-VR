using UnityEngine;
using System.Collections;
using Extensions;
using System;
using UnityEngine.Events;
using System.Collections.Generic;
using TMPro;
using UnityEngine.InputSystem;
using UnityEngine.UI;

namespace AmbitiousSnake
{
	public class GameplayMenu : SingletonUpdateWhileEnabled<GameplayMenu>
	{
		public bool isTopTier;
		public Transform trs;
		public float distanceFromCamera;
		public SphereCollider centerOptionRange;
		public Option centerOption;
		public float optionSeperationFromCenterOption;
		public Option[] options = new Option[0];
		public GameObject optionsParentGo;
		public GameObject titleTextGo;
		public TMP_Text[] movementTypeTexts = new TMP_Text[0];
		public TMP_Text[] masteryModeTexts = new TMP_Text[0];
		public Button clearDataButton;
		public static Transform selectorTrs;
		public static ClearDataSettings clearDataSettings = new ClearDataSettings();
		Option selectedOption;
		bool leftGameplayMenuInput;
		bool previousLeftGameplayMenuInput;
		bool rightGameplayMenuInput;
		bool previousRightGameplayMenuInput;
		bool gameplayMenuInput;
		bool previousGameplayMenuInput;

#if UNITY_EDITOR
		void OnValidate ()
		{
			if (options.Length > 0)
			{
				int optionIndex = 0;
				for (float angle = 0; angle < 360; angle += 360f / options.Length)
				{
					options[optionIndex].trs.position = centerOptionRange.bounds.center + (trs.rotation * VectorExtensions.FromFacingAngle(angle)) * (centerOptionRange.bounds.size.x + optionSeperationFromCenterOption);
					optionIndex ++;
				}
			}
		}
#endif

		public override void Awake ()
		{
			if (isTopTier)
			{
				gameObject.SetActive(false);
				if (instance == null)
					instance = this;
			}
			else
				Disable ();
			for (int i = 0; i < VR_UIPointer.instances.Length; i ++)
			{
				VR_UIPointer vR_UIPointer = VR_UIPointer.instances[i];
				if (vR_UIPointer.uiPlaneTrs == null)
					vR_UIPointer.uiPlaneTrs = trs;
			}
		}

		public override void OnEnable ()
		{
			previousLeftGameplayMenuInput = true;
			previousRightGameplayMenuInput = true;
			previousGameplayMenuInput = true;
			base.OnEnable ();
			if (instance == this)
			{
				GameManager.paused = true;
				BattleSnake battleSnake = Snake.instance as BattleSnake;
				if (battleSnake != null)
				{
					battleSnake.breath.particleSystemWithTrigger.Pause();
					battleSnake.breath.particleSystemWithCollision.Pause();
				}
				// Time.timeScale = 0;
				Physics.autoSimulation = false;
			}
			bool moveHandToChangeDirection = GameManager.gameModifierDict["Move hand to change direction"].isActive;
			UpdateMovementTypeTexts (moveHandToChangeDirection);
			bool inMasteryMode = GameManager.gameModifierDict["Mastery mode"].isActive;
			UpdateMasteryModeTexts (inMasteryMode);
		}

		public override void OnDisable ()
		{
			base.OnDisable ();
			if (instance == this)
			{
				BattleSnake battleSnake = Snake.instance as BattleSnake;
				if (battleSnake != null)
				{
					battleSnake.breath.particleSystemWithTrigger.Play();
					battleSnake.breath.particleSystemWithCollision.Play();
				}
				// Time.timeScale = 1;
				GameManager.paused = false;
				Physics.autoSimulation = true;
			}
		}

		public override void DoUpdate ()
		{
			if (!optionsParentGo.activeSelf || selectorTrs == null)
				return;
			leftGameplayMenuInput = InputManager.LeftGameplayMenuInput;
			rightGameplayMenuInput = InputManager.RightGameplayMenuInput;
			gameplayMenuInput = InputManager.GameplayMenuInput;
			if (optionsParentGo.activeSelf && selectorTrs != null)
			{
				HandleSelecting ();
				HandleInteracting ();
			}
			previousLeftGameplayMenuInput = leftGameplayMenuInput;
			previousRightGameplayMenuInput = rightGameplayMenuInput;
			previousGameplayMenuInput = gameplayMenuInput;
		}

		void HandleSelecting ()
		{
			Plane plane = new Plane(trs.forward, centerOptionRange.bounds.center);
			float hitDistance;
			Ray ray = new Ray(selectorTrs.position, selectorTrs.forward);
			if (plane.Raycast(ray, out hitDistance))
			{
				Vector3 hitPoint = ray.GetPoint(hitDistance);
				List<Transform> optionTransforms = new List<Transform>();
				for (int i = 0; i < options.Length; i ++)
				{
					Option option = options[i];
					optionTransforms.Add(option.trs);
				}
				optionTransforms.Add(centerOption.trs);
				Transform closestOptionTrs = TransformExtensions.GetClosestTransform_3D(optionTransforms.ToArray(), hitPoint);
				for (int i = 0; i < optionTransforms.Count - 1; i ++)
				{
					Transform optionTrs = optionTransforms[i];
					if (closestOptionTrs == optionTrs)
					{
						Select (options[i]);
						return;
					}
				}
				Select (centerOption);
			}
		}

		void HandleInteracting ()
		{
			if (!selectedOption.Equals(default(Option)) && selectedOption.isInteractive)
			{
				if (selectorTrs == VRCameraRig.instance.leftHandTrs && leftGameplayMenuInput && !previousLeftGameplayMenuInput)
					selectedOption.interactUnityEvent.Invoke();
				else if (selectorTrs == VRCameraRig.instance.rightHandTrs && rightGameplayMenuInput && !previousRightGameplayMenuInput)
					selectedOption.interactUnityEvent.Invoke();
				else if (gameplayMenuInput && !previousGameplayMenuInput)
					selectedOption.interactUnityEvent.Invoke();
			}
		}

		void Select (Option option)
		{
			if (selectedOption.Equals(option))
				return;
			if (!selectedOption.Equals(default(Option)))
				Deselect (selectedOption);
			if (option.isInteractive)
			{
				selectedOption = option;
				selectedOption.deselectedGo.SetActive(false);
				selectedOption.selectedGo.SetActive(true);
				selectedOption.selectUnityEvent.Invoke();
			}
			else
				selectedOption = default(Option);
		}

		void Deselect (Option option)
		{
			if (option.isInteractive)
			{
				option.selectedGo.SetActive(false);
				option.deselectedGo.SetActive(true);
				option.deselectUnityEvent.Invoke();
			}
		}

		public void ToggleMovementMode ()
		{
			bool moveHandToChangeDirection = GameManager.gameModifierDict["Move hand to change direction"].isActive;
			UpdateMovementTypeTexts (!moveHandToChangeDirection);
			GameManager.gameModifierDict["Move hand to change direction"].isActive = !moveHandToChangeDirection;
		}

		void UpdateMovementTypeTexts (bool moveHandToChangeDirection)
		{
			for (int i = 0; i < movementTypeTexts.Length; i ++)
			{
				TMP_Text movementTypeText = movementTypeTexts[i];
				if (moveHandToChangeDirection)
					movementTypeText.text = "Rotate Hand";
				else
					movementTypeText.text = "Move Hand";
			}
		}

		public void ToggleMasteryMode ()
		{
			bool inMasteryMode = GameManager.gameModifierDict["Mastery mode"].isActive;
			UpdateMasteryModeTexts (!inMasteryMode);
			GameManager.gameModifierDict["Mastery mode"].isActive = !inMasteryMode;
		}

		void UpdateMasteryModeTexts (bool inMasteryMode)
		{
			for (int i = 0; i < masteryModeTexts.Length; i ++)
			{
				TMP_Text masteryModeText = masteryModeTexts[i];
				if (inMasteryMode)
					masteryModeText.text = "Leave Mastery Mode";
				else
					masteryModeText.text = "Enter Mastery Mode";
			}
		}

		public void SetClearNonMasteryData (bool value)
		{
			clearDataSettings.clearNonMasteryData = value;
			UpdateClearDataButton ();
		}

		public void SetClearMasteryData (bool value)
		{
			clearDataSettings.clearMasteryData = value;
			UpdateClearDataButton ();
		}

		void UpdateClearDataButton ()
		{
			clearDataButton.interactable = clearDataSettings.clearNonMasteryData || clearDataSettings.clearMasteryData;
		}

		public void ClearData ()
		{
			if (clearDataSettings.clearNonMasteryData && clearDataSettings.clearMasteryData)
			{
				PlayerPrefs.DeleteAll();
				SaveAndLoadManager.saveData.score = 0;
				SaveAndLoadManager.saveData.masteryScore = 0;
				SaveAndLoadManager.saveData.masteryHighscore = 0;
			}
			else if (clearDataSettings.clearNonMasteryData)
			{
				SaveAndLoadManager.saveData.score = 0;
				for (int i = 0; i < LevelSelect.instance.levels.Length; i ++)
				{
					Level level = LevelSelect.instance.levels[i];
					PlayerPrefs.DeleteKey(level.name + " won");
					PlayerPrefs.DeleteKey(level.name + " best time");
					PlayerPrefs.DeleteKey(level.name + " star collected");
				}
			}
			else if (clearDataSettings.clearMasteryData)
			{
				SaveAndLoadManager.saveData.masteryHighscore = 0;
				ResetMasteryData ();
			}
			_SceneManager.instance.LoadSceneWithoutTransition (0);
		}

		public void ResetMasteryData ()
		{
			SaveAndLoadManager.saveData.masteryScore = 0;
			for (int i = 0; i < LevelSelect.instance.levels.Length; i ++)
			{
				Level level = LevelSelect.instance.levels[i];
				PlayerPrefs.DeleteKey(level.name + " won in mastery mode");
				PlayerPrefs.DeleteKey(level.name + " best time in mastery mode");
				PlayerPrefs.DeleteKey(level.name + " star collected in mastery mode");
				PlayerPrefs.DeleteKey(level.name + " tries remaining in mastery mode");
			}
		}

		public void SwitchGameplayMenu (GameplayMenu switchTo)
		{
			Disable ();
			switchTo.Enable ();
		}

		public void SetOrientation ()
		{
			if (instance != this)
			{
				instance.SetOrientation ();
				return;
			}
			InputSystem.Update();
			VRCameraRig.instance.DoUpdate ();
			trs.position = VRCameraRig.instance.eyesTrs.position + (VRCameraRig.instance.eyesTrs.forward * distanceFromCamera);
			trs.rotation = VRCameraRig.instance.eyesTrs.rotation;
		}

		void Enable ()
		{
			optionsParentGo.SetActive(true);
			if (titleTextGo != null)
				titleTextGo.SetActive(true);
		}

		public void Disable ()
		{
			optionsParentGo.SetActive(false);
			if (titleTextGo != null)
				titleTextGo.SetActive(false);
		}

		[Serializable]
		public struct Option
		{
			public Transform trs;
			public GameObject selectedGo;
			public GameObject deselectedGo;
			public bool isInteractive;
			public GameObject uninteractiveGo;
			public UnityEvent selectUnityEvent;
			public UnityEvent deselectUnityEvent;
			public UnityEvent interactUnityEvent;
		}

		public struct ClearDataSettings
		{
			public bool clearNonMasteryData;
			public bool clearMasteryData;
		}
	}
}