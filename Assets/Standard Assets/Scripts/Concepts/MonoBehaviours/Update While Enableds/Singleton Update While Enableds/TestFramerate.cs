using UnityEngine;
using AmbitiousSnake;

public class TestFramerate : SingletonUpdateWhileEnabled<TestFramerate>
{
	float totalTime;
	int frameCount;

	public override void DoUpdate ()
	{
		if (GameManager.framesSinceLevelLoaded <= GameManager.LAGGY_FRAMES_AFTER_SCENE_LOAD)
			return;
		float deltaTime = Time.unscaledDeltaTime;
		totalTime += deltaTime;
		frameCount ++;
		print("Delta time: " + deltaTime + " Average delta time: " + totalTime / frameCount);
	}
}