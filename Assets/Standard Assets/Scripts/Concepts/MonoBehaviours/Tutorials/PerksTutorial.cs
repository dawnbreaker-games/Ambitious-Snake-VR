using UnityEngine;

namespace AmbitiousSnake
{
	public class PerksTutorial : Tutorial
	{
		public override void Awake ()
		{
			base.Awake ();
			gameObject.SetActive(false);
		}
	}
}