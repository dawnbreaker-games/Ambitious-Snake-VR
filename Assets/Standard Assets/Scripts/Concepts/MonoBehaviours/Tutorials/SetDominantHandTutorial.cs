using UnityEngine;
using UnityEngine.Playables;
using Extensions;

namespace AmbitiousSnake
{
	public class SetDominantHandTutorial : Tutorial
	{
		public int minHandShakeFramesToFinish;
		public float minHandMoveSpeedToFinish;
		public float minHandRotateRateToFinish;
		public static bool LeftHandIsDominant
		{
			get
			{
				return PlayerPrefsExtensions.GetBool("Left-handed", false);
			}
			set
			{
				PlayerPrefsExtensions.SetBool("Left-handed", value);
			}
		}
		int handShakeFramesRemaining;
		bool isShakingLeftHand;
		Vector3? leftHandPosition;
		Vector3? rightHandPosition;
		Vector3? previousLeftHandPosition;
		Vector3? previousRightHandPosition;
		Quaternion? leftHandRotation;
		Quaternion? rightHandRotation;
		Quaternion? previousLeftHandRotation;
		Quaternion? previousRightHandRotation;

		public override void OnEnable ()
		{
			handShakeFramesRemaining = minHandShakeFramesToFinish;
			base.OnEnable ();
		}

		public override void Finish ()
		{
			LeftHandIsDominant = isShakingLeftHand;
			base.Finish ();
		}

		public override void DoUpdate ()
		{
			leftHandPosition = InputManager.LeftHandPosition;
			rightHandPosition = InputManager.RightHandPosition;
			leftHandRotation = InputManager.LeftHandRotation;
			rightHandRotation = InputManager.RightHandRotation;
			isShakingLeftHand = (leftHandPosition != null && previousLeftHandPosition != null && (((Vector3) leftHandPosition - (Vector3) previousLeftHandPosition) / Time.deltaTime).sqrMagnitude >= minHandMoveSpeedToFinish * minHandMoveSpeedToFinish) || 
								(leftHandRotation != null && previousLeftHandRotation != null && QuaternionExtensions.GetAngularVelocity((Quaternion) previousLeftHandRotation, (Quaternion) leftHandRotation).sqrMagnitude >= minHandRotateRateToFinish * minHandRotateRateToFinish);
			if (isShakingLeftHand || 
				(rightHandPosition != null && previousRightHandPosition != null && (((Vector3) rightHandPosition - (Vector3) previousRightHandPosition) / Time.deltaTime).sqrMagnitude >= minHandMoveSpeedToFinish * minHandMoveSpeedToFinish) || 
				(rightHandRotation != null && previousRightHandRotation != null && QuaternionExtensions.GetAngularVelocity((Quaternion) previousRightHandRotation, (Quaternion) rightHandRotation).sqrMagnitude >= minHandRotateRateToFinish * minHandRotateRateToFinish))
			{
				// if (leftHandPosition != null && previousLeftHandPosition != null)
				// 	print("Left hand: " + (((Vector3) leftHandPosition - (Vector3) previousLeftHandPosition).magnitude / Time.deltaTime));
				// if (rightHandPosition != null && previousRightHandPosition != null)
				// 	print("Right hand: " + (((Vector3) rightHandPosition - (Vector3) previousRightHandPosition).magnitude / Time.deltaTime));
				handShakeFramesRemaining --;
				if (handShakeFramesRemaining == 0)
					Finish ();
			}
			else
				handShakeFramesRemaining = minHandShakeFramesToFinish;
			previousLeftHandPosition = leftHandPosition;
			previousRightHandPosition = rightHandPosition;
			previousLeftHandRotation = leftHandRotation;
			previousRightHandRotation = rightHandRotation;
		}
	}
}