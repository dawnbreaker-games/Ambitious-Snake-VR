using UnityEngine;

namespace AmbitiousSnake
{
	public class LengthTutorial : Tutorial
	{
		bool hasGrown;
		bool hasShrunk;
		
		public override void DoUpdate ()
		{
			if (InputManager.ChangeLengthInput > 0)
			{
				hasGrown = true;
				if (hasShrunk)
					Finish ();
			}
			else if (InputManager.ChangeLengthInput < 0)
			{
				hasShrunk = true;
				if (hasGrown)
					Finish ();
			}
		}
	}
}