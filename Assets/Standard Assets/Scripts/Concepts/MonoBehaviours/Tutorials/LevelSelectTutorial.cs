using UnityEngine;
using UnityEngine.Playables;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

namespace AmbitiousSnake
{
	public class LevelSelectTutorial : Tutorial
	{
		public string textOnEnable_LeftHand;
		public string textOnEnable_RightHand;
		public string textOnViewOtherLevel;
		public PlayableAsset playOnOpenGameplayMenu;
		public PlayableAsset playOnReopenGameplayMenu;
		public PlayableAsset playOnReturnToGameplayMenuFromLevelSelect;
		public PlayableAsset playOnReturnToGameplayMenuNotFromLevelSelect;
		public PlayableAsset playOnCloseGameplayMenuEarly;
		public PlayableAsset playOnOpenLevelSelect;
		public PlayableAsset playOnReopenLevelSelectWithNoOtherLevelViewed;
		public PlayableAsset playOnOpenPerksMenu;
		public PlayableAsset playOnOpenOptions;
		public GameObject leftController;
		public GameObject rightController;
		public GameObject aButtonIndicator;
		public GameObject bButtonIndicator;
		public GameObject rightTriggerIndicator;
		public GameObject xButtonIndicator;
		public GameObject yButtonIndicator;
		public GameObject leftTriggerIndicator;
		public Button backButton;
		bool gameplayMenuIsOpen;
		bool previousGameplayMenuIsOpen;
		bool gameplayMenuInput;
		bool previousGameplayMenuInput;
		bool hasOpenedLevelSelect;

		public override void OnEnable ()
		{
			base.OnEnable ();
			leftController.SetActive(true);
			rightController.SetActive(true);
			if (SetDominantHandTutorial.LeftHandIsDominant)
			{
				xButtonIndicator.SetActive(true);
				yButtonIndicator.SetActive(true);
				leftTriggerIndicator.SetActive(true);
				Level.instance.notificationText.text = textOnEnable_LeftHand;
			}
			else
			{
				aButtonIndicator.SetActive(true);
				bButtonIndicator.SetActive(true);
				rightTriggerIndicator.SetActive(true);
				Level.instance.notificationText.text = textOnEnable_RightHand;
			}
		}
		
		public override void DoUpdate ()
		{
			gameplayMenuIsOpen = GameplayMenu.instance.gameObject.activeSelf;
			gameplayMenuInput = InputManager.GameplayMenuInput;
			if (gameplayMenuInput != previousGameplayMenuInput)
			{
				if (gameplayMenuIsOpen != previousGameplayMenuIsOpen)
				{
					playableDirector.Stop();
					if (gameplayMenuIsOpen)
					{
						if (playableDirector.playableAsset == null)
							playableDirector.Play(playOnOpenGameplayMenu);
						else
							playableDirector.Play(playOnReopenGameplayMenu);
					}
					else if (!gameplayMenuIsOpen)
						playableDirector.Play(playOnCloseGameplayMenuEarly);
				}
				else if (playableDirector.playableAsset != null)
				{
					if (LevelSelect.instance.gameObject.activeSelf)
					{
						if (!hasOpenedLevelSelect)
						{
							playableDirector.Stop();
							playableDirector.Play(playOnOpenLevelSelect);
							LevelButton[] levelButtons = FindObjectsOfType<LevelButton>();
							SceneManager.sceneLoaded += OnSceneLoaded;
							for (int i = 0; i < levelButtons.Length; i ++)
							{
								LevelButton levelButton = levelButtons[i];
								levelButton.button.onClick.AddListener(() => {OnLevelButtonClicked (levelButton.trs.GetSiblingIndex());});
							}
						}
						else if (playableDirector.playableAsset != playOnOpenLevelSelect)
						{
							playableDirector.Stop();
							playableDirector.Play(playOnReopenLevelSelectWithNoOtherLevelViewed);
						}
						hasOpenedLevelSelect = true;
					}
					else if (hasOpenedLevelSelect)
					{
						playableDirector.Stop();
						playableDirector.Play(playOnReturnToGameplayMenuFromLevelSelect);
					}
				}
			}
			previousGameplayMenuIsOpen = gameplayMenuIsOpen;
			previousGameplayMenuInput = gameplayMenuInput;
		}

		public void OnReturnToGameplayMenuNotFromLevelSelect ()
		{
			if (playableDirector.playableAsset == null)
				return;
			playableDirector.Stop();
			playableDirector.Play(playOnReturnToGameplayMenuNotFromLevelSelect);
		}

		public void OnOpenPerksMenu ()
		{
			if (playableDirector.playableAsset == null)
				return;
			playableDirector.Stop();
			playableDirector.Play(playOnOpenPerksMenu);
		}

		public void OnOpenOptions ()
		{
			if (playableDirector.playableAsset == null)
				return;
			playableDirector.Stop();
			playableDirector.Play(playOnOpenOptions);
		}

		void OnLevelButtonClicked (int levelButtonIndex)
		{
			if (levelButtonIndex == _SceneManager.CurrentScene.buildIndex)
				return;
			LevelButton[] levelButtons = FindObjectsOfType<LevelButton>();
			for (int i = 0; i < levelButtons.Length; i ++)
			{
				LevelButton levelButton = levelButtons[i];
				levelButton.button.onClick.RemoveListener(() => {OnLevelButtonClicked (levelButton.trs.GetSiblingIndex());});
			}
			backButton.onClick.AddListener(Finish);
		}

		void OnSceneLoaded (Scene scene = new Scene(), LoadSceneMode loadSceneMode = LoadSceneMode.Single)
		{
			if (scene.buildIndex == _SceneManager.CurrentScene.buildIndex)
				return;
			SceneManager.sceneLoaded -= OnSceneLoaded;
			Level.instance.notificationText.text = textOnViewOtherLevel;
			Level.instance.canvasRectTrs.SetParent(null);
		}

		public override void OnDisable ()
		{
			base.OnDisable ();
			xButtonIndicator.SetActive(false);
			yButtonIndicator.SetActive(false);
			leftTriggerIndicator.SetActive(false);
			leftController.SetActive(false);
			aButtonIndicator.SetActive(false);
			bButtonIndicator.SetActive(false);
			rightTriggerIndicator.SetActive(false);
			rightController.SetActive(false);
			backButton.onClick.RemoveListener(Finish);
		}
	}
}