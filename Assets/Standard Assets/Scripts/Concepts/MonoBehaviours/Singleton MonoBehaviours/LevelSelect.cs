using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Extensions;
using UnityEngine.SceneManagement;
using System.Collections;

namespace AmbitiousSnake
{
	[ExecuteInEditMode]
	public class LevelSelect : SingletonMonoBehaviour<LevelSelect>
	{
		public Transform trs;
        public Level[] levels = new Level[0];
        public LevelButton[] levelButtons = new LevelButton[0];
		public Button startLevelButton;
		public TMP_Text titleText;
		public TMP_Text infoText;
		public static int? selectedLevelIndex;
		public const int LEVEL_1_SCENE_INDEX = 2;
		Coroutine startLevelPreviewCoroutine;
		Coroutine stopLevelPreviewCoroutine;

		public override void Awake ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				levelButtons = GetComponentsInChildren<LevelButton>(true);
				return;
			}
#endif
			base.Awake ();
			gameObject.SetActive(false);
		}

		void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			for (int i = 0; i < VR_UIPointer.instances.Length; i ++)
			{
				VR_UIPointer vR_UIPointer = VR_UIPointer.instances[i];
				vR_UIPointer.uiPlaneTrs = trs;
			}
			selectedLevelIndex = null;
			int completedLevelCount = 0;
			int parTimeCount = 0;
			int starCount = 0;
			int levelCount = levels.Length;
			for (int i = 0; i < levelCount; i ++)
			{
				Level level = levels[i];
				if (GameManager.ModifierIsActive("Mastery mode"))
				{
					if (level.HasWonInMasteryMode)
						completedLevelCount ++;
					if (level.GotParTimeInMasteryMode)
						parTimeCount ++;
					if (level.CollectedStarInMasteryMode)
						starCount ++;
				}
				else
				{
					if (level.HasWon)
						completedLevelCount ++;
					if (level.GotParTime)
						parTimeCount ++;
					if (level.CollectedStar)
						starCount ++;
				}
			}
			infoText.text = "Current Level:" + "\n" + (_SceneManager.CurrentScene.buildIndex - LEVEL_1_SCENE_INDEX + 1) + "\n\n" + 
				"Levels Complete:" + "\n" + completedLevelCount + "/" + levelCount + "\n\n" + 
				"Par Times:" + "\n" + parTimeCount + "/" + levelCount + "\n\n" + 
				"Stars:" + "\n" + starCount + "/" + levelCount + "\n\n";
			if (GameManager.ModifierIsActive("Mastery mode"))
			{
				titleText.text = "Mastery Level Select";
				infoText.text += "Score:" + "\n" + (SaveAndLoadManager.saveData.masteryScore) + "/" + (levelCount * 3) + "\n\n" + 
					"Highscore:" + "\n" + SaveAndLoadManager.saveData.masteryHighscore + "/" + (levelCount * 3);
			}
			else
			{
				titleText.text = "Level Select";
				infoText.text += "Score:" + "\n" + (SaveAndLoadManager.saveData.score) + "/" + (levelCount * 3);
			}
		}

		public void StartLevelPreview (int index)
		{
			if (startLevelPreviewCoroutine != null)
				StopCoroutine(startLevelPreviewCoroutine);
			if (stopLevelPreviewCoroutine != null)
				StopCoroutine(stopLevelPreviewCoroutine);
			stopLevelPreviewCoroutine = null;
			startLevelPreviewCoroutine = StartCoroutine(StartLevelPreviewRoutine (index));
		}

		IEnumerator StartLevelPreviewRoutine (int index)
		{
			yield return new WaitUntil(() => (SceneManager.sceneCount == 1 && stopLevelPreviewCoroutine == null));
			if (index == _SceneManager.CurrentScene.buildIndex)
			{
				Level.instance.gameObject.SetActive(true);
				Snake.instance.SetSkinParts (Snake.instance.skin);
			}
			else
			{
				GameplayMenu.instance.trs.SetParent(null);
				Level.instance.gameObject.SetActive(false);
				yield return _SceneManager.instance.LoadSceneAsyncAdditiveWithoutTransition(index + LEVEL_1_SCENE_INDEX);
			}
			selectedLevelIndex = index;
			yield return new WaitForEndOfFrame();
			yield return new WaitForEndOfFrame();
			GameplayMenu.instance.SetOrientation ();
			startLevelButton.interactable = true;
			startLevelPreviewCoroutine = null;
		}

		public void StopLevelPreview (int index)
		{
			if (index != _SceneManager.CurrentScene.buildIndex - LEVEL_1_SCENE_INDEX && SceneManager.sceneCount == 2 && SceneManager.GetSceneByBuildIndex(index + LEVEL_1_SCENE_INDEX).isLoaded)
			{
				if (startLevelPreviewCoroutine != null)
					StopCoroutine(startLevelPreviewCoroutine);
				startLevelPreviewCoroutine = null;
				if (stopLevelPreviewCoroutine != null)
					StopCoroutine(stopLevelPreviewCoroutine);
				stopLevelPreviewCoroutine = StartCoroutine(StopLevelPreviewRoutine (index));
			}
		}

		IEnumerator StopLevelPreviewRoutine (int index)
		{
			yield return SceneManager.UnloadSceneAsync(index + LEVEL_1_SCENE_INDEX, UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);
			Level.instance.gameObject.SetActive(true);
			stopLevelPreviewCoroutine = null;
		}

		public void StartSelectedLevel ()
		{
			if (levelButtons[(int) selectedLevelIndex].button.interactable)
				_SceneManager.instance.LoadSceneWithoutTransition ((int) selectedLevelIndex + LEVEL_1_SCENE_INDEX);
		}

		public void HideAllLevelButtons ()
		{
			if (instance != this)
			{
				instance.HideAllLevelButtons ();
				return;
			}
			for (int i = 0; i < levelButtons.Length; i ++)
			{
				LevelButton levelButton = levelButtons[i];
				levelButton.button.gameObject.SetActive(false);
				levelButton.indicatorGo.SetActive(true);
			}
		}

		public void Close ()
		{
			if (selectedLevelIndex != null)
				StopLevelPreview ((int) selectedLevelIndex);
			StartCoroutine(CloseRoutine ());
		}

		IEnumerator CloseRoutine ()
		{
			yield return new WaitUntil(() => (Level.Instance != null && Level.instance.gameObject.activeSelf));
			for (int i = 0; i < VR_UIPointer.instances.Length; i ++)
			{
				VR_UIPointer vR_UIPointer = VR_UIPointer.instances[i];
				vR_UIPointer.uiPlaneTrs = GameplayMenu.instance.trs;
			}
			gameObject.SetActive(false);
			GameplayMenu.instance.SetOrientation ();
			GameplayMenu.instance.optionsParentGo.SetActive(true);
			startLevelButton.interactable = false;
		}
	}
}