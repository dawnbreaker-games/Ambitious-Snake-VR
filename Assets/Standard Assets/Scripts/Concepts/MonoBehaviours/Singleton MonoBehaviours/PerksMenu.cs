using TMPro;
using System;
using UnityEngine;
using System.Collections;

namespace AmbitiousSnake
{
	public class PerksMenu : SingletonMonoBehaviour<PerksMenu>
	{
		public Transform trs;
		public TMP_Text scoreText;
		public Perk[] perks = new Perk[0];
		public static Snake.Skin defaultSkin;
		public static int CurrentSkinPerkIndex
		{
			get
			{
				return PlayerPrefs.GetInt("Current skin perk index", -1);
			}
			set
			{
				PlayerPrefs.SetInt("Current skin perk index", value);
			}
		}

		public override void Awake ()
		{
			base.Awake ();
			defaultSkin = Snake.Instance.skin;
			if (CurrentSkinPerkIndex != -1)
			{
				SkinPerk skinPerk = (SkinPerk) perks[CurrentSkinPerkIndex];
				skinPerk.Equip ();
			}
			else
				Snake.instance.SetSkin (defaultSkin);
			gameObject.SetActive(false);
		}

		void OnEnable ()
		{
			for (int i = 0; i < VR_UIPointer.instances.Length; i ++)
			{
				VR_UIPointer vR_UIPointer = VR_UIPointer.instances[i];
				vR_UIPointer.uiPlaneTrs = trs;
			}
			scoreText.text = "Score: " + SaveAndLoadManager.saveData.score;
		}

		public void Close ()
		{
			for (int i = 0; i < VR_UIPointer.instances.Length; i ++)
			{
				VR_UIPointer vR_UIPointer = VR_UIPointer.instances[i];
				vR_UIPointer.uiPlaneTrs = GameplayMenu.instance.trs;
			}
			gameObject.SetActive(false);
			GameplayMenu.instance.SetOrientation ();
			GameplayMenu.instance.optionsParentGo.SetActive(true);
		}
	}
}