using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Extensions;

namespace AmbitiousSnake
{
	[ExecuteInEditMode]
	public class LevelButton : MonoBehaviour
	{
		public int unlockAtScore;
		public Transform trs;
		public TMP_Text unlockText;
		public TMP_Text levelNameText;
		public TMP_Text bestTimeText;
		public TMP_Text parTimeText;
		public TMP_Text triesRemainingInMasteryModeText;
		public Image parTimeIcon;
		public Image starIcon;
		public Button button;
		public float maxDoubleClickRate;
		public GameObject infoParentGo;
		public GameObject indicatorGo;
		float lastClickedTime;

		void OnEnable ()
		{
			Level level;
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				if (trs == null)
					trs = GetComponent<Transform>();
				if (button == null)
					button = trs.Find("Button").GetComponent<Button>();
				if (indicatorGo == null)
					indicatorGo = trs.Find("Indicator").gameObject;
				Transform buttonTrs = button.GetComponent<Transform>();
				if (infoParentGo == null)
					infoParentGo = buttonTrs.Find("Vertical Layout Group").gameObject;
				if (unlockText == null)
					unlockText = buttonTrs.Find("Unlock Text").GetComponent<TMP_Text>();
				Transform infoParentTrs = infoParentGo.GetComponent<Transform>();
				Transform horizontalLayoutGroupTrs = infoParentTrs.Find("Horizontal Layout Group");
				if (levelNameText == null)
					levelNameText = horizontalLayoutGroupTrs.Find("Level Name Text").GetComponent<TMP_Text>();
				if (bestTimeText == null)
					bestTimeText = infoParentTrs.Find("Best Time Text").GetComponent<TMP_Text>();
				if (parTimeText == null)
					parTimeText = infoParentTrs.Find("Par Time Text").GetComponent<TMP_Text>();
				if (triesRemainingInMasteryModeText == null)
					triesRemainingInMasteryModeText = infoParentTrs.Find("Tries Remaining Text").GetComponent<TMP_Text>();
				if (parTimeIcon == null)
					parTimeIcon = horizontalLayoutGroupTrs.Find("Par Time Icon").GetComponent<Image>();
				if (starIcon == null)
					starIcon = horizontalLayoutGroupTrs.Find("Star Icon").GetComponent<Image>();
				levelNameText.text = "" + (trs.GetSiblingIndex() + 1);
				LevelSelect.instance = trs.root.GetComponentInChildren<LevelSelect>();
				level = LevelSelect.instance.levels[trs.GetSiblingIndex()];
				parTimeText.text = "Par time: " + level.parTime;
				unlockText.text = "" + unlockAtScore + " to unlock";
				return;
			}
#endif
			level = LevelSelect.Instance.levels[trs.GetSiblingIndex()];
			triesRemainingInMasteryModeText.gameObject.SetActive(false);
			if (GameManager.ModifierIsActive("Mastery mode"))
			{
				if (SaveAndLoadManager.saveData.masteryScore < unlockAtScore)
				{
					Disable ();
					return;
				}
				else
				{
					unlockText.gameObject.SetActive(false);
					triesRemainingInMasteryModeText.gameObject.SetActive(true);
					triesRemainingInMasteryModeText.text = "Tries remaining: " + level.TriesRemainingInMasteryMode;
					if (level.TriesRemainingInMasteryMode == 0)
					{
						button.interactable = false;
						return;
					}
				}
			}
			else if (SaveAndLoadManager.saveData.score < unlockAtScore)
			{
				Disable ();
				return;
			}
			button.interactable = true;
			unlockText.gameObject.SetActive(false);
			lastClickedTime = Mathf.NegativeInfinity;
			if (level.HasWon)
				bestTimeText.text = "Best time: " + string.Format("{0:0.#}", level.FastestTime);
			parTimeIcon.enabled = level.GotParTime;
			starIcon.enabled = level.CollectedStar;
			button.onClick.RemoveAllListeners();
			button.onClick.AddListener(OnButtonClicked);
		}

		void Disable ()
		{
			button.interactable = false;
			infoParentGo.SetActive(false);
		}

		void OnButtonClicked ()
		{
			if (Time.realtimeSinceStartup - lastClickedTime <= maxDoubleClickRate)
				_SceneManager.instance.LoadSceneWithoutTransition (trs.GetSiblingIndex() + LevelSelect.LEVEL_1_SCENE_INDEX);
			lastClickedTime = Time.realtimeSinceStartup;
			if (LevelSelect.selectedLevelIndex != null)
				LevelSelect.instance.StopLevelPreview ((int) LevelSelect.selectedLevelIndex);
			LevelSelect.selectedLevelIndex = trs.GetSiblingIndex();
			LevelSelect.instance.StartLevelPreview ((int) LevelSelect.selectedLevelIndex);
		}
	}
}