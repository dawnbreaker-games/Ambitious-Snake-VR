﻿using System;
using Extensions;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace AmbitiousSnake
{
	public class VR_UIPointer : SingletonUpdateWhileEnabled<VR_UIPointer>
	{
		public Transform trs;
		public Transform pointerTrs;
		public GameObject graphicsGo;
		public Transform uiPlaneTrs;
		public Plane uiPlane;
		public ComplexTimer selectableColorMultiplier;
		public float minDistanceToRotate;
		public string submitInputVariablePath;
		public static VR_UIPointer[] instances = new VR_UIPointer[0];
		float minDistanceToRotateSqr;
		Vector3 rotateEffectorPosition;
		_Selectable hoveredOver;
		_Selectable previousHoveredOver;
		Vector3 previousPosition;
		bool submitInput;
		bool previousSubmitInput;
		Vector2 sliderInitDragPosition;

		void Start ()
		{
			minDistanceToRotateSqr = minDistanceToRotate * minDistanceToRotate;
			trs.SetParent(null);
			instances = instances.Add(this);
		}

		void OnDestroy ()
		{
			instances = instances.Remove(this);
		}
		
		public override void DoUpdate ()
		{
			if (uiPlaneTrs == null)
			{
				Destroy(gameObject);
				return;
			}
			if (!uiPlaneTrs.gameObject.activeSelf)
			{
				graphicsGo.SetActive(false);
				return;
			}
			submitInput = InputManager.instance.GetMember<bool>(submitInputVariablePath);
			uiPlane = new Plane(uiPlaneTrs.forward, uiPlaneTrs.position);
			Vector3 position = VectorExtensions.INFINITE3;
			if (uiPlane.Raycast(new Ray(pointerTrs.position, pointerTrs.forward), out position))
			{
				if (position != previousPosition)
				{
					trs.position = position;
					if ((position - rotateEffectorPosition).sqrMagnitude >= minDistanceToRotateSqr)
					{
						trs.rotation = Quaternion.LookRotation(pointerTrs.forward, position - rotateEffectorPosition);
						rotateEffectorPosition = position + (rotateEffectorPosition - position).normalized * minDistanceToRotate;
					}
					previousPosition = position;
					hoveredOver = null;
					for (int i = 0; i < _Selectable.instances.Length; i ++)
					{
						_Selectable selectable = _Selectable.instances[i];
						if (selectable.rectTrs.rect.Contains(selectable.rectTrs.InverseTransformPoint(position)))
						{
							hoveredOver = selectable;
							break;
						}
					}
				}
				graphicsGo.SetActive(true);
			}
			else
			{
				hoveredOver = null;
				graphicsGo.SetActive(false);
			}
			if (hoveredOver != null && hoveredOver.enabled)
			{
				if (previousHoveredOver != hoveredOver)
				{
					if (previousHoveredOver != null)
					{
						if (previousHoveredOver.onDeselected != null)
							previousHoveredOver.onDeselected.Invoke();
						SetSelectableColorMultiplier (previousHoveredOver.selectable, 1);
					}
					selectableColorMultiplier.JumpToInitValue ();
				}
				if (hoveredOver.onSelected != null)
					hoveredOver.onSelected.Invoke();
				SetSelectableColorMultiplier (hoveredOver.selectable, selectableColorMultiplier.GetValue());
				if (submitInput && hoveredOver.selectable.interactable)
				{
					if (!previousSubmitInput)
					{
						Button button = hoveredOver.selectable as Button;
						if (button != null)
						{
							if (button.interactable)
								button.onClick.Invoke();
						}
						else
						{
							Toggle toggle = hoveredOver.selectable as Toggle;
							if (toggle != null)
							{
								toggle.isOn = !toggle.isOn;
								toggle.onValueChanged.Invoke(toggle.isOn);
							}
							else
							{
								Scrollbar scrollbar = hoveredOver.selectable as Scrollbar;
								if (scrollbar != null)
									sliderInitDragPosition = Rect.PointToNormalized(hoveredOver.rectTrs.rect, hoveredOver.rectTrs.InverseTransformPoint(trs.position));
								else
								{
									Slider slider = hoveredOver.selectable as Slider;
									if (slider != null)
										sliderInitDragPosition = Rect.PointToNormalized(hoveredOver.rectTrs.rect, hoveredOver.rectTrs.InverseTransformPoint(trs.position));
								}
							}
						}
					}
					else
					{
						Scrollbar scrollbar = hoveredOver.selectable as Scrollbar;
						if (scrollbar != null)
						{
							if (scrollbar.direction == Scrollbar.Direction.BottomToTop)
								scrollbar.value += (Rect.PointToNormalized(hoveredOver.rectTrs.rect, hoveredOver.rectTrs.InverseTransformPoint(trs.position)).y - sliderInitDragPosition.y);
							else
								throw new NotImplementedException();
						}
						else
						{
							Slider slider = hoveredOver.selectable as Slider;
							if (slider != null)
							{
								if (slider.direction == Slider.Direction.LeftToRight)
									slider.value += (Rect.PointToNormalized(hoveredOver.rectTrs.rect, hoveredOver.rectTrs.InverseTransformPoint(trs.position)).x - sliderInitDragPosition.x);
								else
									throw new NotImplementedException();
							}
						}
					}
				}
			}
			else if (previousHoveredOver != null)
			{
				if (previousHoveredOver.onDeselected != null)
					previousHoveredOver.onDeselected.Invoke();
				SetSelectableColorMultiplier (previousHoveredOver.selectable, 1);
			}
			previousHoveredOver = hoveredOver;
			previousSubmitInput = submitInput;
		}

		void SetSelectableColorMultiplier (Selectable selectable, float colorMultiplier)
		{
			ColorBlock colorBlock = selectable.colors;
			colorBlock.colorMultiplier = colorMultiplier;
			selectable.colors = colorBlock;
		}
	}
}