using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;
using System;

[Serializable]
public class LineSegment3D
{
	public Vector3 start;
	public Vector3 end;

	public LineSegment3D ()
	{
	}

	public LineSegment3D (Vector3 start, Vector3 end)
	{
		this.start = start;
		this.end = end;
	}

	public override string ToString ()
	{
		return "[" + start + "], [" + end + "]";
	}

#if UNITY_EDITOR
	public void DrawGizmos (Color color)
	{
		GizmosManager.GizmosEntry gizmosEntry = new GizmosManager.GizmosEntry();
		gizmosEntry.setColor = true;
		gizmosEntry.color = color;
		gizmosEntry.onDrawGizmos += DrawGizmos;
		GizmosManager.gizmosEntries.Add(gizmosEntry);
	}

	public void DrawGizmos (params object[] args)
	{
		Gizmos.DrawLine(start, end);
	}
#endif
	
	public bool ContainsPoint (Vector3 point)
	{
		return Vector3.Distance(point, start) + Vector3.Distance(point, end) == Vector3.Distance(start, end);
	}
	
	public LineSegment3D Move (Vector3 movement)
	{
		return new LineSegment3D(start + movement, end + movement);
	}
	
	public LineSegment3D Rotate (Vector3 pivotPoint, Quaternion rotation)
	{
		Vector3 outputStart = start.Rotate(pivotPoint, rotation);
		Vector3 outputEnd = end.Rotate(pivotPoint, rotation);
		return new LineSegment3D(outputStart, outputEnd);
	}

	public Vector3 ClosestPoint (Vector3 point)
	{
		Vector3 output;
		float directedDistanceAlongParallel = GetDirectedDistanceAlongParallel(point);
		if (directedDistanceAlongParallel > 0 && directedDistanceAlongParallel < GetLength())
			output = GetPointWithDirectedDistance(directedDistanceAlongParallel);
		else if (directedDistanceAlongParallel >= GetLength())
			output = end;
		else
			output = start;
		return output;
	}

	public Vector3 GetMidpoint ()
	{
		return (start + end) / 2;
	}

	public float GetDirectedDistanceAlongParallel (Vector3 point)
	{
		Quaternion rotation = Quaternion.LookRotation(end - start);
		rotation = Quaternion.Inverse(rotation);
		LineSegment3D rotatedLine = Rotate(Vector3.zero, rotation);
		point = rotation * point;
		return point.z - rotatedLine.start.z;
	}

	public Vector3 GetPointWithDirectedDistance (float directedDistance)
	{
		return start + (GetDirection() * directedDistance);
	}

	public float GetLength ()
	{
		return Vector3.Distance(start, end);
	}

	public float GetLengthSqr ()
	{
		return (start - end).sqrMagnitude;
	}

	public Vector3 GetDirection ()
	{
		return (end - start).normalized;
	}
	
	public LineSegment3D Flip ()
	{
		return new LineSegment3D(end, start);
	}

	public bool DoIIntersectWithSphere (Sphere sphere)
	{
		return (ClosestPoint(sphere.center) - sphere.center).sqrMagnitude <= sphere.radius * sphere.radius;
	}

	public bool DoIIntersectWithLineSegment (LineSegment3D other, bool shouldIncludeEndPoints)
	{
		throw new NotImplementedException();
	}
}