using UnityEngine;

namespace AmbitiousSnake
{
	public class MultiplayerSnakeSpawnPoint : MonoBehaviour
	{
		public Transform trs;
		public ushort useWithPlayerCount;
	}
}