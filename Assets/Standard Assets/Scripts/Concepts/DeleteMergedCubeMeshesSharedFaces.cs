using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
using Unity.EditorCoroutines.Editor;
#endif

namespace AmbitiousSnake
{
	public class DeleteMergedCubeMeshesSharedFaces
	{
		const float SMALL_VALUE = 0.1f;

		public static void Do (Mesh mesh)
		{
#if UNITY_EDITOR
			EditorCoroutineUtility.StartCoroutine(DoRoutine (mesh), Object.FindObjectOfType<MonoBehaviour>());
#else
			Object.FindObjectOfType<MonoBehaviour>().StartCoroutine(DoRoutine (mesh));
#endif
		}

		static IEnumerator DoRoutine (Mesh mesh)
		{
			MonoBehaviour.print("DeleteMeshSharedFaces operation began for " + mesh.name);
			List<Vector3> vertices = new List<Vector3>();
			mesh.GetVertices(vertices);
			List<int> faceIndices = new List<int>(mesh.GetTriangles(0));
			for (int i = 0; i < faceIndices.Count - 12; i += 3)
			{
				Vector3[] faceVertices = mesh.GetFaceVertices(i / 3, vertices, faceIndices);
				Vector3 faceCenter = BoundsExtensions.FromPoints(faceVertices).center;
				for (int i2 = i + 3; i2 < faceIndices.Count - 9; i2 += 3)
				{
					Vector3[] face2Vertices = mesh.GetFaceVertices(i2 / 3, vertices, faceIndices);
					if ((faceCenter - BoundsExtensions.FromPoints(face2Vertices).center).sqrMagnitude <= SMALL_VALUE * SMALL_VALUE)
					{
						bool removedFaces = false;
						for (int i3 = i2 + 3; i3 < faceIndices.Count - 6; i3 += 3)
						{
							Vector3[] face3Vertices = mesh.GetFaceVertices(i3 / 3, vertices, faceIndices);
							if ((faceCenter - BoundsExtensions.FromPoints(face3Vertices).center).sqrMagnitude <= SMALL_VALUE * SMALL_VALUE)
							{
								for (int i4 = i3 + 3; i4 < faceIndices.Count - 3; i4 += 3)
								{
									Vector3[] face4Vertices = mesh.GetFaceVertices(i4 / 3, vertices, faceIndices);
									if ((faceCenter - BoundsExtensions.FromPoints(face4Vertices).center).sqrMagnitude <= SMALL_VALUE * SMALL_VALUE)
									{
										mesh.RemoveFace (i / 3, ref faceIndices);
										i -= 3;
										i2 -= 3;
										i3 -= 3;
										i4 -= 3;
										mesh.RemoveFace (i2 / 3, ref faceIndices);
										i3 -= 3;
										i4 -= 3;
										mesh.RemoveFace (i3 / 3, ref faceIndices);
										i4 -= 3;
										mesh.RemoveFace (i4 / 3, ref faceIndices);
										MonoBehaviour.print("DeleteMeshSharedFaces operation for " + mesh.name + " removed 4 faces");
										removedFaces = true;
										break;
									}
									yield return new WaitForEndOfFrame();
								}
								if (removedFaces)
									break;
							}
						}
						if (removedFaces)
							break;
					}
				}
			}
			MonoBehaviour.print("DeleteMeshSharedFaces operation done for " + mesh.name);
		}

#if UNITY_EDITOR
		[MenuItem("Tools/Delete shared faces of selected shared merged cube Meshes")]
		static void _DoToSelectedSharedMeshes ()
		{
			MeshFilter[] meshFilters = SelectionExtensions.GetSelected<MeshFilter>();
			for (int i = 0; i < meshFilters.Length; i ++)
			{
				MeshFilter meshFilter = meshFilters[i];
				Do (meshFilter.sharedMesh);
			}
		}
		[MenuItem("Tools/Delete shared faces of selected merged cube Meshes")]
		static void _DoToSelectedMeshes ()
		{
			MeshFilter[] meshFilters = SelectionExtensions.GetSelected<MeshFilter>();
			for (int i = 0; i < meshFilters.Length; i ++)
			{
				MeshFilter meshFilter = meshFilters[i];
				Do (meshFilter.mesh);
			}
		}
#endif
	}
}