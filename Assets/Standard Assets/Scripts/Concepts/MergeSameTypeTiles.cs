#if PROBUILDER
#if UNITY_EDITOR
using Extensions;
using UnityEditor;
using UnityEngine;
using System.Collections;
using UnityEngine.ProBuilder;
using System.Collections.Generic;

namespace AmbitiousSnake
{
	[ExecuteInEditMode]
	public class MergeSameTypeTiles
	{
		public static void Do (Tile[] tiles)
		{
			List<Tile> tilesWithNoPrefab = new List<Tile>();
			Dictionary<Transform, List<ProBuilderMesh>> allProBuilderMeshesToCombine = new Dictionary<Transform, List<ProBuilderMesh>>();
			for (int i = 0; i < tiles.Length; i ++)
			{
				Tile tile = tiles[i];
				if (tile.neighbors.Length == 0)
				{
					Transform tileTrs = tile.trs;
					Collider collider = tileTrs.GetComponent<Collider>();
					Bounds bounds = collider.bounds;
					Transform trsPrefab = PrefabUtility.GetCorrespondingObjectFromSource(tileTrs);
					if (trsPrefab == null)
						tilesWithNoPrefab.Add(tile);
					else
					{
						List<ProBuilderMesh> combineProBuilderMeshes = new List<ProBuilderMesh>();
						Transform[] newTransforms = MakeGridOfTransformPrefabsInBounds._Do(trsPrefab, bounds, Vector3.one, new BoundsOffset(Vector3.one / 2, Vector3.zero), tileTrs.parent);
						for (int i2 = 0; i2 < newTransforms.Length; i2 ++)
						{
							Transform newTrs = newTransforms[i2];
							ProBuilderMesh proBuilderMesh = newTrs.GetComponentInChildren<ProBuilderMesh>();
							if (!allProBuilderMeshesToCombine.ContainsKey(trsPrefab))
								allProBuilderMeshesToCombine.Add(trsPrefab, new List<ProBuilderMesh> { proBuilderMesh });
							else
								allProBuilderMeshesToCombine[trsPrefab].Add(proBuilderMesh);
						}
						GameManager.DestroyOnNextEditorUpdate (tile.gameObject);
					}
				}
			}
			foreach (List<ProBuilderMesh> proBuilderMeshes in allProBuilderMeshesToCombine.Values)
			{
				ProBuilderMesh[] combinedProBuilderMeshes = MergeProBuilderMeshes._Do(proBuilderMeshes.ToArray(), int.MaxValue);
				for (int i = 0; i < combinedProBuilderMeshes.Length; i ++)
				{
					ProBuilderMesh proBuilderMesh = combinedProBuilderMeshes[i];
					DeleteProBuilderMeshSharedFaces.Do (proBuilderMesh);
					BoxCollider boxCollider = proBuilderMesh.GetComponentInParent<BoxCollider>();
					GameManager.DestroyOnNextEditorUpdate (boxCollider);
					proBuilderMesh.gameObject.AddComponent<MeshCollider>();
				}
			}
		}

		[MenuItem("Tools/Merge selected same type Tiles")]
		static void DoToSelected ()
		{
			Do (SelectionExtensions.GetSelected<Tile>());
		}

		[MenuItem("Tools/Merge all same type Tiles")]
		static void DoToAll ()
		{
			Do (GameManager.FindObjectsOfType<Tile>());
		}
	}
}
#endif
#endif