﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;
using UnityEngine.InputSystem;

namespace AmbitiousSnake
{
	public class VRCameraRig : SingletonMonoBehaviour<VRCameraRig>, IUpdatable
	{
		public new Camera camera;
		public Transform trackingSpaceTrs;
		public Transform eyesTrs;
		public Transform leftHandTrs;
		public Transform rightHandTrs;
		public Transform trs;
		public float lookRate;
		public LayerMask whatICollideWith;
		[HideInInspector]
		public float cameraDistance;
		[HideInInspector]
		public Vector3 positionOffset;
		[HideInInspector]
		public Quaternion rota;
		bool setOrientationInput;
		bool previousSetOrientationInput;
		
		void OnEnable ()
		{
			instance = this;
			Init ();
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public void Init ()
		{
			positionOffset = trs.localPosition;
			cameraDistance = positionOffset.magnitude;
			trs.SetParent(null);
			SetOrientation ();
		}

		public void DoUpdate ()
		{
			setOrientationInput = InputManager.SetOrientationInput;
			if (InputManager.Instance.inputDevice == InputManager.InputDevice.VR)
				UpdateAnchors ();
			else
			{
				Vector2 rotaInput = Mouse.current.delta.ReadValue().FlipY() * lookRate * Time.unscaledDeltaTime;
				if (rotaInput != Vector2.zero)
				{
					trackingSpaceTrs.RotateAround(trackingSpaceTrs.position, trackingSpaceTrs.right, rotaInput.y);
					trackingSpaceTrs.RotateAround(trackingSpaceTrs.position, Vector3.up, rotaInput.x);
				}
				RaycastHit hit;
				if (Physics.Raycast(Snake.Instance.HeadPosition, trs.position - Snake.instance.HeadPosition, out hit, cameraDistance, whatICollideWith))
					positionOffset = positionOffset.normalized * (hit.distance - camera.nearClipPlane);
				else
					positionOffset = positionOffset.normalized * cameraDistance;
				trs.position = Snake.instance.HeadPosition + (rota * positionOffset);
			}
			if (setOrientationInput && (!previousSetOrientationInput || InputManager.instance.inputDevice == InputManager.InputDevice.KeyboardAndMouse))
				SetOrientation ();
			previousSetOrientationInput = setOrientationInput;
		}

		void OnDisable ()
		{
			GameManager.updatables = GameManager.updatables.Remove(this);
		}

		void UpdateAnchors ()
		{
			RaycastHit hit;
			if (Physics.Raycast(Snake.instance.HeadPosition, trs.position - Snake.instance.HeadPosition, out hit, cameraDistance, whatICollideWith))
				positionOffset = positionOffset.normalized * (hit.distance - camera.nearClipPlane);
			else
				positionOffset = positionOffset.normalized * cameraDistance;
			trs.position = Snake.instance.HeadPosition + (rota * positionOffset);
			Vector3 desiredLocalViewPosition = eyesTrs.localPosition;
			if (InputManager.HeadPosition != null)
			{
				desiredLocalViewPosition = (Vector3) InputManager.HeadPosition;
				Vector3 toDesiredViewPosition = trs.TransformPoint(desiredLocalViewPosition) - trs.position;
				if (Physics.Raycast(trs.position, toDesiredViewPosition, out hit, toDesiredViewPosition.magnitude, whatICollideWith))
					eyesTrs.localPosition = desiredLocalViewPosition.normalized * (hit.distance - camera.nearClipPlane);
				else
					eyesTrs.localPosition = desiredLocalViewPosition;
			}
			if (InputManager.HeadRotation != null)
				eyesTrs.localRotation = (Quaternion) InputManager.HeadRotation;
			if (InputManager.LeftHandPosition != null)
				leftHandTrs.localPosition = (Vector3) InputManager.LeftHandPosition + (eyesTrs.localPosition - desiredLocalViewPosition);
			if (InputManager.LeftHandRotation != null)
				leftHandTrs.localRotation = (Quaternion) InputManager.LeftHandRotation;
			if (InputManager.RightHandPosition != null)
				rightHandTrs.localPosition = (Vector3) InputManager.RightHandPosition + (eyesTrs.localPosition - desiredLocalViewPosition);
			if (InputManager.RightHandRotation != null)
				rightHandTrs.localRotation = (Quaternion) InputManager.RightHandRotation;
		}
		
		public void SetOrientation ()
		{
			rota = eyesTrs.rotation;
			if (InputManager.instance.inputDevice != InputManager.InputDevice.KeyboardAndMouse)
				trackingSpaceTrs.forward = eyesTrs.forward.GetXZ();
		}
	}
}