using UnityEngine;
using System.Collections.Generic;

namespace AmbitiousSnake
{
	public class Star_RPG : Star
	{
		public static List<Star_RPG> instances = new List<Star_RPG>();

		void Awake ()
		{
			instances.Add(this);
		}

		void OnDestroy ()
		{
			instances.Remove(this);
		}
	}
}