using UnityEngine;
using System.Collections.Generic;

namespace AmbitiousSnake
{
	public class GrubWizard : Enemy
	{
		public float maxShootDistance;
		float maxShootDistanceSqr;

		public override void Awake ()
		{
			base.Awake ();
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			maxShootDistanceSqr	= maxShootDistance * maxShootDistance;
		}
		
		public override void HandleRotating ()
		{
			trs.forward = lastKnownSnakePiecePosition - trs.position;
		}

		public override void HandleMoving ()
		{
			move = (lastKnownSnakePiecePosition - trs.position).normalized;
			if (canSeeSnake && (trs.position - lastKnownSnakePiecePosition).sqrMagnitude < targetDistanceFromSnakeSqr)
				move *= -1;
			rigid.velocity = move * moveSpeed;
		}

		public override void HandleAttacking ()
		{
			if (canSeeSnake && (trs.position - lastKnownSnakePiecePosition).sqrMagnitude <= maxShootDistanceSqr)
				PlayAnimationEntry ("Shoot");
		}

		public override void HandlePathfinding ()
		{
			if (canSeeSnake)
				lastKnownSnakePiecePosition = targetSnakePieceTrs.position;
		}

		public void Shoot ()
		{
			foreach (KeyValuePair<string, BulletPatternEntry> keyValuePair in bulletPatternEntriesDict)
			{
				Bullet[] bullets = keyValuePair.Value.Shoot();
				for (int i = 0; i < bullets.Length; i ++)
				{
					Bullet bullet = bullets[i];
					Bomb bomb = bullet as Bomb;
					if (bomb != null)
					{
						bomb.dead = true;
						bomb.explodeDelayTimer.Reset ();
						bomb.explodeDelayTimer.Start ();
					}
				}
			}
		}
	}
}