﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;
using TMPro;
using System;
using Random = UnityEngine.Random;

namespace AmbitiousSnake
{
	public class Snake : SingletonMonoBehaviour<Snake>, IDestructable, IUpdatable
	{
		public Transform trs;
		public Rigidbody rigid;
		public float moveSpeed;
		[HideInInspector]
		public float hp;
		public float Hp
		{
			get
			{
				return hp;
			}
			set
			{
				hp = value;
			}
		}
		public int maxHp;
		public int MaxHp
		{
			get
			{
				return maxHp;
			}
			set
			{
				maxHp = value;
			}
		}
		public TMP_Text hpText;
		[HideInInspector]
		public bool dead;
		public float maxDistanceBetweenPieces;
		public float minCollisionSoundEffectTimeSeparation;
		public float minCollisionSoundEffectDistanceSeparation;
		public float minAngleBetweenCollisionNormalsToMakeSound;
		public List<SnakePiece> pieces = new List<SnakePiece>();
		public LayerMask whatICrashInto;
		public ClampedFloat length;
		public float currentLength;
		public SnakePiece piecePrefab;
		public float changeLengthRate;
		public CollisionSoundEffectEntry[] collisionSoundEffectEntries = new CollisionSoundEffectEntry[0];
		public Dictionary<CollisionSoundEffectEntry.Type, CollisionSoundEffectEntry> collisionSoundEffectEntriesDict = new Dictionary<CollisionSoundEffectEntry.Type, CollisionSoundEffectEntry>();
		public AudioClip unstuckFromStickyTileSoundEffect;
		public LineRenderer lengthIndicator;
		public delegate void OnAddHeadPiece(Vector3 position);
		public event OnAddHeadPiece onAddHeadPiece;
		public delegate void OnAddTailPiece(Vector3 position);
		public event OnAddTailPiece onAddTailPiece;
		public delegate void OnRemoveTailPiece();
		public event OnRemoveTailPiece onRemoveTailPiece;
		public AudioSource headMovementAudioSource;
		public Transform headMovementAudioSourceTrs;
		public AudioSource tailMovementAudioSource;
		public Transform tailMovementAudioSourceTrs;
		public Skin skin;
		public Animator animator;
		public Transform eyesParentTrs;
		public Transform leftEyeTrs;
		public Transform rightEyeTrs;
		public delegate void OnDeath();
		public event OnDeath onDeath;
		[HideInInspector]
		public Vector3 move;
		public SnakePiece HeadPiece
		{
			get
			{
				return pieces[pieces.Count - 1];
			}
		}
		public SnakePiece TailPiece
		{
			get
			{
				return pieces[0];
			}
		}
		public Vector3 HeadPosition
		{
			get
			{
				return HeadPiece.trs.position;
			}
		}
		public Vector3 TailPosition
		{
			get
			{
				return TailPiece.trs.position;
			}
		}
		public Vector3 HeadLocalPosition
		{
			get
			{
				return HeadPiece.trs.localPosition;
			}
		}
		public Vector3 TailLocalPosition
		{
			get
			{
				return TailPiece.trs.localPosition;
			}
		}
		[HideInInspector]
		public LayerMask whatICrashIntoExcludingSnakes;
		float changeLengthInput;
		Dictionary<Collider, ContactPoint[]> contactPointsWithCollidersDict = new Dictionary<Collider, ContactPoint[]>();
		List<CollisionPoint> collisionPoints = new List<CollisionPoint>();
		RuntimeAnimatorController defaultRuntimeAnimatorController;
		float minCollisionSoundEffectDistanceSeparationSqr;
		bool isDead;

		public override void Awake ()
		{
			base.Awake ();
			for (int i = 0; i < collisionSoundEffectEntries.Length; i ++)
			{
				CollisionSoundEffectEntry collisionSoundEffectEntry = collisionSoundEffectEntries[i];
				collisionSoundEffectEntriesDict.Add(collisionSoundEffectEntry.type, collisionSoundEffectEntry);
			}
			InitPieces ();
			HeadPiece.trs.forward = trs.forward;
			eyesParentTrs.position = HeadPiece.trs.position;
			eyesParentTrs.forward = trs.forward;
			defaultRuntimeAnimatorController = animator.runtimeAnimatorController;
			lengthIndicator.SetPosition(0, Vector3.up * currentLength / length.valueRange.max);
			minCollisionSoundEffectDistanceSeparationSqr = minCollisionSoundEffectDistanceSeparation * minCollisionSoundEffectDistanceSeparation;
			whatICrashIntoExcludingSnakes = whatICrashInto.RemoveFromMask("Snake");
		}
		
		public virtual void OnEnable ()
		{
			instance = this;
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public virtual void InitPieces ()
		{
			Vector3 headPosition = trs.position + Vector3.up * Physics.defaultContactOffset;
			AddHeadPiece (headPosition, 0);
			for (float distance = maxDistanceBetweenPieces; distance <= length.value; distance += maxDistanceBetweenPieces)
				AddTailPiece (headPosition - trs.forward * distance, maxDistanceBetweenPieces);
		}
		
		public void Death ()
		{
			isDead = true;
			if (onDeath != null)
				onDeath ();
			else
			{
				if (GameManager.ModifierIsActive("Mastery mode"))
				{
					Level.instance.TriesRemainingInMasteryMode --;
					if (Level.instance.TriesRemainingInMasteryMode == 0)
					{
						for (int i = 0; i < LevelSelect.instance.levels.Length; i ++)
						{
							Level level = LevelSelect.instance.levels[i];
							LevelButton levelButton = LevelSelect.instance.levelButtons[i];
							if (SaveAndLoadManager.saveData.masteryScore >= levelButton.unlockAtScore && level.TriesRemainingInMasteryMode > 0)
								return;
						}
						GameplayMenu.instance.ResetMasteryData ();
						_SceneManager.instance.LoadSceneWithoutTransition (0);
						return;
					}
				}
				_SceneManager.instance.RestartSceneWithoutTransition ();
			}
		}
		
		public virtual void DoUpdate ()
		{
			if (GameManager.paused || dead)
			{
				headMovementAudioSource.Pause();
				tailMovementAudioSource.Pause();
				return;
			}
			HandleMovement ();
			changeLengthInput = InputManager.ChangeLengthInput;
			SetLength (Mathf.Clamp(length.value + changeLengthInput * changeLengthRate * Time.deltaTime, length.valueRange.min, length.valueRange.max));
			rigid.ResetCenterOfMass();
			rigid.ResetInertiaTensor();
			if (move.sqrMagnitude == 0)
				headMovementAudioSource.Pause();
			else
			{
				HeadPiece.trs.forward = move;
				headMovementAudioSourceTrs.position = HeadPosition;
				eyesParentTrs.position = HeadPosition;
				eyesParentTrs.forward = move;
			}
			if (changeLengthInput == 0)
				tailMovementAudioSource.Pause();
			else
				tailMovementAudioSourceTrs.position = TailPosition;
			contactPointsWithCollidersDict.Clear();
		}

		public virtual void TakeDamage (float amount, Hazard source)
		{
			if (dead)
				return;
			hp = Mathf.Clamp(hp - amount, 0, MaxHp);
			if (hpText != null)
				hpText.text = "Health: " + hp;
			if (hp == 0)
			{
				dead = true;
				Death ();
			}
		}

		public virtual void HandleMovement ()
		{
			move = VRCameraRig.instance.eyesTrs.rotation * InputManager.MoveInput;
			if (move.sqrMagnitude == 0)
				return;
			Move (move);
		}
		
		public virtual void Move (Vector3 move)
		{
			RaycastHit hit;
			float totalMoveAmount = move.magnitude * moveSpeed * Time.deltaTime;
			Vector3 headPosition = HeadPosition;
			bool didMove = false;
			if (Physics.Raycast(headPosition, move, out hit, totalMoveAmount + SnakePiece.RADIUS + Physics.defaultContactOffset, whatICrashInto, QueryTriggerInteraction.Ignore))
				totalMoveAmount = hit.distance - SnakePiece.RADIUS - Physics.defaultContactOffset;
			while (totalMoveAmount > 0)
			{
				float moveAmount = Mathf.Min(maxDistanceBetweenPieces, totalMoveAmount);
				Vector3 position;
				if (Physics.Raycast(headPosition, move, out hit, moveAmount + SnakePiece.RADIUS + Physics.defaultContactOffset, whatICrashInto, QueryTriggerInteraction.Ignore))
				{
					// if (hit.distance <= SnakePiece.RADIUS + Physics.defaultContactOffset)
					// {
					// 	if (!didMove)
					// 		this.move = Vector3.zero;
					// 	return;
					// }
					position = hit.point + (headPosition - hit.point).normalized * (SnakePiece.RADIUS + Physics.defaultContactOffset);
					moveAmount = hit.distance - SnakePiece.RADIUS - Physics.defaultContactOffset;
				}
				else
					position = headPosition + (move.normalized * moveAmount);
				moveAmount = Mathf.Abs(moveAmount);
				AddHeadPiece (position, moveAmount);
				headPosition = position;
				totalMoveAmount -= moveAmount;
				didMove = true;
			}
			if (!didMove)
				this.move = Vector3.zero;
		}

		public void AddHeadPiece (Vector3 position)
		{
			AddHeadPiece (position, (HeadPosition - position).magnitude);
		}

		public virtual void AddHeadPiece (Vector3 position, float distanceToPreviousPiece)
		{
			SnakePiece headPiece = ObjectPool.Instance.SpawnComponent<SnakePiece>(piecePrefab.prefabIndex, position, parent:trs);
			headPiece.distanceToPreviousPiece = distanceToPreviousPiece;
			headPiece.snake = this;
			pieces.Add(headPiece);
			currentLength += distanceToPreviousPiece;
			headMovementAudioSource.UnPause();
			if (onAddHeadPiece != null)
				onAddHeadPiece (position);
		}

		public void AddTailPiece (Vector3 position)
		{
			AddTailPiece (position, (TailPosition - position).magnitude);
		}

		public virtual void AddTailPiece (Vector3 position, float distanceToPreviousPiece)
		{
			SnakePiece tailPiece = ObjectPool.Instance.SpawnComponent<SnakePiece>(piecePrefab.prefabIndex, position, parent:trs);
			tailPiece.distanceToPreviousPiece = distanceToPreviousPiece;
			tailPiece.snake = this;
			pieces.Insert(0, tailPiece);
			currentLength += distanceToPreviousPiece;
			tailMovementAudioSource.UnPause();
			if (onAddTailPiece != null)
				onAddTailPiece (position);
		}

		public virtual void RemoveTailPiece ()
		{
			SnakePiece tailPiece = TailPiece;
			ObjectPool.instance.Despawn (tailPiece.prefabIndex, tailPiece.gameObject, tailPiece.trs);
			pieces.RemoveAt(0);
			currentLength -= tailPiece.distanceToPreviousPiece;
			if (onRemoveTailPiece != null)
				onRemoveTailPiece ();
		}

		public void SetLength (float newLength)
		{
			float lengthChange = newLength - length.value;
			if (lengthChange > 0)
			{
				Vector3 tailPosition = TailPosition;
				Vector3 move = (tailPosition - pieces[1].trs.position).normalized;
				float totalMoveAmount = newLength - currentLength;
				while (totalMoveAmount > 0)
				{
					float moveAmount = Mathf.Min(maxDistanceBetweenPieces, totalMoveAmount);
					if (Physics.Raycast(tailPosition, move, moveAmount + SnakePiece.RADIUS + Physics.defaultContactOffset, whatICrashIntoExcludingSnakes, QueryTriggerInteraction.Ignore))
					{
						for (int i = 0; i < pieces.Count; i ++)
						{
							SnakePiece piece = pieces[i];
							if (Physics.Raycast(piece.trs.position, -move, moveAmount + SnakePiece.RADIUS + Physics.defaultContactOffset, whatICrashIntoExcludingSnakes, QueryTriggerInteraction.Ignore))
							{
								OnSetLength ();
								return;
							}
						}
					}
					moveAmount = Mathf.Abs(moveAmount);
					Vector3 position = tailPosition + (move * moveAmount);
					AddTailPiece (position, moveAmount);
					tailPosition = position;
					totalMoveAmount -= moveAmount;
				}
			}
			while (currentLength > newLength)
				RemoveTailPiece ();
			length.SetValue (newLength);
			OnSetLength ();
		}

		public virtual void OnSetLength ()
		{
			SetSkinParts (skin);
			lengthIndicator.SetPosition(0, Vector3.up * currentLength / length.valueRange.max);
		}

		public void SetSkin (Skin skin)
		{
			this.skin = skin;
			leftEyeTrs.localPosition = skin.leftEyeTrs.localPosition;
			leftEyeTrs.localRotation = skin.leftEyeTrs.localRotation;
			leftEyeTrs.localScale = skin.leftEyeTrs.localScale;
			rightEyeTrs.localPosition = skin.rightEyeTrs.localPosition;
			rightEyeTrs.localRotation = skin.rightEyeTrs.localRotation;
			rightEyeTrs.localScale = skin.rightEyeTrs.localScale;
			if (skin.animStateOverrides.Length > 0)
			{
				AnimatorOverrideController animatorOverrideController = new AnimatorOverrideController(animator.runtimeAnimatorController);
				List<KeyValuePair<AnimationClip, AnimationClip>> animOverrides = new List<KeyValuePair<AnimationClip, AnimationClip>>(animatorOverrideController.overridesCount);
				animatorOverrideController.GetOverrides(animOverrides);
				for (int i = 0; i < skin.animStateOverrides.Length; i ++)
				{
					Skin.AnimationStateOverride animStateOverride = skin.animStateOverrides[i];
					for (int i2 = 0; i2 < animOverrides.Count; i2 ++)
					{
						KeyValuePair<AnimationClip, AnimationClip> animOverride = animOverrides[i2];
						if (animOverride.Key.name == animStateOverride.name)
						{
							animOverrides[i2] = new KeyValuePair<AnimationClip, AnimationClip>(animOverride.Key, animStateOverride.animClip);
							break;
						}
					}
				}
				animatorOverrideController.ApplyOverrides(animOverrides);
				animator.runtimeAnimatorController = animatorOverrideController;
			}
			else
				animator.runtimeAnimatorController = defaultRuntimeAnimatorController;
			SetSkinParts (skin);
		}

		public virtual void SetSkinParts (Skin skin)
		{
			float distance = 0;
			for (int i = 1; i < pieces.Count - 1; i ++)
			{
				SnakePiece piece = pieces[i];
				distance += piece.distanceToPreviousPiece;
				piece.distanceToTailPiece = distance;
				for (int i2 = 0; i2 < skin.parts.Length; i2 ++)
				{
					Skin.Part part = skin.parts[i2];
					if (part.percentRange.Contains(piece.distanceToTailPiece / currentLength * 100))
					{
						part.data.Apply (piece);
						break;
					}
				}
			}
			SnakePiece tailPiece = TailPiece;
			tailPiece.distanceToTailPiece = 0;
			skin.tailData.Apply (tailPiece);
			SnakePiece headPiece = HeadPiece;
			headPiece.distanceToTailPiece = currentLength;
			skin.headData.Apply (headPiece);
		}

		public virtual void OnDisable ()
		{
			GameManager.updatables = GameManager.updatables.Remove(this);
			if (!isDead && GameManager.ModifierIsActive("Mastery mode"))
			{
				Level.instance.TriesRemainingInMasteryMode --;
				if (Level.instance.TriesRemainingInMasteryMode == 0)
				{
					for (int i = 0; i < LevelSelect.instance.levels.Length; i ++)
					{
						Level level = LevelSelect.instance.levels[i];
						LevelButton levelButton = LevelSelect.instance.levelButtons[i];
						if (SaveAndLoadManager.saveData.masteryScore >= levelButton.unlockAtScore && level.TriesRemainingInMasteryMode > 0)
							return;
					}
					GameplayMenu.instance.ResetMasteryData ();
					return;
				}
			}
		}

		void OnDestroy ()
		{
			onAddHeadPiece = null;
			onAddTailPiece = null;
			onRemoveTailPiece = null;
			onDeath = null;
		}

		public Vector3 GetPiecePosition (int index)
		{
			return pieces[index].trs.position;
		}

		public Vector3 GetPieceLocalPosition (int index)
		{
			return pieces[index].trs.localPosition;
		}

		void OnCollisionEnter (Collision coll)
		{
			ContactPoint[] contactPoints = new ContactPoint[coll.contactCount];
			coll.GetContacts(contactPoints);
			for (int i = 0; i < contactPoints.Length; i ++)
			{
				ContactPoint contactPoint = contactPoints[i];
				bool shouldMakeSound = true;
				for (int i2 = 0; i2 < collisionPoints.Count; i2 ++)
				{
					CollisionPoint collisionPoint = collisionPoints[i2];
					if (collisionPoint.go.layer == coll.gameObject.layer && Time.time - collisionPoint.time < minCollisionSoundEffectTimeSeparation && ((Vector3.Angle(collisionPoint.trs.TransformDirection(collisionPoint.normal), contactPoint.normal) < minAngleBetweenCollisionNormalsToMakeSound) ||
						(collisionPoint.point - contactPoint.point).sqrMagnitude < minCollisionSoundEffectDistanceSeparationSqr))
					{
						shouldMakeSound = false;
						break;
					}
				}
				if (shouldMakeSound)
				{
					MakeCollisionSoundEffect (coll.gameObject, contactPoint.point);
					Transform collidedTrs = coll.transform;
					collisionPoints.Add(new CollisionPoint(contactPoint.point, collidedTrs.InverseTransformDirection(contactPoint.normal), coll.gameObject, collidedTrs, Time.time));
				}
			}
			OnCollisionStay (coll);
			StartCoroutine(CollisionEnterRoutine (coll.collider));
		}

		IEnumerator CollisionEnterRoutine (Collider collider)
		{
			yield return new WaitForEndOfFrame();
			if (!contactPointsWithCollidersDict.ContainsKey(collider))
				StartCoroutine(CollisionExitRoutine (collider));
		}

		public virtual void OnCollisionStay (Collision coll)
		{
			ContactPoint[] contactPoints = new ContactPoint[coll.contactCount];
			coll.GetContacts(contactPoints);
			contactPointsWithCollidersDict[coll.collider] = contactPoints;
		}

		public virtual void OnCollisionExit (Collision coll)
		{
			contactPointsWithCollidersDict.Remove(coll.collider);
			StartCoroutine(CollisionExitRoutine (coll.collider));
		}

		IEnumerator CollisionExitRoutine (Collider collider)
		{
			yield return new WaitForFixedUpdate();
			if (contactPointsWithCollidersDict.ContainsKey(collider))
				yield break;
			for (int i = 0; i < collisionPoints.Count; i ++)
			{
				CollisionPoint collisionPoint = collisionPoints[i];
				if (collisionPoint.go == collider.gameObject)
				{
					collisionPoints.RemoveAt(i);
					i --;
				}
			}
		}

		SoundEffect MakeCollisionSoundEffect (GameObject hitGo, Vector3 hitPoint)
		{
			return MakeCollisionSoundEffect(hitGo, hitPoint, rigid.GetPointVelocity(hitPoint).magnitude);
		}

		SoundEffect MakeCollisionSoundEffect (GameObject hitGo, Vector3 hitPoint, float hitSpeed)
		{
			if (hitGo.layer == LayerMask.NameToLayer("Wall"))
				return MakeCollisionSoundEffect(CollisionSoundEffectEntry.Type.Wall, hitPoint, hitSpeed);
			else if (hitGo.layer == LayerMask.NameToLayer("End"))
				return MakeCollisionSoundEffect(CollisionSoundEffectEntry.Type.End, hitPoint, hitSpeed);
			else if (hitGo.layer == LayerMask.NameToLayer("Hazard") || hitGo.layer == LayerMask.NameToLayer("Bullet"))
				return MakeCollisionSoundEffect(CollisionSoundEffectEntry.Type.Hazard, hitPoint, hitSpeed);
			else if (hitGo.layer == LayerMask.NameToLayer("Bouncy Tile"))
				return MakeCollisionSoundEffect(CollisionSoundEffectEntry.Type.BouncyTile, hitPoint, hitSpeed);
			else if (hitGo.layer == LayerMask.NameToLayer("Sticky Tile"))
				return MakeCollisionSoundEffect(CollisionSoundEffectEntry.Type.StickyTile, hitPoint, hitSpeed);
			else
				return null;
		}

		SoundEffect MakeCollisionSoundEffect (CollisionSoundEffectEntry.Type collisionSoundEffectEntryType, Vector3 hitPoint)
		{
			return MakeCollisionSoundEffect(collisionSoundEffectEntryType, hitPoint, rigid.GetPointVelocity(hitPoint).magnitude);
		}

		SoundEffect MakeCollisionSoundEffect (CollisionSoundEffectEntry.Type collisionSoundEffectEntryType, Vector3 hitPoint, float hitSpeed)
		{
			return collisionSoundEffectEntriesDict[collisionSoundEffectEntryType].Make(hitPoint, hitSpeed);
		}

		public float GetDistanceSqrToClosestPiece (Vector3 position)
		{
			float closestDistanceSqr = Mathf.Infinity;
			for (int i = 0; i < pieces.Count; i ++)
			{
				Vector3 piecePosition = GetPiecePosition(i);
				float distanceSqr = (piecePosition - position).sqrMagnitude;
				if (distanceSqr < closestDistanceSqr)
					closestDistanceSqr = distanceSqr;
			}
			return closestDistanceSqr;
		}

		public SnakePiece GetClosestPiece (Vector3 position)
		{
			SnakePiece closestSnakePiece = pieces[0];
			float closestDistanceSqr = (closestSnakePiece.trs.position - position).sqrMagnitude;
			for (int i = 1; i < pieces.Count; i ++)
			{
				SnakePiece snakePiece = pieces[i];
				float distanceSqr = (snakePiece.trs.position - position).sqrMagnitude;
				if (distanceSqr < closestDistanceSqr)
				{
					closestDistanceSqr = distanceSqr;
					closestSnakePiece = snakePiece;
				}
			}
			return closestSnakePiece;
		}

		public struct CollisionPoint
		{
			public Vector3 point;
			public Vector3 normal;
			public GameObject go;
			public Transform trs;
			public float time;

			public CollisionPoint (Vector3 point, Vector3 normal, GameObject go, Transform trs, float time)
			{
				this.point = point;
				this.normal = normal;
				this.go = go;
				this.trs = trs;
				this.time = time;
			}
		}

		[Serializable]
		public struct CollisionSoundEffectEntry
		{
			public Type type;
			public AudioClip[] audioClips;
			public AnimationCurve hitSpeedToVolumeCurve;

			public SoundEffect Make (Vector3 hitPoint)
			{
				return Make(hitPoint, Snake.instance.rigid.GetPointVelocity(hitPoint).magnitude);
			}

			public SoundEffect Make (Vector3 hitPoint, float hitSpeed)
			{
				return AudioManager.instance.MakeSoundEffect(audioClips[Random.Range(0, audioClips.Length)], hitPoint, hitSpeedToVolumeCurve.Evaluate(hitSpeed));
			}

			public enum Type
			{
				Wall,
				End,
				Hazard,
				BouncyTile,
				StickyTile
			}
		}

		[Serializable]
		public struct Skin
		{
			public Part.Data headData;
			public Part.Data tailData;
			public Transform leftEyeTrs;
			public Transform rightEyeTrs;
			public AnimationStateOverride[] animStateOverrides;
			public Part[] parts;

			[Serializable]
			public struct AnimationStateOverride
			{
				public string name;
				public AnimationClip animClip;
			}

			[Serializable]
			public struct Part
			{
				public FloatRange percentRange;
				public Data data;

				[Serializable]
				public struct Data
				{
					public Material[] materials;
					public Mesh mesh;

					public void Apply (SnakePiece snakePiece)
					{
						snakePiece.meshFilter.sharedMesh = mesh;
						snakePiece.DestroyMaterials ();
						Material[] newMaterials = new Material[materials.Length];
						for (int i = 0; i < materials.Length; i ++)
						{
							Material material = materials[i];
							Material newMaterial = new Material(material);
							if (newMaterial.HasProperty("fractionToHead"))
								newMaterial.SetFloat("fractionToHead", snakePiece.distanceToTailPiece / snakePiece.snake.currentLength);
							if (newMaterial.HasProperty("pieceIndex"))
								newMaterial.SetFloat("pieceIndex", snakePiece.trs.GetSiblingIndex());
							newMaterials[i] = newMaterial;
						}
						snakePiece.meshRenderer.sharedMaterials = newMaterials;
					}
				}
			}
		}
	}
}