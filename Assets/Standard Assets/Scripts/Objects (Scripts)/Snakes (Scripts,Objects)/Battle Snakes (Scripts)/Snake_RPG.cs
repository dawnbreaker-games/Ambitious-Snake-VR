using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace AmbitiousSnake
{
	public class Snake_RPG : BattleSnake
	{
		public override void InitPieces ()
		{
			if (SaveAndLoadManager.saveData.startPosition != trs.position)
				trs.position = SaveAndLoadManager.saveData.startPosition.ToVec3();
			if (SaveAndLoadManager.saveData.startEulerAngles != trs.eulerAngles)
				trs.eulerAngles = SaveAndLoadManager.saveData.startEulerAngles.ToVec3();
			if (SaveAndLoadManager.saveData.savedPieceEntries.Length == 0)
				base.InitPieces ();
			else
			{
				AddHeadPiece (SaveAndLoadManager.saveData.savedPieceEntries[0].position, 0);
				for (int i = 0; i < SaveAndLoadManager.saveData.savedPieceEntries.Length; i ++)
				{
					PieceEntry pieceEntry = SaveAndLoadManager.saveData.savedPieceEntries[i];
					AddTailPiece (pieceEntry.position, pieceEntry.distanceToPreviousPiece);
				}
			}
		}

		[Serializable]
		public struct PieceEntry
		{
			public Vector3 position;
			public float distanceToPreviousPiece;

			public PieceEntry (Vector3 position, float distanceToPreviousPiece)
			{
				this.position = position;
				this.distanceToPreviousPiece = distanceToPreviousPiece;
			}
		}
	}
}