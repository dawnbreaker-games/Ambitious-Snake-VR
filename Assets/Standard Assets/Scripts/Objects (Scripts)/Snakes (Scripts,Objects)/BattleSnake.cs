using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace AmbitiousSnake
{
	public class BattleSnake : Snake
	{
		public SnakeBreath breath;
		public Vector3 breathPositionOffset;

		public override void DoUpdate ()
		{
			base.DoUpdate ();
			breath.particleSystemWithTrigger.trigger.SetCollider(0, HeadPiece.collider);
			breath.trs.forward = HeadPiece.trs.forward;
			breath.trs.position = HeadPosition + breath.trs.rotation * breathPositionOffset;
		}
	}
}