using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

namespace AmbitiousSnake
{
	public class End : Tile, ICollisionEnterHandler
	{
		public Collider collider;
		public Collider Collider
		{
			get
			{
				return collider;
			}
		}
		public static End instance;

		public override void OnEnable ()
		{
			instance = this;
		}
		
		public void OnCollisionEnter (Collision coll)
		{
			if (coll.gameObject.layer == LayerMask.NameToLayer("Snake"))
			{
#if PROBUILDER
				TestSnake testSnake = Snake.instance as TestSnake;
				if (testSnake != null)
				{
					testSnake.OnHitEnd ();
					return;
				}
#endif
				Perk lowestCostingPerk = PerksMenu.instance.perks[0];
				bool previouslyHadEnoughForPerk = SaveAndLoadManager.saveData.score >= lowestCostingPerk.scoreRequired;
				if (Star.instance.isCollected)
				{
					if (!Level.instance.CollectedStar)
					{
						Level.instance.CollectedStar = true;
						SaveAndLoadManager.saveData.score ++;
					}
					if (GameManager.ModifierIsActive("Mastery mode") && !Level.instance.CollectedStarInMasteryMode)
					{
						Level.instance.CollectedStarInMasteryMode = true;
						SaveAndLoadManager.saveData.masteryScore ++;
					}
				}
				if (!Level.instance.HasWon)
				{
					Level.instance.HasWon = true;
					SaveAndLoadManager.saveData.score ++;
				}
				if (GameManager.ModifierIsActive("Mastery mode") && !Level.instance.HasWonInMasteryMode)
				{
					Level.instance.HasWonInMasteryMode = true;
					SaveAndLoadManager.saveData.masteryScore ++;
				}
				float timeSinceLevelLoad = GameManager.UnpausedTimeSinceLevelLoad;
				if (Level.instance.FastestTime > timeSinceLevelLoad)
				{
					if (!Level.instance.GotParTime && timeSinceLevelLoad <= Level.instance.parTime)
						SaveAndLoadManager.saveData.score ++;
					Level.instance.FastestTime = timeSinceLevelLoad;
				}
				if (GameManager.ModifierIsActive("Mastery mode") && Level.instance.FastestTimeInMasteryMode > timeSinceLevelLoad)
				{
					if (!Level.instance.GotParTimeInMasteryMode && timeSinceLevelLoad <= Level.instance.parTime)
						SaveAndLoadManager.saveData.masteryScore ++;
					Level.instance.FastestTimeInMasteryMode = timeSinceLevelLoad;
				}
				if (SaveAndLoadManager.saveData.masteryScore > SaveAndLoadManager.saveData.masteryHighscore)
					SaveAndLoadManager.saveData.masteryHighscore = SaveAndLoadManager.saveData.masteryScore;
				if (!previouslyHadEnoughForPerk && SaveAndLoadManager.saveData.score >= lowestCostingPerk.scoreRequired)
					SceneManager.sceneLoaded += OnSceneLoadedAndCanBuyPerk;
				if (_SceneManager.CurrentScene.buildIndex < LevelSelect.instance.levels.Length + LevelSelect.LEVEL_1_SCENE_INDEX - 1 && 
						SaveAndLoadManager.saveData.score >= LevelSelect.instance.levelButtons[_SceneManager.CurrentScene.buildIndex - LevelSelect.LEVEL_1_SCENE_INDEX + 1].unlockAtScore)
					_SceneManager.instance.NextSceneWithoutTransition ();
				else
				{
					SceneManager.sceneLoaded += OnSameSceneLoaded;
					_SceneManager.instance.RestartSceneWithoutTransition ();
				}
			}
		}

		void OnSceneLoadedAndCanBuyPerk (Scene scene = new Scene(), LoadSceneMode loadSceneMode = LoadSceneMode.Single)
		{
			SceneManager.sceneLoaded -= OnSceneLoadedAndCanBuyPerk;
			GameManager.instance.StartCoroutine(StartPerksTutorialRoutine ());
		}

		IEnumerator StartPerksTutorialRoutine ()
		{
			yield return new WaitForEndOfFrame();
			if (Tutorial.instance != null)
				Tutorial.instance.playableDirector.Pause();
			PerksTutorial.instance.gameObject.SetActive(true);
		}

		void OnSameSceneLoaded (Scene scene = new Scene(), LoadSceneMode loadSceneMode = LoadSceneMode.Single)
		{
			SceneManager.sceneLoaded -= OnSameSceneLoaded;
			GameManager.instance.StartCoroutine(OpenLevelSelectRoutine ());
		}

		IEnumerator OpenLevelSelectRoutine ()
		{
			yield return new WaitForEndOfFrame();
			GameplayMenu.instance.gameObject.SetActive(true);
			GameplayMenu.instance.Disable ();
			LevelSelect.instance.gameObject.SetActive(true);
			if (_SceneManager.CurrentScene.buildIndex < LevelSelect.instance.levels.Length - 1)
			{
				LevelButton levelButton = LevelSelect.instance.levelButtons[_SceneManager.CurrentScene.buildIndex + 1];
				levelButton.indicatorGo.SetActive(false);
				levelButton.button.gameObject.SetActive(true);
			}
		}
	}
}