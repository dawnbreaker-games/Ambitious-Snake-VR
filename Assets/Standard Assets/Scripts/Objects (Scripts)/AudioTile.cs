using UnityEngine;
using Extensions;
using System.Collections;
using System.Collections.Generic;

namespace AmbitiousSnake
{
	[ExecuteInEditMode]
	public class AudioTile : Tile, ICollisionEnterHandler
	{
		public Collider collider;
		public Collider Collider
		{
			get
			{
				return collider;
			}
		}
		public AudioSource audioSource;

		public void OnCollisionEnter (Collision coll)
		{
            if (!audioSource.isPlaying)
		    	audioSource.Play();
		}
	}
}