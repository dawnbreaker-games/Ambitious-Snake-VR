using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace AmbitiousSnake
{
	public class BouncyTile : Tile, ICollisionEnterHandler
	{
		public Collider collider;
		public Collider Collider
		{
			get
			{
				return collider;
			}
		}
		public float forceAmount;
		public float bounceCooldown;
		public AnimationCurve positionCurve;
		List<Rigidbody> bouncingRigids = new List<Rigidbody>();

		public void OnCollisionEnter (Collision coll)
		{
			Rigidbody rigid = coll.gameObject.GetComponentInParent<Rigidbody>();
			if (rigid == null || bouncingRigids.Contains(rigid))
				return;
			bouncingRigids.Add(rigid);
			Vector3 forceDirection = new Vector3();
			for (int i = 0; i < coll.contactCount; i ++)
			{
				ContactPoint contactPoint = coll.GetContact(i);
				forceDirection -= contactPoint.normal;
			}
			StartCoroutine(BounceRoutine (rigid, forceDirection));
		}

		IEnumerator BounceRoutine (Rigidbody rigid, Vector3 forceDirection)
		{
			Vector3 position = trs.localPosition;
			rigid.AddForce(forceDirection.normalized * forceAmount, ForceMode.Impulse);
			float time = Time.time;
			while (true)
			{
				trs.localPosition = position + forceDirection.normalized * positionCurve.Evaluate((Time.time - time) / bounceCooldown);
				if (Time.time - time >= bounceCooldown)
					break;
				yield return new WaitForEndOfFrame();
			}
			bouncingRigids.Remove(rigid);
		}

		void OnCollisionStay (Collision coll)
		{
			OnCollisionEnter (coll);
		}
	}
}