using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

namespace AmbitiousSnake
{
	public class Checkpoint : Tile, ICollisionEnterHandler
	{
		public Collider collider;
		public Collider Collider
		{
			get
			{
				return collider;
			}
		}
		public static Checkpoint lastTouched;
		
		public void OnCollisionEnter (Collision coll)
		{
			if (coll.gameObject.layer == LayerMask.NameToLayer("Snake"))
			{
				lastTouched = this;
				Snake_RPG snake_RPG = (Snake_RPG) Snake.instance;
				List<Snake_RPG.PieceEntry> snakePieceEntires = new List<Snake_RPG.PieceEntry>();
				for (int i = 0; i < snake_RPG.pieces.Count; i ++)
				{
					SnakePiece snakePiece = snake_RPG.pieces[i];
					snakePieceEntires.Add(new Snake_RPG.PieceEntry(snakePiece.trs.position, snakePiece.distanceToPreviousPiece));
				}
				SaveAndLoadManager.saveData.savedPieceEntries = snakePieceEntires.ToArray();
				List<string> collectedStars = new List<string>(SaveAndLoadManager.saveData.collectedStars);
				for (int i = 0; i < Star_RPG.instances.Count; i ++)
				{
					Star_RPG star_RPG = Star_RPG.instances[i];
					if (star_RPG.isCollected && !collectedStars.Contains(star_RPG.name))
						collectedStars.Add(star_RPG.name);
				}
				SaveAndLoadManager.saveData.collectedStars = collectedStars.ToArray();
			}
		}
	}
}