﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace AmbitiousSnake
{
	public class Bomb : Bullet
	{
		public bool explodeOnContact;
		public Explosion explosionPrefab;
		public Timer explodeDelayTimer;

		void Awake ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			explodeDelayTimer.onFinished += Explode;
		}

		void OnDestroy ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			explodeDelayTimer.onFinished -= Explode;
		}

		public override void OnCollisionEnter (Collision coll)
		{
			if (!dead && explodeOnContact)
			{
				dead = true;
				explodeDelayTimer.Reset ();
				explodeDelayTimer.Start ();
			}
			rigid.velocity = trs.forward * moveSpeed;
		}
		
		void OnCollisionStay (Collision coll)
		{
			rigid.velocity = trs.forward * moveSpeed;
		}
		
		void OnCollisionExit (Collision coll)
		{
			rigid.velocity = trs.forward * moveSpeed;
		}

		void Explode (params object[] args)
		{
			dead = true;
			ObjectPool.Instance.SpawnComponent<Explosion>(explosionPrefab.prefabIndex, trs.position);
			// ObjectPool.Instance.Despawn (prefabIndex, gameObject, trs);
			Destroy(gameObject);
		}

		public override void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			rigid.velocity = trs.forward * moveSpeed;
		}

		public override void OnDisable ()
		{
			base.OnDisable ();
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			collider.enabled = false;
			dead = false;
		}

		public void DestroyMe ()
		{
			Destroy(gameObject);
		}
	}
}