using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;

namespace AmbitiousSnake
{
	public class Lava : Hazard
	{
		public AudioClip[] onTriggerEnterAudioClips = new AudioClip[0];
		public AnimationCurve onTriggerEnterSpeedToVolumeCurve;
		public float addToHitRigidbodyDrag;
		public float addToHitRigidbodyAngularDrag;
		public _ParticleSystem burnParticlesPrefab;
		public float snakePieceBurnRate;
		public float snakePieceBurnDelay;
		public int snakePieceFireSpreadAmount = 1;
		public float snakePieceBurnDuration;
		public float cameraZoomOutRate;
		public Color burnedSnakePieceColor;
		List<Rigidbody> hitRigidbodies = new List<Rigidbody>();
		Dictionary<int, SnakePiece> burnedSnakePieces = new Dictionary<int, SnakePiece>();

		void OnTriggerEnter (Collider other)
		{
			Rigidbody rigid = other.GetComponentInParent<Rigidbody>();
			if (hitRigidbodies.Contains(rigid))
				return;
			hitRigidbodies.Add(rigid);
			float volume = onTriggerEnterSpeedToVolumeCurve.Evaluate(rigid.velocity.magnitude);
			AudioManager.instance.MakeSoundEffect (onTriggerEnterAudioClips[Random.Range(0, onTriggerEnterAudioClips.Length)], other.GetComponent<Transform>().position, volume);
			rigid.drag += addToHitRigidbodyDrag;
			rigid.angularDrag += addToHitRigidbodyAngularDrag;
			IDestructable destructable = other.GetComponentInParent<IDestructable>();
			if (destructable != null)
				ApplyDamage (destructable, damage);
		}
		
		public override void ApplyDamage (IDestructable destructable, float amount)
		{
			Snake snake = destructable as Snake;
			if (snake != null)
			{
				Snake.instance.enabled = false;
				VRCameraRig.instance.enabled = false;
				StartCoroutine(BurnSnakeRoutine ());
				StartCoroutine(ZoomCameraRoutine ());
			}
			else
				destructable.TakeDamage (amount, this);
		}

		IEnumerator BurnSnakeRoutine ()
		{
			Snake.instance.animator.Play("Death");
			for (int i = 0; i < Snake.instance.pieces.Count; i ++)
			{
				SnakePiece snakePiece = Snake.instance.pieces[i];
				if (snakePiece.trs.position.y <= trs.GetBounds().max.y + SnakePiece.RADIUS + Physics.defaultContactOffset && !burnedSnakePieces.ContainsKey(i))
					StartBurningSnakePiece (snakePiece, i);
			}
			yield return new WaitUntil(() => (burnedSnakePieces.Count == Snake.instance.pieces.Count));
			yield return new WaitForSeconds(snakePieceBurnDuration);
			VRCameraRig.instance.enabled = true;
			Snake.instance.TakeDamage (damage, this);
		}

		void StartBurningSnakePiece (SnakePiece snakePiece, int snakePieceIndex)
		{
			if (snakePiece.meshRenderer.sharedMaterial.HasProperty("fractionToHead"))
			{
				snakePiece.meshRenderer.material.SetColor("tailColor", Color.Lerp(snakePiece.meshRenderer.material.GetColor("tailColor"), snakePiece.meshRenderer.material.GetColor("headColor"), snakePiece.meshRenderer.material.GetFloat("fractionToHead")));
				snakePiece.meshRenderer.material.SetColor("headColor", burnedSnakePieceColor);
				snakePiece.meshRenderer.material.SetFloat("fractionToHead", 0);
			}
			if (burnParticlesPrefab != null)
				ObjectPool.instance.SpawnComponent<_ParticleSystem>(burnParticlesPrefab.prefabIndex, snakePiece.trs.position, burnParticlesPrefab.trs.rotation, snakePiece.trs);
			burnedSnakePieces.Add(snakePieceIndex, snakePiece);
			StartCoroutine(BurnSnakePieceRoutine (snakePiece, snakePieceIndex));
		}

		void StartBurningNextSnakePieces (SnakePiece snakePiece, int snakePieceIndex)
		{
			for (int i = snakePieceIndex - 1; i >= snakePieceIndex - snakePieceFireSpreadAmount; i --)
			{
				if (i >= 0 && !burnedSnakePieces.ContainsKey(i))
				{
					SnakePiece nextSnakePiece = Snake.instance.pieces[i];
					StartBurningSnakePiece (nextSnakePiece, i);
				}
			}
			for (int i = snakePieceIndex + 1; i <= snakePieceIndex + snakePieceFireSpreadAmount; i ++)
			{
				if (Snake.instance.pieces.Count > i && !burnedSnakePieces.ContainsKey(i))
				{
					SnakePiece nextSnakePiece = Snake.instance.pieces[i];
					StartBurningSnakePiece (nextSnakePiece, i);
				}
			}
		}

		IEnumerator BurnSnakePieceRoutine (SnakePiece snakePiece, int snakePieceIndex)
		{
			float snakePieceColorLerpValue = 0;
			float timeTillBurnNextSnakePiece = Time.time;
			while (snakePieceColorLerpValue < 1)
			{
				snakePieceColorLerpValue += snakePieceBurnRate * Time.deltaTime;
				if (snakePiece.meshRenderer.sharedMaterial.HasProperty("fractionToHead"))
					snakePiece.meshRenderer.material.SetFloat("fractionToHead", snakePieceColorLerpValue);
				else
					snakePiece.meshRenderer.material.color = Color.Lerp(snakePiece.meshRenderer.sharedMaterial.color, burnedSnakePieceColor, snakePieceColorLerpValue);
				if (Time.time - timeTillBurnNextSnakePiece > snakePieceBurnDelay)
				{
					timeTillBurnNextSnakePiece = Mathf.Infinity;
					StartBurningNextSnakePieces (snakePiece, snakePieceIndex);
				}
				yield return new WaitForEndOfFrame();
			}
			StartBurningNextSnakePieces (snakePiece, snakePieceIndex);
		}

		IEnumerator ZoomCameraRoutine ()
		{
			while (true)
			{
				VRCameraRig.instance.cameraDistance += cameraZoomOutRate * Time.deltaTime;
				RaycastHit hit;
				if (Physics.Raycast(Snake.instance.HeadPosition, VRCameraRig.instance.trs.position - Snake.instance.HeadPosition, out hit, VRCameraRig.instance.cameraDistance, VRCameraRig.instance.whatICollideWith))
					VRCameraRig.instance.positionOffset = VRCameraRig.instance.positionOffset.normalized * hit.distance;
				else
					VRCameraRig.instance.positionOffset = VRCameraRig.instance.positionOffset.normalized * VRCameraRig.instance.cameraDistance;
				VRCameraRig.instance.trs.position = Snake.instance.HeadPosition + (VRCameraRig.instance.eyesTrs.rotation * VRCameraRig.instance.positionOffset);
				yield return new WaitForEndOfFrame();
			}
		}
	}
}