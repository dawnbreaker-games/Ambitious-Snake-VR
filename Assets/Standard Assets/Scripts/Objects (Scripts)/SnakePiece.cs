using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace AmbitiousSnake
{
	public class SnakePiece : Spawnable
	{
        public float distanceToPreviousPiece;
        public float distanceToTailPiece;
		public MeshRenderer meshRenderer;
		public MeshFilter meshFilter;
		public Collider collider;
		public Snake snake;
		public const float RADIUS = 0.6f;

		void OnDisable ()
		{
			DestroyMaterials ();
		}

		public void DestroyMaterials ()
		{
			for (int i = 0; i < meshRenderer.sharedMaterials.Length; i ++)
			{
				Material material = meshRenderer.sharedMaterials[i];
				// DestroyImmediate(material);
				Destroy(material);
			}
		}
	}
}