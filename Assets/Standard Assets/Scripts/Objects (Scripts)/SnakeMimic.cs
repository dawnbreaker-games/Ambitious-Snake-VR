using System.Collections;
using UnityEngine;
using Extensions;
using System.Collections.Generic;
using System;

namespace AmbitiousSnake
{
	public class SnakeMimic : Snake, ISpawnable
	{
		public int prefabIndex;
		public int PrefabIndex
		{
			get
			{
				return prefabIndex;
			}
		}
		[HideInInspector]
		public SnakeRecording playing;
		public float shrinkSphereChecks;
		public MaterialRemapEntry[] materialRemapEntries = new MaterialRemapEntry[0];
		static Dictionary<string, Material> materialRemapDict = new Dictionary<string, Material>();
		SnakeRecording.Frame currentFrame;
		int currentFrameIndex;
		bool isPlaying;
		float playbackTime;
		
		public override void Awake ()
		{
			if (materialRemapDict.Count == 0)
			{
				for (int i = 0; i < materialRemapEntries.Length; i ++)
				{
					MaterialRemapEntry materialRemapEntry = materialRemapEntries[i];
					materialRemapDict.Add(materialRemapEntry.oldMaterialName, materialRemapEntry.newMaterial);
				}
			}
		}

		public override void OnEnable ()
		{
			isPlaying = true;
			currentFrameIndex = 0;
			playbackTime = 0;
			StartCoroutine(Init ());
		}
		
		public override void DoUpdate ()
		{
			if (GameManager.paused)
				return;
			currentFrame = playing.frames[currentFrameIndex];
			if (!isPlaying)
			{
				if (!WillCollide())
					isPlaying = true;
			}
			else
			{
				playbackTime += Time.deltaTime;
				while (playbackTime > currentFrame.timeSinceCreated)
				{
					NextFrame ();
					if (currentFrameIndex == playing.frames.Count - 1)
						return;
					// if (WillCollide())
					// {
					// 	isPlaying = false;
					// 	break;
					// }
					Translate ();
					Rotate ();
					AddHeadPieces ();
					AddTailPieces ();
					RemoveTailPieces ();
					OnSetLength ();
					if (currentFrame.newHeadPositions.Length == 0)
						headMovementAudioSource.Pause();
					else
					{
						Vector3 move = HeadPosition - GetPiecePosition(pieces.Count - 2);
						HeadPiece.trs.forward = move;
						headMovementAudioSourceTrs.position = HeadPosition;
						eyesParentTrs.position = HeadPosition;
						eyesParentTrs.forward = move;
					}
					if (currentFrame.newTailPositions.Length == 0 && currentFrame.removedTailPiecesCount == 0)
						tailMovementAudioSource.Pause();
					else
						tailMovementAudioSourceTrs.position = TailPosition;
				}
			}
		}

		public override void OnSetLength ()
		{
			SetSkinParts (Snake.instance.skin);
		}
		
		public override void TakeDamage (float amount, Hazard source)
		{
		}
		
		bool WillCollide ()
		{
			Vector3 previousPiecePosition;
			if (currentFrame.newHeadPositions.Length > 0)
			{
				previousPiecePosition = currentFrame.newHeadPositions[0];
				for (int i = 1; i < currentFrame.newHeadPositions.Length; i ++)
				{
					Vector3 piecePosition = currentFrame.newHeadPositions[i];
					Vector3 previousToCurrentPiecePosition = piecePosition - previousPiecePosition;
					float sphereCheckRadius = SnakePiece.RADIUS + Physics.defaultContactOffset - shrinkSphereChecks;
					// RaycastHit[] hits = Physics.SphereCastAll(new Ray(piecePosition, previousToCurrentPiecePosition), sphereCheckRadius, previousToCurrentPiecePosition.magnitude, whatICrashInto);
					RaycastHit[] hits = Physics.RaycastAll(new Ray(piecePosition, previousToCurrentPiecePosition), previousToCurrentPiecePosition.magnitude + SnakePiece.RADIUS + Physics.defaultContactOffset - shrinkSphereChecks, whatICrashInto);
					for (int i2 = 0; i2 < hits.Length; i2 ++)
					{
						RaycastHit hit = hits[i2];
						if (hit.rigidbody != rigid && hit.distance <= SnakePiece.RADIUS + Physics.defaultContactOffset - shrinkSphereChecks)
							return true;
					}
					previousPiecePosition = piecePosition;
				}
			}
			if (currentFrame.newTailPositions.Length > 0)
			{
				previousPiecePosition = currentFrame.newTailPositions[0];
				for (int i = 1; i < currentFrame.newTailPositions.Length - currentFrame.removedTailPiecesCount; i ++)
				{
					Vector3 piecePosition = currentFrame.newTailPositions[i];
					Vector3 previousToCurrentPiecePosition = piecePosition - previousPiecePosition;
					float sphereCheckRadius = SnakePiece.RADIUS + Physics.defaultContactOffset - shrinkSphereChecks;
					// RaycastHit[] hits = Physics.SphereCastAll(new Ray(piecePosition, previousToCurrentPiecePosition), sphereCheckRadius, previousToCurrentPiecePosition.magnitude, whatICrashInto);
					RaycastHit[] hits = Physics.RaycastAll(new Ray(piecePosition, previousToCurrentPiecePosition), previousToCurrentPiecePosition.magnitude + SnakePiece.RADIUS + Physics.defaultContactOffset - shrinkSphereChecks, whatICrashInto);
					for (int i2 = 0; i2 < hits.Length; i2 ++)
					{
						RaycastHit hit = hits[i2];
						if (hit.rigidbody != rigid && hit.distance <= SnakePiece.RADIUS + Physics.defaultContactOffset - shrinkSphereChecks)
							return true;
					}
					previousPiecePosition = piecePosition;
				}
			}
			return false;
		}
		
		IEnumerator Init ()
		{
			yield return new WaitUntil(() => (!playing.Equals(null)));
			currentFrame = playing.frames[currentFrameIndex];
			Translate ();
			Rotate ();
			while (true)
			{
				bool willCollide = false;
				for (int i = 0; i < currentFrame.newHeadPositions.Length; i ++)
				{
					Vector3 headPosition = currentFrame.newHeadPositions[i];
					if (Physics.CheckSphere(headPosition, SnakePiece.RADIUS - shrinkSphereChecks, whatICrashInto))
					{
						willCollide = true;
						break;
					}
				}
				if (!willCollide)
					break;
				yield return new WaitForEndOfFrame();
			}
			AddHeadPiece (currentFrame.newHeadPositions[0], 0);
			for (int i = 1; i < currentFrame.newHeadPositions.Length; i ++)
			{
				Vector3 newHeadPosition = currentFrame.newHeadPositions[i];
				AddHeadPiece (newHeadPosition);
			}
			SetSkin (Snake.instance.skin);
			GameManager.updatables = GameManager.updatables.Add(this);
		}
		
		void Translate ()
		{
			trs.position = currentFrame.trsPosition;
		}

		void Rotate ()
		{
			trs.eulerAngles = currentFrame.trsRotation;
		}

		void AddHeadPieces ()
		{
			if (currentFrame.newHeadPositions.Length == 0)
				return;
			for (int i = 0; i < currentFrame.newHeadPositions.Length; i ++)
			{
				Vector3 newHeadPosition = currentFrame.newHeadPositions[i];
				AddHeadPiece (newHeadPosition);
			}
			headMovementAudioSource.UnPause();
		}

		void AddTailPieces ()
		{
			if (currentFrame.newTailPositions.Length == 0)
				return;
			for (int i = 0; i < currentFrame.newTailPositions.Length; i ++)
			{
				Vector3 newTailPosition = currentFrame.newTailPositions[i];
				AddTailPiece (newTailPosition);
			}
			tailMovementAudioSource.UnPause();
		}

		void RemoveTailPieces ()
		{
			for (int i = 0; i < currentFrame.removedTailPiecesCount; i ++)
				RemoveTailPiece ();
		}
		
		void NextFrame ()
		{
			if (currentFrameIndex < playing.frames.Count - 1)
			{
				currentFrameIndex ++;
				currentFrame = playing.frames[currentFrameIndex];
			}
			else if (currentFrameIndex == playing.frames.Count - 1)
				ObjectPool.instance.Despawn (prefabIndex, gameObject, trs);
		}

		public override void OnCollisionStay (Collision coll)
		{
		}

		public override void OnCollisionExit (Collision coll)
		{
		}

		public override void OnDisable ()
		{
			GameManager.updatables = GameManager.updatables.Remove(this);
		}

		public override void SetSkinParts (Skin skin)
		{
			base.SetSkinParts (skin);
			for (int i = 0; i < pieces.Count; i ++)
			{
				SnakePiece snakePiece = pieces[i];
				snakePiece.DestroyMaterials ();
				Material[] newMaterials = new Material[snakePiece.meshRenderer.sharedMaterials.Length];
				for (int i2 = 0; i2 < snakePiece.meshRenderer.sharedMaterials.Length; i2 ++)
				{
					Material material = snakePiece.meshRenderer.sharedMaterials[i2];
					Material newMaterial = new Material(materialRemapDict[material.name]);
					if (newMaterial.HasProperty("fractionToHead"))
						newMaterial.SetFloat("fractionToHead", snakePiece.distanceToTailPiece / snakePiece.snake.currentLength);
					newMaterials[i2] = newMaterial;
				}
				snakePiece.meshRenderer.sharedMaterials = newMaterials;
			}
		}

		[Serializable]
		public struct MaterialRemapEntry
		{
			public string oldMaterialName;
			public Material newMaterial;
		}
	}
}