using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Extensions;

namespace AmbitiousSnake
{
	public class StickyTile : Tile, ICollisionEnterHandler, IUpdatable
	{
		public BoxCollider boxCollider;
		public Collider Collider
		{
			get
			{
				return boxCollider;
			}
		}
		List<StuckObject> stuckObjects = new List<StuckObject>();

		public void OnCollisionEnter (Collision coll)
		{
			Rigidbody rigid = coll.gameObject.GetComponentInParent<Rigidbody>();
			StuckObject stuckObject;
			for (int i = 0; i < stuckObjects.Count; i ++)
			{
				stuckObject = stuckObjects[i];
				if (stuckObject.rigid == rigid)
					return;
			}
			rigid.useGravity = false;
			rigid.velocity = Vector3.zero;
			rigid.angularVelocity = Vector3.zero;
			stuckObject = new StuckObject(rigid, rigid.GetComponent<Transform>());
			stuckObjects.Add(stuckObject);
			stuckObject.trs.SetParent(trs);
		}

		void OnCollisionStay (Collision coll)
		{
			OnCollisionEnter (coll);
		}

		public override void OnEnable ()
		{
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public void DoUpdate ()
		{
			for (int i = 0; i < stuckObjects.Count; i ++)
			{
				StuckObject stuckObject = stuckObjects[i];
				bool isHittingRigid = false;
				Collider[] hits = Physics.OverlapBox(boxCollider.bounds.center, (boxCollider.size / 2).Multiply(trs.lossyScale) + Vector3.one * Physics.defaultContactOffset / 2, trs.rotation);
				for (int i2 = 0; i2 < hits.Length; i2 ++)
				{
					Collider hit = hits[i2];
					if (hit.GetComponentInParent<Rigidbody>() == stuckObject.rigid)
					{
						isHittingRigid = true;
						break;
					}
				}
				if (!isHittingRigid)
				{
					stuckObject.rigid.useGravity = true;
					stuckObjects.RemoveAt(i);
					stuckObject.trs.SetParent(stuckObject.previousParent);
					if (stuckObject.rigid == Snake.instance.rigid)
						AudioManager.instance.MakeSoundEffect (Snake.instance.unstuckFromStickyTileSoundEffect, Vector3.zero);
					i --;
				}
				else
				{
					stuckObject.trs.SetParent(trs);
					stuckObject.rigid.velocity = Vector3.zero;
					stuckObject.rigid.angularVelocity = Vector3.zero;
				}
			}
		}

		void OnDisable ()
		{
			GameManager.updatables = GameManager.updatables.Remove(this);
		}

		public struct StuckObject
		{
			public Rigidbody rigid;
			public Transform trs;
			public Transform previousParent;

			public StuckObject (Rigidbody rigid, Transform trs)
			{
				this.rigid = rigid;
				this.trs = trs;
				previousParent = trs.parent;
			}
		}
	}
}